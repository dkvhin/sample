﻿using System.Data.Entity.ModelConfiguration;
using D7Soft.Data.DAO.Entity;

namespace D7Soft.Data.DAO.Mapping
{
    public class FormTemplateInputMap : EntityTypeConfiguration<FormTemplateInput>
    {
        public FormTemplateInputMap()
        {
            HasKey(c => c.Id);

            ToTable("FormTemplateInputs");

            HasRequired(c => c.Template)
                .WithMany(c => c.Inputs)
                .HasForeignKey(c => c.FormTemplateId);
        }   
    }
}
