﻿using D7Soft.Data.DAO.Entity;
using System.Data.Entity.ModelConfiguration;
namespace D7Soft.Data.DAO.Mapping
{
    internal sealed class UserMap : EntityTypeConfiguration<User>
    {
        public UserMap()
        {
            HasKey(c => c.Id);

            ToTable("Users");

            Property(c => c.FirstName).HasMaxLength(50).IsRequired();
            Property(c => c.LastName).HasMaxLength(50).IsRequired();
            Property(c => c.Email).HasMaxLength(150).IsRequired();
            Property(c => c.PasswordSalt).IsRequired();
            Property(c => c.HashedPassword).IsRequired();
            Property(c => c.Username).HasMaxLength(150).IsRequired();
            Property(c => c.DateCreated).IsRequired();
            Property(c => c.DateModified).IsRequired();

            HasMany(c => c.Roles)
                .WithMany(c => c.Users)
                .Map(c => c.MapLeftKey("UserId").MapRightKey("RoleId"));
        }
    }
}
