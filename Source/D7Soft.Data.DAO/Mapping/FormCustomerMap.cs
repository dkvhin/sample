﻿using D7Soft.Data.DAO.Entity;
using System.Data.Entity.ModelConfiguration;
namespace D7Soft.Data.DAO.Mapping
{
    public sealed class FormCustomerMap : EntityTypeConfiguration<FormCustomer>
    {
        public FormCustomerMap()
        {
            HasKey(c => c.Id);
            ToTable("FormCustomers");

            Property(c => c.FirstName).HasMaxLength(150);
            Property(c => c.LastName).HasMaxLength(150);
            Property(c => c.MiddleName).HasMaxLength(150);
            Property(c => c.Company).HasMaxLength(250);
            Property(c => c.ContactNumber).HasMaxLength(50);
            Property(c => c.Address).HasMaxLength(250);
        }
    }
}
