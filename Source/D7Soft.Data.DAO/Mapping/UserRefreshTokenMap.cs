﻿using System.Data.Entity.ModelConfiguration;
using D7Soft.Data.DAO.Entity;

namespace D7Soft.Data.DAO.Mapping
{
    public class UserRefreshTokenMap : EntityTypeConfiguration<UserRefreshToken>
    {
        public UserRefreshTokenMap()
        {
            HasKey(c => c.Id);

            ToTable("UserRefreshTokens");

            Property(c => c.Token).IsRequired();
            Property(c => c.ClientId).IsRequired();

            HasRequired(c => c.User)
                .WithMany(c => c.RefreshTokens)
                .HasForeignKey(c => c.UserId);
        }
    }
}
