﻿using System.Data.Entity.ModelConfiguration;
using D7Soft.Data.DAO.Entity;

namespace D7Soft.Data.DAO.Mapping
{
    public class FormTemplateMap : EntityTypeConfiguration<FormTemplate>
    {
        public FormTemplateMap()
        {
            HasKey(c => c.Id);

            ToTable("FormTemplates");

            Property(c => c.Name).IsRequired();
            Property(c => c.Image).IsRequired();

        }
    }
}
