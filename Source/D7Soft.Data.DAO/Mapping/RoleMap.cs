﻿using D7Soft.Data.DAO.Entity;
using System.Data.Entity.ModelConfiguration;

namespace D7Soft.Data.DAO.Mapping
{
    internal sealed class RoleMap : EntityTypeConfiguration<Role>
    {
        public RoleMap()
        {
            HasKey(c => c.Id);

            ToTable("Roles");

            Property(c => c.Name).HasMaxLength(50).IsRequired();
            Property(c => c.Description).IsRequired();
            Property(c => c.Key).HasMaxLength(50).IsRequired();

            HasRequired(c => c.Group)
                .WithMany(c => c.Roles)
                .HasForeignKey(c => c.RoleGroupId);
        }
    }
}
