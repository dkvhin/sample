﻿using D7Soft.Data.DAO.Entity;
using System.Data.Entity.ModelConfiguration;

namespace D7Soft.Data.DAO.Mapping
{
    internal sealed class ClientAppMap : EntityTypeConfiguration<ClientApp>
    {
        public ClientAppMap()
        {
            HasKey(c => c.Id);

            ToTable("ClientApps");

            Property(c => c.Name).HasMaxLength(50).IsRequired();
            Property(c => c.ClientId).HasMaxLength(50).IsRequired();
            Property(c => c.ClientSecret).IsRequired();
        }
    }
}
