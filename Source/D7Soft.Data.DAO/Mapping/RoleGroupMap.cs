﻿using D7Soft.Data.DAO.Entity;
using System.Data.Entity.ModelConfiguration;

namespace D7Soft.Data.DAO.Mapping
{
    internal sealed class RoleGroupMap : EntityTypeConfiguration<RoleGroup>
    {
        public RoleGroupMap()
        {
            HasKey(c => c.Id);

            ToTable("RoleGroups");

            Property(c => c.Name).HasMaxLength(50).IsRequired();
            Property(c => c.Description).IsRequired();
        }
    }
}
