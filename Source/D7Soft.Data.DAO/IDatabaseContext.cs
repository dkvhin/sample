﻿using D7Soft.Data.DAO.Entity;
using System.Data.Entity;

namespace D7Soft.Data.DAO
{
    public interface IDatabaseContext
    {
        DbSet<User> Users { get; set; }
        DbSet<Role> Roles { get; set; }
        DbSet<RoleGroup> RoleGroups { get; set; }
        DbSet<ClientApp> ClientApps { get; set; }
        DbSet<UserRefreshToken> UserRefreshTokens { get; set; }
        DbSet<FormTemplate> FormTemplates { get; set; }
        DbSet<FormTemplateInput> FormTemplateInputs { get; set; }
        DbSet<FormCustomer> FormCustomers { get; set; }
        int SaveChanges();
    }
}
