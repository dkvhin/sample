﻿using D7Soft.Data.DAO.Entity;
using D7Soft.Data.DAO.Mapping;
using System.Data.Entity;
namespace D7Soft.Data.DAO
{
    public sealed class DatabaseContext : DbContext, IDatabaseContext
    {
        public DatabaseContext()
            : base("DatabaseContext")
        {
            Configuration.LazyLoadingEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.AddFromAssembly(typeof(UserMap).Assembly);
        }

        public DbSet<User> Users { get;set; }
        public DbSet<Role> Roles { get;set; }
        public DbSet<RoleGroup> RoleGroups { get;set; }
        public DbSet<ClientApp> ClientApps { get; set; }
        public DbSet<UserRefreshToken> UserRefreshTokens { get; set; }
        public DbSet<FormTemplate> FormTemplates { get; set; }
        public DbSet<FormTemplateInput> FormTemplateInputs { get; set; }
        public DbSet<FormCustomer> FormCustomers { get; set; }
    }
}
