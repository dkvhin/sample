﻿using System;
using System.Collections.Generic;

namespace D7Soft.Data.DAO.Entity
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string PasswordSalt { get; set; }
        public string HashedPassword { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }

        public virtual ICollection<Role> Roles { get; set; }
        public virtual ICollection<UserRefreshToken> RefreshTokens { get; set; }
    }
}
