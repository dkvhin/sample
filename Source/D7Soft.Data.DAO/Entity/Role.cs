﻿using System;
using System.Collections.Generic;

namespace D7Soft.Data.DAO.Entity
{
    public class Role
    {
        public int Id { get; set; }
        public int RoleGroupId { get; set; }
        public string Name { get; set; }
        public string Key { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }

        public virtual RoleGroup Group { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}
