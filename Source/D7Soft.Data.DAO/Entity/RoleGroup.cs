﻿using System;
using System.Collections.Generic;

namespace D7Soft.Data.DAO.Entity
{
    public class RoleGroup
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public DateTime DateCreated { get; set; }
        public virtual ICollection<Role> Roles { get; set; }
    }
}
