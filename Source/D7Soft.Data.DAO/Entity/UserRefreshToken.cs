﻿using System;

namespace D7Soft.Data.DAO.Entity
{
    public class UserRefreshToken
    {
        public int Id { get; set; }
        public string Token { get; set; }
        public int UserId { get; set; }
        public string ClientId { get; set; }
        public DateTime Expires { get; set; }

        public virtual User User { get; set; }
    }
}
