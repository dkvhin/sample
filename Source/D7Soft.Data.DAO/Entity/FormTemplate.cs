﻿using System;
using System.Collections.Generic;

namespace D7Soft.Data.DAO.Entity
{
    public class FormTemplate
    {
        public int Id { get; set; }
        public string Name { get;set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string Image { get; set; }
        public bool IsActive { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }

        public ICollection<FormTemplateInput> Inputs { get; set; }
    }
}
