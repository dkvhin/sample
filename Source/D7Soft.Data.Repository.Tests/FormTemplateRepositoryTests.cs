﻿using System.Linq;
using D7Soft.Data.DAO;
using D7Soft.Data.Poco.FormTemplate;
using D7Soft.Data.Poco.User;
using D7Soft.Data.Repository.Interfaces;
using NUnit.Framework;
using Ploeh.AutoFixture;

namespace D7Soft.Data.Repository.Tests
{
    [TestFixture]
    public class FormTemplateRepositoryTests : BaseRepositoryTests
    {

        [Test]
        public void Can_Add_Form_Template()
        {
            using (var context = new DatabaseContext())
            {
                var fixture = new Fixture();
                IFormTemplateRepository repo = new FormTemplateRepository(context);

                var result = repo.AddFormTemplate(fixture.Create<AddFormTemplatePoco>());

                Assert.NotNull(result);
            }
        }

        [Test]
        public void Can_Update_Form_Template()
        {
            using (var context = new DatabaseContext())
            {
                var fixture = new Fixture();
                IFormTemplateRepository repo = new FormTemplateRepository(context);

                var result = repo.AddFormTemplate(fixture.Create<AddFormTemplatePoco>());

                Assert.NotNull(result);

                var update = fixture.Create<UpdateFormTemplatePoco>();

                update.Id = result.Id;

                var updated = repo.UpdateFormTemplate(update);

                Assert.NotNull(updated);
            }
        }

        [Test]
        public void Can_Add_Form_Template_Inputs()
        {
            using (var context = new DatabaseContext())
            {
                var fixture = new Fixture();
                IFormTemplateRepository repo = new FormTemplateRepository(context);

                var result = repo.AddFormTemplate(fixture.Create<AddFormTemplatePoco>());

                Assert.NotNull(result);
                var fixtureObj = fixture.Create<AddFormTemplateInputPoco>();
                fixtureObj.FormTemplateId = result.Id;
                var input = repo.AddFormTemplateInput(fixtureObj);

                Assert.NotNull(input);
            }
        }

        [Test]
        public void Can_Get_Form_Template_Inputs()
        {
            using (var context = new DatabaseContext())
            {
                var fixture = new Fixture();
                IFormTemplateRepository repo = new FormTemplateRepository(context);

                var result = repo.AddFormTemplate(fixture.Create<AddFormTemplatePoco>());

                Assert.NotNull(result);
                var fixtureObj = fixture.Create<AddFormTemplateInputPoco>();
                fixtureObj.FormTemplateId = result.Id;
                var input = repo.AddFormTemplateInput(fixtureObj);

                Assert.NotNull(input);

                var inputs = repo.GetFormTemplateInputs(fixtureObj.FormTemplateId);

                Assert.NotNull(inputs);
                Assert.Greater(inputs.Count(), 0);
            }
        }

        [Test]
        public void Can_Get_All_Form_Templates()
        {
            using (var context = new DatabaseContext())
            {
                var fixture = new Fixture();
                IFormTemplateRepository repo = new FormTemplateRepository(context);

                var result = repo.AddFormTemplate(fixture.Create<AddFormTemplatePoco>());
                Assert.NotNull(result);

                var fixtureObj = fixture.Create<AddFormTemplateInputPoco>();
                fixtureObj.FormTemplateId = result.Id;
                var input = repo.AddFormTemplateInput(fixtureObj);

                Assert.NotNull(input);


                var list = repo.GetAllFormTemplates();
                Assert.NotNull(list);

                foreach (var temp in list)
                {
                    var inputs = repo.GetFormTemplateInputs(temp.Id);
                    Assert.NotNull(inputs);
                }
                
            }
        }

        [Test]
        public void Can_Get_Form_Template_By_Id()
        {
            using (var context = new DatabaseContext())
            {
                var fixture = new Fixture();
                IFormTemplateRepository repo = new FormTemplateRepository(context);

                var result = repo.AddFormTemplate(fixture.Create<AddFormTemplatePoco>());
                Assert.NotNull(result);

                var retrieved = repo.GetFormTemplateById(result.Id);

                Assert.NotNull(retrieved);
            }
        }

        [Test]
        public void Can_Delete_Form_Template()
        {
            using (var context = new DatabaseContext())
            {
                var fixture = new Fixture();
                IFormTemplateRepository repo = new FormTemplateRepository(context);

                var result = repo.AddFormTemplate(fixture.Create<AddFormTemplatePoco>());
                Assert.NotNull(result);

                var fixtureObj = fixture.Create<AddFormTemplateInputPoco>();
                fixtureObj.FormTemplateId = result.Id;
                var input = repo.AddFormTemplateInput(fixtureObj);

                Assert.NotNull(input);

                repo.DeleteFormTemplate(result.Id);
                var retrieved = repo.GetFormTemplateById(result.Id);

                Assert.Null(retrieved);

            }
        }

        [Test]
        public void Can_Delete_Form_Template_Inputs()
        {
            using (var context = new DatabaseContext())
            {
                var fixture = new Fixture();
                IFormTemplateRepository repo = new FormTemplateRepository(context);

                var result = repo.AddFormTemplate(fixture.Create<AddFormTemplatePoco>());
                Assert.NotNull(result);

                var fixtureObj = fixture.Create<AddFormTemplateInputPoco>();
                fixtureObj.FormTemplateId = result.Id;
                var input = repo.AddFormTemplateInput(fixtureObj);

                Assert.NotNull(input);

                repo.DeleteFormTemplateInputs(result.Id);

                var inputs = repo.GetFormTemplateInputs(result.Id);
                Assert.NotNull(inputs);
                Assert.AreEqual(0, inputs.Count());
            }
        }

    }
}
