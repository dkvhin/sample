﻿using System.Transactions;
using NUnit.Framework;
using D7Soft.Data.DAO;
using D7Soft.Data.Poco.User;
using D7Soft.Data.Repository.Interfaces;
using Ploeh.AutoFixture;

namespace D7Soft.Data.Repository.Tests
{
    [TestFixture]
    public class UserRepositoryTests : BaseRepositoryTests
    {
        [Test]
        public void Can_Add_User()
        {
            using (var context = new DatabaseContext())
            {
                var fixture = new Fixture();
                IUserRepository repo = new UserRepository(context);

                var result = repo.AddUser(fixture.Create<AddUserPoco>());

                Assert.NotNull(result);
            }
        }

        [Test]
        public void Can_Get_User_By_Email()
        {
            using (var context = new DatabaseContext())
            {
                var fixture = new Fixture();
                IUserRepository repo = new UserRepository(context);

                var result = repo.AddUser(fixture.Create<AddUserPoco>());

                var retrieved = repo.GetUserByEmail(result.Email);

                Assert.NotNull(retrieved);
            }
        }

        [Test]
        public void Can_Get_User_By_Username()
        {
            using (var context = new DatabaseContext())
            {
                var fixture = new Fixture();
                IUserRepository repo = new UserRepository(context);

                var result = repo.AddUser(fixture.Create<AddUserPoco>());

                var retrieved = repo.GetUserByUsername(result.Username);

                Assert.NotNull(retrieved);
            }
        }

        [Test]
        public void Can_Get_User_By_Id()
        {
            using (var context = new DatabaseContext())
            {
                var fixture = new Fixture();
                IUserRepository repo = new UserRepository(context);

                var result = repo.AddUser(fixture.Create<AddUserPoco>());

                var retrieved = repo.GetUserCredentialDetailsById(result.Id);

                Assert.NotNull(retrieved);
            }
        }

        [Test]
        public void Can_Update_User_Details()
        {
            using (var context = new DatabaseContext())
            {
                var fixture = new Fixture();
                IUserRepository repo = new UserRepository(context);

                var result = repo.AddUser(fixture.Create<AddUserPoco>());
                var dummy = fixture.Create<UpdateUserDetailsPoco>();
                dummy.Id = result.Id;

                var updated = repo.UpdateUserDetails(dummy);

                Assert.NotNull(updated);
                Assert.AreEqual(dummy.FirstName, updated.FirstName);
            }
        }

        [Test]
        public void Can_Get_All_Users()
        {
            using (var context = new DatabaseContext())
            {
                IUserRepository repo = new UserRepository(context);

                var result = repo.GetAll();

                Assert.NotNull(result);
            }
        }

        [Test]
        public void Can_Check_If_Username_Already_Existing()
        {
            using (var context = new DatabaseContext())
            {
                IUserRepository repo = new UserRepository(context);

                var result = repo.IsDuplicateUsername("admin");

                Assert.IsTrue(result);
            }
        }

        [Test]
        public void Can_Check_If_Email_Already_Existing()
        {
            using (var context = new DatabaseContext())
            {
                IUserRepository repo = new UserRepository(context);

                var result = repo.IsDuplicateEmail("admin@email.com");

                Assert.IsTrue(result);
            }
        }

        [Test]
        public void Can_Get_User_Details_By_Id()
        {
            using (var context = new DatabaseContext())
            {
                var fixture = new Fixture();
                IUserRepository repo = new UserRepository(context);

                var result = repo.AddUser(fixture.Create<AddUserPoco>());

                var retrieved = repo.GetUserDetailsById(result.Id);

                Assert.NotNull(retrieved);
            }
        }

        [Test]
        public void Can_Update_User_Password()
        {
            using (var context = new DatabaseContext())
            {
                var fixture = new Fixture();
                IUserRepository repo = new UserRepository(context);

                var result = repo.AddUser(fixture.Create<AddUserPoco>());

                Assert.NotNull(result);
                var poco = fixture.Create<UpdateUserPasswordPoco>();
                poco.Id = result.Id;

                var updated = repo.UpdateUserPassword(poco);

                Assert.NotNull(updated);
            }
        }
    }
}
