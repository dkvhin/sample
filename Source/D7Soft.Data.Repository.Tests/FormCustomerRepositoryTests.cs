﻿
using System.Linq;
using D7Soft.Data.DAO;
using D7Soft.Data.Poco.FormCustomer;
using D7Soft.Data.Poco.FormTemplate;
using D7Soft.Data.Repository.Interfaces;
using NUnit.Framework;
using Ploeh.AutoFixture;

namespace D7Soft.Data.Repository.Tests
{
    [TestFixture]
    public class FormCustomerRepositoryTests : BaseRepositoryTests
    {

        [Test]
        public void Can_Add_New_Customer()
        {
            using (var context = new DatabaseContext())
            {
                var fixture = new Fixture();
                IFormCustomerRepository repo = new FormCustomerRepository(context);

                var result = repo.AddCustomer(fixture.Create<AddFormCustomerPoco>());

                Assert.NotNull(result);
            }
        }

        [Test]
        public void Can_Update_Form_Customer()
        {
            using (var context = new DatabaseContext())
            {
                var fixture = new Fixture();
                IFormCustomerRepository repo = new FormCustomerRepository(context);

                var result = repo.AddCustomer(fixture.Create<AddFormCustomerPoco>());

                Assert.NotNull(result);

                var update = fixture.Create<UpdateFormCustomerPoco>();

                update.Id = result.Id;

                var updated = repo.UpdateCustomer(update);

                Assert.NotNull(updated);
            }
        }

        [Test]
        public void Can_Get_All_Form_Customer()
        {
            using (var context = new DatabaseContext())
            {
                var fixture = new Fixture();
                IFormCustomerRepository repo = new FormCustomerRepository(context);

                var result = repo.AddCustomer(fixture.Create<AddFormCustomerPoco>());
                Assert.NotNull(result);
                var list = repo.GetAll();
                Assert.NotNull(list);
                Assert.IsTrue(list.Any());
            }
        }

        [Test]
        public void Can_Get_Form_Customer_By_Id()
        {
            using (var context = new DatabaseContext())
            {
                var fixture = new Fixture();
                IFormCustomerRepository repo = new FormCustomerRepository(context);

                var result = repo.AddCustomer(fixture.Create<AddFormCustomerPoco>());
                Assert.NotNull(result);

                var retrieved = repo.GetCustomerById(result.Id);

                Assert.NotNull(retrieved);
            }
        }

        [Test]
        public void Can_Delete_Form_Customer()
        {
            using (var context = new DatabaseContext())
            {
                var fixture = new Fixture();
                IFormCustomerRepository repo = new FormCustomerRepository(context);

                var result = repo.AddCustomer(fixture.Create<AddFormCustomerPoco>());
                Assert.NotNull(result);

                var retrieved = repo.GetCustomerById(result.Id);

                Assert.NotNull(retrieved);

                repo.DeleteCustomer(result.Id);

                retrieved = repo.GetCustomerById(result.Id);

                Assert.Null(retrieved);
            }
        }

        [Test]
        public void Can_Filter_Form_Customer()
        {
            using (var context = new DatabaseContext())
            {
                var fixture = new Fixture();
                IFormCustomerRepository repo = new FormCustomerRepository(context);
                var poco = fixture.Create<AddFormCustomerPoco>();
                poco.FirstName = "Sample";
                poco.LastName = "Mermaid";
                var result = repo.AddCustomer(poco);
                Assert.NotNull(result);


                var list = repo.Filter("sam mer");
                Assert.NotNull(list);
                Assert.IsTrue(list.Any());
            }
        }

        [Test]
        public void Can_Filter_Form_Customer_No_Result()
        {
            using (var context = new DatabaseContext())
            {
                var fixture = new Fixture();
                IFormCustomerRepository repo = new FormCustomerRepository(context);
                var poco = fixture.Create<AddFormCustomerPoco>();
                poco.FirstName = "Sample";
                poco.LastName = "Mermaid";
                var result = repo.AddCustomer(poco);
                Assert.NotNull(result);


                var list = repo.Filter("marr roxas");
                Assert.NotNull(list);
                Assert.IsFalse(list.Any());
            }
        }
    }
}
