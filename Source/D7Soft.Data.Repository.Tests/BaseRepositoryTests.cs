﻿using System.Transactions;
using NUnit.Framework;

namespace D7Soft.Data.Repository.Tests
{
    public abstract class BaseRepositoryTests
    {
        private TransactionScope _scope;

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            _scope = new TransactionScope(TransactionScopeOption.Required,
             new TransactionOptions { IsolationLevel = IsolationLevel.ReadUncommitted });
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            _scope.Dispose();
        }
    }
}
