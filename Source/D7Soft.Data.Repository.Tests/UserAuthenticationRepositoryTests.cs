﻿using System.Transactions;
using D7Soft.Data.DAO;
using D7Soft.Data.Poco.User;
using D7Soft.Data.Poco.UserRefreshToken;
using D7Soft.Data.Repository.Interfaces;
using NUnit.Framework;
using Ploeh.AutoFixture;

namespace D7Soft.Data.Repository.Tests
{
    [TestFixture]
    public class UserAuthenticationRepositoryTests : BaseRepositoryTests
    {
        private int _newUserId;
        [SetUp]
        public void Setup()
        {
            using (var context = new DatabaseContext())
            {
                var fixture = new Fixture();
                IUserRepository repoUser = new UserRepository(context);
                var newUser = repoUser.AddUser(fixture.Create<AddUserPoco>());
                _newUserId = newUser.Id;
            }
        }

        [Test]
        public void Can_Get_Client_App_By_Client_Id()
        {
            using (var context = new DatabaseContext())
            {
                IUserAuthenticationRepository repo = new UserAuthenticationRepository(context);

                var result = repo.GetByClientId("18d55035-c3b4-43c8-b723-e9bf787e5db1");

                Assert.NotNull(result);
            }
        }

        [Test]
        public void Can_Create_New_User_Refresh_Token()
        {
            using (var context = new DatabaseContext())
            {
                IUserAuthenticationRepository repo = new UserAuthenticationRepository(context);

                  var fixture = new Fixture();
                var objFixture = fixture.Create<AddUserRefreshTokenPoco>();
                objFixture.UserId = _newUserId;

                var result = repo.AddRefreshToken(objFixture);

                Assert.NotNull(result);
            }
        }

        [Test]
        public void Can_Get_User_Refresh_token()
        {
            using (var context = new DatabaseContext())
            {
                IUserAuthenticationRepository repo = new UserAuthenticationRepository(context);

                var fixture = new Fixture();
                var objFixture = fixture.Create<AddUserRefreshTokenPoco>();
                objFixture.UserId = _newUserId;

                var result = repo.AddRefreshToken(objFixture);

                Assert.NotNull(result);

                var retrieved = repo.GetRefreshTokenByToken(result.Token);
                Assert.NotNull(retrieved);
            }
        }

        [Test]
        public void Can_Delete_Refresh_Token()
        {
            using (var context = new DatabaseContext())
            {
                IUserAuthenticationRepository repo = new UserAuthenticationRepository(context);

                var fixture = new Fixture();
                var objFixture = fixture.Create<AddUserRefreshTokenPoco>();
                objFixture.UserId = _newUserId;

                var result = repo.AddRefreshToken(objFixture);

                Assert.NotNull(result);

                var retrieved = repo.GetRefreshTokenByToken(result.Token);
                Assert.NotNull(retrieved);

                repo.DeleteByToken(result.Token);

                retrieved = repo.GetRefreshTokenByToken(result.Token);
                Assert.Null(retrieved);
            }
        }
    }
}
