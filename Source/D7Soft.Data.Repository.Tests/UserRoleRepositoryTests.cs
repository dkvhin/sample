﻿using System.Collections.Generic;
using System.Linq;
using D7Soft.Data.DAO;
using D7Soft.Data.Poco.User;
using D7Soft.Data.Poco.UserRole;
using D7Soft.Data.Repository.Interfaces;
using NUnit.Framework;
using Ploeh.AutoFixture;

namespace D7Soft.Data.Repository.Tests
{
    [TestFixture]
    public class UserRoleRepositoryTests : BaseRepositoryTests
    {
        private int _newUserId;
        [SetUp]
        public void Setup()
        {
            using (var context = new DatabaseContext())
            {
                var fixture = new Fixture();
                IUserRepository repoUser = new UserRepository(context);
                var newUser = repoUser.AddUser(fixture.Create<AddUserPoco>());
                _newUserId = newUser.Id;
            }
        }

        [Test]
        public void Can_Add_New_Role()
        {
            using (var context = new DatabaseContext())
            {
                IUserRoleRepository repo = new UserRoleRepository(context);
              
                var objFixture = new AddUserRolePoco
                {
                    UserId = _newUserId,
                    RoleIds = new List<int> {1, 2}
                };
                var result = repo.AddRoles(objFixture);

                Assert.NotNull(result);
            }
        }

        [Test]
        public void Can_Get_User_Roles()
        {
            using (var context = new DatabaseContext())
            {
                IUserRoleRepository repo = new UserRoleRepository(context);

                var objFixture = new AddUserRolePoco
                {
                    UserId = _newUserId,
                    RoleIds = new List<int> { 1, 2 }
                };
                var result = repo.AddRoles(objFixture);

                Assert.NotNull(result);


                var roles = repo.GetRolesByUserId(_newUserId);

                Assert.NotNull(roles);
                Assert.AreEqual(2, roles.Count());
            }
        }

        [Test]
        public void Can_Get_User_Groups_And_Roles()
        {
            using (var context = new DatabaseContext())
            {
                IUserRoleRepository repo = new UserRoleRepository(context);

                var roles = repo.GetAllGroupsWithRoles(1);
                Assert.NotNull(roles);

                foreach (var role in roles)
                {
                    Assert.Greater(role.Roles.Count(), 0);
                }
            }
        }

        [Test]
        public void Can_Remove_User_Roles()
        {
            using (var context = new DatabaseContext())
            {
                IUserRoleRepository repo = new UserRoleRepository(context);
                repo.RemoveUserRoles(1);
                var roles = repo.GetRolesByUserId(1);
                Assert.AreEqual(0, roles.Count());
            }
        }
    }
}
