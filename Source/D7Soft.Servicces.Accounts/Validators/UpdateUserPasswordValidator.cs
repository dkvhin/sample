﻿using D7Soft.Core.Validation;
using D7Soft.Data.Repository.Interfaces;
using D7Soft.Services.Accounts.ParamModels;
using System.Collections.Generic;

namespace D7Soft.Services.Accounts.Validators
{
    public class UpdateUserPasswordValidator: Validator<UpdateUserPasswordParamModel> 
    {
        private readonly IUserRepository _userRepository;
        public UpdateUserPasswordValidator(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        protected override IEnumerable<ValidationResult> Validate(UpdateUserPasswordParamModel entity)
        {
            if (!string.IsNullOrEmpty(entity.Password) && entity.Password.Length < 4)
                yield return
                    new ValidationResult<UpdateUserPasswordParamModel>(c => c.Password,
                        "Password must be at least 4 character long");

        }
    }
}
