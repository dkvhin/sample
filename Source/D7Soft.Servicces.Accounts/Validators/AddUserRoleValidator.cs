﻿using D7Soft.Core.Validation;
using D7Soft.Data.Repository.Interfaces;
using D7Soft.Services.Accounts.ParamModels;
using System.Collections.Generic;

namespace D7Soft.Services.Accounts.Validators
{
    public class AddUserRoleValidator: Validator<AddUserRolesParamModel> 
    {
        private readonly IUserRepository _userRepository;
        public AddUserRoleValidator(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        protected override IEnumerable<ValidationResult> Validate(AddUserRolesParamModel entity)
        {
            if (entity.RoleIds.Count == 0)
                yield return
                    new ValidationResult<AddUserRolesParamModel>(c => c.RoleIds, "Please select at least one role");
        }
    }
}
