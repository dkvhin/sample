﻿using D7Soft.Core.Validation;
using D7Soft.Data.Poco.User;
using D7Soft.Data.Repository.Interfaces;
using System.Collections.Generic;
using D7Soft.Services.Accounts.ParamModels;

namespace D7Soft.Services.Accounts.Validators
{
    public class UpdateUserValidator : Validator<UpdateUserParamModel>
    {
        private readonly IUserRepository _userRepository;

        public UpdateUserValidator(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        protected override IEnumerable<ValidationResult> Validate(UpdateUserParamModel entity)
        {
            if (_userRepository.IsDuplicateUsername(entity.Username, entity.Id))
                yield return new ValidationResult<UpdateUserDetailsPoco>(c => c.Username, "Username already exist");

            if(_userRepository.IsDuplicateEmail(entity.Email, entity.Id))
                yield return new ValidationResult<UpdateUserDetailsPoco>(c => c.Email, "Email already exist");

        }
    }
}
