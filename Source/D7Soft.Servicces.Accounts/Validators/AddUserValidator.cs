﻿using System.Collections.Generic;
using D7Soft.Core.Helpers;
using D7Soft.Core.Validation;
using D7Soft.Data.Repository.Interfaces;
using D7Soft.Services.Accounts.ParamModels;

namespace D7Soft.Services.Accounts.Validators
{
    public class AddUserValidator : Validator<AddUserParamModel> 
    {
        private readonly IUserRepository _userRepository;
        public AddUserValidator(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        protected override IEnumerable<ValidationResult> Validate(AddUserParamModel entity)
        {
            if (_userRepository.IsDuplicateUsername(entity.UserName))
                yield return new ValidationResult<AddUserParamModel>(c => c.UserName, "Username already existing.");

            if (_userRepository.IsDuplicateEmail(entity.Email))
                yield return new ValidationResult<AddUserParamModel>(c => c.Email, "Email already existing");

            if(string.IsNullOrEmpty(entity.Password))
                yield return new ValidationResult<AddUserParamModel>(c => c.Password, "Password is required");

            if (!string.IsNullOrEmpty(entity.Password) && entity.Password.Length < 4)
                yield return new ValidationResult<AddUserParamModel>(c => c.Password, "Password should be 4 or more characters long");

            if (!string.IsNullOrEmpty(entity.Email) && !RegexValidation.IsValidEmail(entity.Email))
                yield return new ValidationResult<AddUserParamModel>(c => c.Email, "Invalid email.");
        }
    }
}
