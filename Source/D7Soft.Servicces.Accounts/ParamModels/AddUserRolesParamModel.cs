﻿using System.Collections.Generic;

namespace D7Soft.Services.Accounts.ParamModels
{
    public class AddUserRolesParamModel
    {
        public int UserId { get; set; }
        public List<int> RoleIds { get; set; }
    }
}
