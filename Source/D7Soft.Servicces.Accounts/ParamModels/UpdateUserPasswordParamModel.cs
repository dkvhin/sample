﻿namespace D7Soft.Services.Accounts.ParamModels
{
    public class UpdateUserPasswordParamModel
    {
        public int Id { get; set; }
        public string Password { get; set; }
    }
}
