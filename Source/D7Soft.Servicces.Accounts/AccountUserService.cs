﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using D7Soft.Authentication;
using D7Soft.Data.Poco.UserRole;
using D7Soft.Data.Repository.Interfaces;
using D7Soft.Services.Accounts.Interfaces;
using D7Soft.Services.Accounts.ParamModels;
using D7Soft.Services.Accounts.ViewModels;
using D7Soft.Core.Validation;
using D7Soft.Data.Poco.User;

namespace D7Soft.Services.Accounts
{
    public class AccountUserService : IAccountUserService
    {
        private readonly IUserRoleRepository _userRoleRepository;
        private readonly IMapper _mapper;
        private readonly IUserRepository _userRepository;
        private readonly IValidationProvider _validationProvider;
        private readonly IPasswordHasher _passwordHasher;

        public AccountUserService(IUserRoleRepository userRoleRepository, IMapper mapper, IUserRepository userRepository,
            IValidationProvider validationProvider, IPasswordHasher passwordHasher)
        {
            _userRoleRepository = userRoleRepository;
            _mapper = mapper;
            _userRepository = userRepository;
            _validationProvider = validationProvider;
            _passwordHasher = passwordHasher;
        }

        public IEnumerable<RoleGroupInfoViewModel> GetAllRoleGroupWithRoles(int userId)
        {
            var result = _userRoleRepository.GetAllGroupsWithRoles(userId);

            foreach (var roleGroup in result)
            {
                var mapped = _mapper.Map<RoleGroupInfoViewModel>(roleGroup);
                mapped.Roles = roleGroup.Roles.Select(_mapper.Map<RoleInfoViewModel>).ToList();
                yield return mapped;
            }
        }


        public IEnumerable<RoleViewModel> AddUserRoles(AddUserRolesParamModel paramModel)
        {
            _validationProvider.Validate(paramModel);
            var poco = _mapper.Map<AddUserRolePoco>(paramModel);
            _userRoleRepository.RemoveUserRoles(paramModel.UserId);
            var result = _userRoleRepository.AddRoles(poco);
            return result.Roles.Select(_mapper.Map<RoleViewModel>);
        }

        public IEnumerable<UserDetailsViewModel> GetAllUsers()
        {
            var result = _userRepository.GetAll();
            return result.Select(_mapper.Map<UserDetailsViewModel>);
        }

        public UserDetailsViewModel AddUser(AddUserParamModel paramModel)
        {
            _validationProvider.Validate(paramModel);

            var poco = _mapper.Map<AddUserPoco>(paramModel);

            poco.PasswordSalt = _passwordHasher.GenerateRandomSalt();
            poco.HashedPassword = _passwordHasher.Sha256Hash(poco.PasswordSalt + paramModel.Password);

            var result = _userRepository.AddUser(poco);
            return _mapper.Map<UserDetailsViewModel>(result);
        }


        public UserDetailsViewModel GetUserDetailsById(int userId)
        {
            var poco = _userRepository.GetUserDetailsById(userId);
            return poco == null ? null : _mapper.Map<UserDetailsViewModel>(poco);
        }


        public UserDetailsViewModel UpdateUser(UpdateUserParamModel paramModel)
        {
            var existing = _userRepository.GetUserDetailsById(paramModel.Id);

            if (existing == null)
                return null;

            _validationProvider.Validate(paramModel);

            var poco = _mapper.Map<UpdateUserDetailsPoco>(paramModel);
            var updated = _userRepository.UpdateUserDetails(poco);
            return _mapper.Map<UserDetailsViewModel>(updated);
        }


        public UserDetailsViewModel UpdateUserPassword(UpdateUserPasswordParamModel paramModel)
        {
            var existing = _userRepository.GetUserDetailsById(paramModel.Id);

            if (existing == null)
                return null;

            _validationProvider.Validate(paramModel);
            var salt = _passwordHasher.GenerateRandomSalt();
            var poco = new UpdateUserPasswordPoco
            {
                PasswordSalt = salt,
                HashedPassword = _passwordHasher.Sha256Hash(paramModel.Password + salt),
                Id = paramModel.Id
            };

            var updated = _userRepository.UpdateUserPassword(poco);
            return _mapper.Map<UserDetailsViewModel>(updated); 
        }
    }
}
