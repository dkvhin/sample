﻿using System.Collections.Generic;
using D7Soft.Services.Accounts.ParamModels;
using D7Soft.Services.Accounts.ViewModels;

namespace D7Soft.Services.Accounts.Interfaces
{
    public interface IAccountUserService
    {
        IEnumerable<RoleGroupInfoViewModel> GetAllRoleGroupWithRoles(int userId);

        IEnumerable<RoleViewModel> AddUserRoles(AddUserRolesParamModel paramModel);

        IEnumerable<UserDetailsViewModel> GetAllUsers();
        UserDetailsViewModel AddUser(AddUserParamModel paramModel);
        UserDetailsViewModel GetUserDetailsById(int userId);

        UserDetailsViewModel UpdateUser(UpdateUserParamModel paramModel);

        UserDetailsViewModel UpdateUserPassword(UpdateUserPasswordParamModel paramModel);
    }
}
