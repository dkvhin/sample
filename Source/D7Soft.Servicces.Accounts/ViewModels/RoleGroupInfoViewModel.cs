﻿using System.Collections.Generic;

namespace D7Soft.Services.Accounts.ViewModels
{
    public class RoleGroupInfoViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<RoleInfoViewModel> Roles { get; set; }
    }
}
