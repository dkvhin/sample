﻿using AutoMapper;
using D7Soft.Common.Interfaces;
using D7Soft.Data.Poco.Role;
using D7Soft.Services.Accounts.ViewModels;

namespace D7Soft.Services.Accounts.Infrastructure.Mappers
{
    public class RoleInfoMapper : ICustomMapper
    {
        public void Map(IMapperConfiguration configuration)
        {
            configuration.CreateMap<RoleInfoSummaryPoco, RoleInfoViewModel>();
            configuration.CreateMap<RoleSummaryPoco, RoleViewModel>();
        }
    }
}
