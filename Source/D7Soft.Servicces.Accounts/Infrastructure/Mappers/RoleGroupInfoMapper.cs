﻿using AutoMapper;
using D7Soft.Common.Interfaces;
using D7Soft.Data.Poco.RoleGroup;
using D7Soft.Services.Accounts.ViewModels;

namespace D7Soft.Services.Accounts.Infrastructure.Mappers
{
    public class RoleGroupInfoMapper : ICustomMapper
    {
        public void Map(IMapperConfiguration configuration)
        {
            configuration.CreateMap<RoleGroupSummaryPoco, RoleGroupInfoViewModel>();
        }
    }
}
