﻿using AutoMapper;
using D7Soft.Common.Interfaces;
using D7Soft.Data.Poco.Role;
using D7Soft.Data.Poco.UserRole;
using D7Soft.Services.Accounts.ParamModels;
using D7Soft.Services.Accounts.ViewModels;

namespace D7Soft.Services.Accounts.Infrastructure.Mappers
{
    public class UserRoleMapper : ICustomMapper
    {
        public void Map(IMapperConfiguration configuration)
        {
            configuration.CreateMap<AddUserRolesParamModel, AddUserRolePoco>();
        }
    }
}
