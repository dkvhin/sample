﻿using AutoMapper;
using D7Soft.Common.Interfaces;
using D7Soft.Data.Poco.User;
using D7Soft.Services.Accounts.ParamModels;
using D7Soft.Services.Accounts.ViewModels;

namespace D7Soft.Services.Accounts.Infrastructure.Mappers
{
    public class UserMapper : ICustomMapper
    {
        public void Map(IMapperConfiguration configuration)
        {
            configuration.CreateMap<UserDetailsPoco, UserDetailsViewModel>();

            configuration.CreateMap<AddUserParamModel, AddUserPoco>()
                .ForMember(c => c.PasswordSalt, opt => opt.Ignore())
                .ForMember(c => c.HashedPassword, opt => opt.Ignore());

            configuration.CreateMap<UpdateUserParamModel, UpdateUserDetailsPoco>();
        }
    }
}
