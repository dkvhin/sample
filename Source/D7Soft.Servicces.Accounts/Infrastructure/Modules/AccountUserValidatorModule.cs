﻿using D7Soft.Common.Interfaces;
using D7Soft.Core.Validation;
using D7Soft.Services.Accounts.ParamModels;
using D7Soft.Services.Accounts.Validators;
using SimpleInjector;

namespace D7Soft.Services.Accounts.Infrastructure.Modules
{
    public class AccountUserValidatorModule : IValidatorBootstrapper
    {
        public void Configure(Container container)
        {
            container.RegisterConditional<Validator<AddUserParamModel>, AddUserValidator>(
                c => c.ImplementationType == typeof (AddUserValidator));

            container.RegisterConditional<Validator<AddUserRolesParamModel>, AddUserRoleValidator>(
                c => c.ImplementationType == typeof(AddUserRoleValidator));

            container.RegisterConditional<Validator<UpdateUserParamModel>, UpdateUserValidator>(
                c => c.ImplementationType == typeof(UpdateUserValidator));

            container.RegisterConditional<Validator<UpdateUserPasswordParamModel>, UpdateUserPasswordValidator>(
                c => c.ImplementationType == typeof(UpdateUserPasswordValidator));
        }
    }
}
