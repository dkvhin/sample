﻿using System;
using System.Linq;
using AutoMapper;
using D7Soft.Common.Interfaces;
using D7Soft.Services.Accounts.Infrastructure.Mappers;

namespace D7Soft.Services.Accounts.Infrastructure
{
    public class AccountModelMapper : IMapperBootstrapper
    {
        public void Configure(IMapperConfiguration configuration)
        {
            var type = typeof(ICustomMapper);
            var types = typeof(RoleGroupInfoMapper).Assembly.GetTypes()
                .Where(p => type.IsAssignableFrom(p) && !p.IsInterface).ToList();

            types.ForEach(t =>
            {
                var n = (ICustomMapper)Activator.CreateInstance(t);
                n.Map(configuration);
            });
        }
    }
}
