﻿using AutoMapper;
using D7Soft.Core.Validation;
using D7Soft.Data.Poco.FormCustomer;
using D7Soft.Data.Repository.Interfaces;
using D7Soft.Services.Forms.Interfaces;
using D7Soft.Services.Forms.ParamModels;
using D7Soft.Services.Forms.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace D7Soft.Services.Forms
{
    public class FormCustomerService : IFormCustomerService
    {
        private readonly IFormCustomerRepository _formCustomerRepository;
        private readonly IMapper _mapper;
        private readonly IValidationProvider _validationProvider;

        public FormCustomerService(IFormCustomerRepository formCustomerRepository, IMapper mapper, IValidationProvider validationProvider)
        {
            _formCustomerRepository = formCustomerRepository;
            _mapper = mapper;
            _validationProvider = validationProvider;
        }

        public FormCustomerViewModel AddCustomer(AddFormCustomerParamModel paramModel)
        {
            _validationProvider.Validate(paramModel);
            var poco = _mapper.Map<AddFormCustomerPoco>(paramModel);
            var result = _formCustomerRepository.AddCustomer(poco);
            return _mapper.Map<FormCustomerViewModel>(result);
        }

        public IEnumerable<FormCustomerViewModel> GetAll()
        {
            var result = _formCustomerRepository.GetAll();
            return result.Select(_mapper.Map<FormCustomerViewModel>);
        }


        public FormCustomerViewModel GetCustomerById(int formCustomerId)
        {
            var result = _formCustomerRepository.GetCustomerById(formCustomerId);
            return result == null ? null : _mapper.Map<FormCustomerViewModel>(result);
        }


        public FormCustomerViewModel UpdateCustomer(UpdateFormCustomerParamModel paramModel)
        {
            _validationProvider.Validate(paramModel);
            var updatePoco = _mapper.Map<UpdateFormCustomerPoco>(paramModel);
            var updatedPoco = _formCustomerRepository.UpdateCustomer(updatePoco);
            return _mapper.Map<FormCustomerViewModel>(updatedPoco);
        }


        public void DeleteCustomer(int formCustomerId)
        {
            _formCustomerRepository.DeleteCustomer(formCustomerId);
        }


        public IEnumerable<FormCustomerViewModel> Filter(string keyword)
        {
            var result = _formCustomerRepository.Filter(keyword);
            return result.Select(_mapper.Map<FormCustomerViewModel>);
        }
    }
}
