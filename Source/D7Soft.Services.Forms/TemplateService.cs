﻿using System.Collections.Generic;
using System.Linq;
using D7Soft.Data.Poco.FormTemplate;
using D7Soft.Data.Repository.Interfaces;
using D7Soft.Services.Forms.Interfaces;
using D7Soft.Services.Forms.ParamModels;
using D7Soft.Services.Forms.ViewModels;
using AutoMapper;

namespace D7Soft.Services.Forms
{
    public class TemplateService : ITemplateService
    {
        private readonly IFormTemplateRepository _formTemplateRepository;
        private readonly IMapper _mapper;
        public TemplateService(IFormTemplateRepository formTemplateRepository, IMapper mapper)
        {
            _formTemplateRepository = formTemplateRepository;
            _mapper = mapper;
        }

        public FormTemplateViewModel AddFormTemplate(AddFormTemplateParamModel paramModel)
        {
            var poco = _mapper.Map<AddFormTemplatePoco>(paramModel);
            poco.IsActive = true;

            var addedPoco = _formTemplateRepository.AddFormTemplate(poco);

            var list = new List<FormTemplateInputViewModel>();

            paramModel.Inputs.ForEach(input =>
            {
                var inputPoco = _mapper.Map<AddFormTemplateInputPoco>(input);

                inputPoco.FormTemplateId = addedPoco.Id;

                var addedInputPoco = _formTemplateRepository.AddFormTemplateInput(inputPoco);

                list.Add(_mapper.Map<FormTemplateInputViewModel>(addedInputPoco));
            });

            var mapped = _mapper.Map<FormTemplateViewModel>(addedPoco);
            mapped.Inputs = list;

            return mapped;
        }


        public IEnumerable<FormTemplateViewModel> GetAllFormTemplates()
        {
            var template = _formTemplateRepository.GetAllFormTemplates();

            var mappedList = template.Select(_mapper.Map<FormTemplateViewModel>).ToList();

            foreach (var mapped in mappedList)
            {
                var inputs = _formTemplateRepository.GetFormTemplateInputs(mapped.Id);
                mapped.Inputs = inputs.Select(_mapper.Map<FormTemplateInputViewModel>);
            }

            return mappedList;
        }


        public FormTemplateViewModel GetFormTemplateById(int formTemplateId)
        {
            var template = _formTemplateRepository.GetFormTemplateById(formTemplateId);

            var mapped = _mapper.Map<FormTemplateViewModel>(template);

            mapped.Inputs =
                _formTemplateRepository.GetFormTemplateInputs(formTemplateId)
                    .Select(_mapper.Map<FormTemplateInputViewModel>);

            return mapped;
        }


        public FormTemplateViewModel UpdateFormTemplate(UpdateFormTemplateParamModel paramModel)
        {
            var updatePoco = _mapper.Map<UpdateFormTemplatePoco>(paramModel);

            var updatedPoco = _formTemplateRepository.UpdateFormTemplate(updatePoco);
            _formTemplateRepository.DeleteFormTemplateInputs(paramModel.Id);
            var list = new List<FormTemplateInputViewModel>();
            paramModel.Inputs.ForEach(input =>
            {
                var inputPoco = _mapper.Map<AddFormTemplateInputPoco>(input);
                inputPoco.FormTemplateId = updatedPoco.Id;
                var addedPoco = _formTemplateRepository.AddFormTemplateInput(inputPoco);
                list.Add(_mapper.Map<FormTemplateInputViewModel>(addedPoco));
            });

            var mapped = _mapper.Map<FormTemplateViewModel>(updatedPoco);
            mapped.Inputs = list;

            return mapped;
        }


        public void DeleteTemplate(int formTemplateId)
        {
            _formTemplateRepository.DeleteFormTemplate(formTemplateId);
        }
    }
}
