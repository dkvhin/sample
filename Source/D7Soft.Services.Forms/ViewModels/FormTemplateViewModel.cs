﻿using System.Collections.Generic;

namespace D7Soft.Services.Forms.ViewModels
{
    public class FormTemplateViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string Image { get; set; }
        public bool IsActive { get; set; }
        public IEnumerable<FormTemplateInputViewModel> Inputs { get; set; }
    }
}
