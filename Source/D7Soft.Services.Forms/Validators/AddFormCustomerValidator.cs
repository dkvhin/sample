﻿using D7Soft.Core.Validation;
using D7Soft.Services.Forms.ParamModels;
using System.Collections.Generic;

namespace D7Soft.Services.Forms.Validators
{
    public sealed class AddFormCustomerValidator : Validator<AddFormCustomerParamModel> 
    {
        protected override IEnumerable<ValidationResult> Validate(AddFormCustomerParamModel entity)
        {
            if (string.IsNullOrEmpty(entity.FirstName) && string.IsNullOrEmpty(entity.LastName) &&
                string.IsNullOrEmpty(entity.Company) && string.IsNullOrEmpty(entity.MiddleName))
                yield return new ValidationResult<AddFormCustomerParamModel>(c => c.FirstName, "Name is required");


            if(!string.IsNullOrEmpty(entity.FirstName) && entity.FirstName.Length > 150)
                yield return new ValidationResult<AddFormCustomerParamModel>(c => c.FirstName, "Cannot exceed 150 characters");

            if (!string.IsNullOrEmpty(entity.LastName) && entity.LastName.Length > 150)
                yield return new ValidationResult<AddFormCustomerParamModel>(c => c.LastName, "Cannot exceed 150 characters");

            if (!string.IsNullOrEmpty(entity.MiddleName) && entity.MiddleName.Length > 150)
                yield return new ValidationResult<AddFormCustomerParamModel>(c => c.MiddleName, "Cannot exceed 150 characters");

            if (!string.IsNullOrEmpty(entity.Company) && entity.Company.Length > 250)
                yield return new ValidationResult<AddFormCustomerParamModel>(c => c.Company, "Cannot exceed 250 characters");

            if (!string.IsNullOrEmpty(entity.ContactNumber) && entity.ContactNumber.Length > 50)
                yield return new ValidationResult<AddFormCustomerParamModel>(c => c.ContactNumber, "Cannot exceed 50 characters");

            if (!string.IsNullOrEmpty(entity.Address) && entity.Address.Length > 250)
                yield return new ValidationResult<AddFormCustomerParamModel>(c => c.Address, "Cannot exceed 250 characters");

        }
    }
}
