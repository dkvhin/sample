﻿namespace D7Soft.Services.Forms.ParamModels
{
    public class AddFormTemplateInputParamModel
    {
        public int Width { get; set; }
        public int PositionX { get; set; }
        public int PositionY { get; set; }
        public bool IsNumeric { get; set; }
        public bool HasConvertInput { get; set; }
        public bool IsConvert { get; set; }
        public int FontSize { get; set; }
        public bool IsDate { get; set; }
        public bool IsAutoComplete { get; set; }
    }
}
