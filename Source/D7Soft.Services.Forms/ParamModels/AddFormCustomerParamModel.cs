﻿namespace D7Soft.Services.Forms.ParamModels
{
    public class AddFormCustomerParamModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Company { get; set; }
        public string ContactNumber { get; set; }
        public string Address { get; set; }

    }
}
