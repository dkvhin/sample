﻿using System.Collections.Generic;

namespace D7Soft.Services.Forms.ParamModels
{
    public class AddFormTemplateParamModel
    {
        public string Name { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string Image { get; set; }

        public List<AddFormTemplateInputParamModel> Inputs { get; set; }
    }
}
