﻿using System.Collections.Generic;
using D7Soft.Services.Forms.ParamModels;
using D7Soft.Services.Forms.ViewModels;

namespace D7Soft.Services.Forms.Interfaces
{
    public interface IFormCustomerService
    {
        FormCustomerViewModel AddCustomer(AddFormCustomerParamModel paramModel);
        IEnumerable<FormCustomerViewModel> GetAll();
        FormCustomerViewModel GetCustomerById(int formCustomerId);
        FormCustomerViewModel UpdateCustomer(UpdateFormCustomerParamModel paramModel);
        void DeleteCustomer(int formCustomerId);
        IEnumerable<FormCustomerViewModel> Filter(string keyword);
    }
}
