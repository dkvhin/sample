﻿using System.Collections.Generic;
using D7Soft.Services.Forms.ParamModels;
using D7Soft.Services.Forms.ViewModels;

namespace D7Soft.Services.Forms.Interfaces
{
    public interface ITemplateService
    {
        FormTemplateViewModel AddFormTemplate(AddFormTemplateParamModel paramModel);

        IEnumerable<FormTemplateViewModel> GetAllFormTemplates();

        FormTemplateViewModel GetFormTemplateById(int formTemplateId);

        FormTemplateViewModel UpdateFormTemplate(UpdateFormTemplateParamModel paramModel);

        void DeleteTemplate(int formTemplateId);
    }
}
