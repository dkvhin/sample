﻿using AutoMapper;
using D7Soft.Common.Interfaces;
using D7Soft.Data.Poco.FormTemplate;
using D7Soft.Services.Forms.ParamModels;
using D7Soft.Services.Forms.ViewModels;

namespace D7Soft.Services.Forms.Infrastructure.Mappers
{
    public class FormTemplateInputMapper : ICustomMapper
    {
        public void Map(IMapperConfiguration configuration)
        {
            configuration.CreateMap<AddFormTemplateInputParamModel, AddFormTemplateInputPoco>()
                .ForMember(c => c.FormTemplateId, opt => opt.Ignore());

            configuration.CreateMap<FormTemplateInputSummaryPoco, FormTemplateInputViewModel>();
        }
    }
}
