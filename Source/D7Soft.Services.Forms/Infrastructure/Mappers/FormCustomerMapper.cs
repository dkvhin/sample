﻿using AutoMapper;
using D7Soft.Common.Interfaces;
using D7Soft.Data.Poco.FormCustomer;
using D7Soft.Services.Forms.ParamModels;
using D7Soft.Services.Forms.ViewModels;

namespace D7Soft.Services.Forms.Infrastructure.Mappers
{
    public class FormCustomerMapper : ICustomMapper
    {
        public void Map(IMapperConfiguration configuration)
        {
            configuration.CreateMap<AddFormCustomerParamModel, AddFormCustomerPoco>();
            configuration.CreateMap<UpdateFormCustomerParamModel, UpdateFormCustomerPoco>();
            configuration.CreateMap<FormCustomerPoco, FormCustomerViewModel>();
        }
    }
}
