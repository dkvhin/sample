﻿using AutoMapper;
using D7Soft.Common.Interfaces;
using D7Soft.Data.Poco.FormTemplate;
using D7Soft.Services.Forms.ParamModels;
using D7Soft.Services.Forms.ViewModels;

namespace D7Soft.Services.Forms.Infrastructure.Mappers
{
    public class FormTemplateMapper : ICustomMapper
    {
        public void Map(IMapperConfiguration configuration)
        {
            configuration.CreateMap<AddFormTemplateParamModel, AddFormTemplatePoco>()
                .ForMember(c => c.IsActive, opt => opt.Ignore());

            configuration.CreateMap<FormTemplateSummaryPoco, FormTemplateViewModel>()
                .ForMember(c => c.Inputs, opt => opt.Ignore());

            configuration.CreateMap<UpdateFormTemplateParamModel, UpdateFormTemplatePoco>();
        }
    }
}
