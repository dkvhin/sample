﻿using System;
using System.Linq;
using AutoMapper;
using D7Soft.Common.Interfaces;
using D7Soft.Services.Forms.Infrastructure.Mappers;

namespace D7Soft.Services.Forms.Infrastructure
{
    public class FormModelMapper : IMapperBootstrapper
    {
        public void Configure(IMapperConfiguration configuration)
        {
            var type = typeof(ICustomMapper);
            var types = typeof(FormTemplateInputMapper).Assembly.GetTypes()
                .Where(p => type.IsAssignableFrom(p) && !p.IsInterface).ToList();

            types.ForEach(t =>
            {
                var n = (ICustomMapper)Activator.CreateInstance(t);
                n.Map(configuration);
            });
        }
    }
}
