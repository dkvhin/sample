﻿using D7Soft.Common.Interfaces;
using D7Soft.Core.Validation;
using D7Soft.Services.Forms.ParamModels;
using D7Soft.Services.Forms.Validators;
using SimpleInjector;
namespace D7Soft.Services.Forms.Infrastructure.Modules
{
    public class FormCustomerValidatorModule : IValidatorBootstrapper
    {
        public void Configure(Container container)
        {
            container.RegisterConditional<Validator<AddFormCustomerParamModel>, AddFormCustomerValidator>(
               c => c.ImplementationType == typeof(AddFormCustomerValidator));

            container.RegisterConditional<Validator<UpdateFormCustomerParamModel>, UpdateFormCustomerValidator>(
              c => c.ImplementationType == typeof(UpdateFormCustomerValidator));
        }
    }
}
