﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using NUnit.Framework;
using ServiceStack;

namespace D7Soft.Data.DAO.Tests
{
    [TestFixture]
    public class ValidateDaoMappingsTests
    {
        [Test]
        public void We_have_data_contexts()
        {
            new DatabaseContext();
            var contexts = ReflectionHelpers.GetAllDbContexts;
            Assert.NotNull(contexts);
            Assert.Greater(contexts.Count, 0);
        }
        public static IEnumerable<TestCaseData> TestSource()
        {
            new DatabaseContext();
            return from contextType in ReflectionHelpers.GetAllDbContexts
                   from prop in contextType.GetProperties()
                   where prop.PropertyType.IsGenericType &&
                       prop.PropertyType.GetGenericTypeDefinition() == typeof(DbSet<>) &&
                       !(new[] { "" }).Contains(prop.Name)
                   select new TestCaseDataEx("{0}.{1}".FormatWith(contextType.Name, prop.Name), contextType, prop);
        }

        [TestCaseSource("TestSource")]
        public void TestGetData(Type type, PropertyInfo dbContextProperty)
        {
            var db = Activator.CreateInstance(type);
            // pull back property value
            var propertyValue = (IEnumerable)dbContextProperty.GetValue(db, null);
            object data;
            try
            {
                data = propertyValue.Cast<object>().FirstOrDefault();
            }
            catch (Exception exception)
            {
                var errorMessage = string.Format("{0} failed to get data from {1}", type, dbContextProperty.Name);
                throw new Exception(errorMessage, exception);
            }

            // now pull back each fields data
            if (data != null)
            {
                data.GetType()
                    .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                    .ToList()
                    .ForEach(x =>
                    {
                        try
                        {
                            x.GetValue(data, null);
                        }
                        catch (Exception exception)
                        {
                            var errorMessage = string.Format("{0}.{1}.{2} => failed to get property data", type.Name, dbContextProperty.Name, x.Name);
                            throw new Exception(errorMessage, exception);
                        }
                    });
            }
        }

    }
}
