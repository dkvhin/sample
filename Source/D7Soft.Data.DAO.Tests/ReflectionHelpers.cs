﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using ServiceStack;

namespace D7Soft.Data.DAO.Tests
{
    public static class ReflectionHelpers
    {
        public static List<Type> GetAllDbContexts
        {
            get
            {
                return AppDomain.CurrentDomain.GetAssemblies()
                    .SelectMany(x => x.GetTypes())
                    .Where(x => x == typeof (DatabaseContext))
                    //.Where(TypeHasAParameterlessConstructor)
                    .ToList();
            }
        }

        public static void CopyTo<T>(this T source, T target)
        {
            CopyTo<T, T>(source, target);
        }

        public static void CopyTo<T, TR>(this T source, TR target)
        {
            var sourcePublicProperties = typeof (T).GetPublicProperties();
            var targetPublicProperties = target.GetType().GetPublicProperties();

            var missingPropertiesOnTarget = sourcePublicProperties.Select(x => x.Name)
                .Except(targetPublicProperties.Select(x => x.Name))
                .ToList();
            if (missingPropertiesOnTarget.Any())
            {
                var missingString = missingPropertiesOnTarget.Join(", ");
                throw new InvalidOperationException("The following properties are missing on the target [{0}]".FormatWith(missingString));
            }

            sourcePublicProperties
                .ToList()
                .ForEach(fieldInfo =>
                {
                    var value = fieldInfo.GetValue(source);
                    target.GetType().GetProperty(fieldInfo.Name).SetValue(target, value);
                });
        }

        public static void IncrementBy<T, TR>(this T source, TR target)
        {
            var sourcePublicProperties = source.GetType().GetPublicProperties();
            var targetPublicProperties = target.GetType().GetPublicProperties();

            var missingPropertiesOnTarget = sourcePublicProperties.Select(x => x.Name)
                .Except(targetPublicProperties.Select(x => x.Name))
                .ToList();
            if (missingPropertiesOnTarget.Any())
            {
                var missingString = missingPropertiesOnTarget.Join(", ");
                throw new InvalidOperationException("The following properties are missing on the target [{0}]".FormatWith(missingString));
            }

            sourcePublicProperties
                .ToList()
                .ForEach(fieldInfo =>
                {
                    var targetProperty = target.GetType().GetProperty(fieldInfo.Name);

                    var sourceValue = fieldInfo.GetValue(source);
                    var tagetValue = targetProperty.GetValue(target);

                    targetProperty.SetValue(target, (dynamic) sourceValue + (dynamic) tagetValue);
                });
        }

        public static void ShouldHaveDefaultsForAllPropertiesIncludedOn<T, TR>(this T target, TR source)
        {
            var targetPublicProperties = target.GetType().GetPublicProperties();
            var sourcePublicProperties = source.GetType().GetPublicProperties();

            var propertiesOnTargetNotOnSource = sourcePublicProperties.Select(x => x.Name)
                .Except(targetPublicProperties.Select(x => x.Name))
                .ToList();
            if (propertiesOnTargetNotOnSource.Any())
            {
                var missingString = propertiesOnTargetNotOnSource.Join(", ");
                throw new InvalidOperationException("The following properties are missing on the target [{0}]".FormatWith(missingString));
            }

            var targetPropertiesThatHaveAlreadyBeenSet = sourcePublicProperties.Select(x => x.Name)
                .Where(sourcePropertyName =>
                {
                    var value = (dynamic) targetPublicProperties
                        .Single(x => x.Name == sourcePropertyName)
                        .GetValue(target);

                    return value != 0;
                })
                .ToList();

            if (targetPropertiesThatHaveAlreadyBeenSet.Any())
            {
                var missingString = targetPropertiesThatHaveAlreadyBeenSet.Join(", ");
                throw new InvalidOperationException("The following properties have already been set [{0}]".FormatWith(missingString));
            }
        }
    }
}