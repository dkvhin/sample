﻿using System;
using NUnit.Framework;

namespace D7Soft.Data.DAO.Tests
{
    public class TestCaseDataEx : TestCaseData
    {
        public TestCaseDataEx(string name, params object[] args) : base(args)
        {
            SetName(name);
        }

        public object Expected
        {
            set
            {
                Returns(value);
            }
        }

        public Type ThrowsType
        {
            set
            {
                if (value == null) throw new ArgumentNullException("value");
                ThrowsType = value;
            }
        }
    }
}