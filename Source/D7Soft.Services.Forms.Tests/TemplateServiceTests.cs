﻿using System.Collections.Generic;
using AutoMapper;
using D7Soft.Data.Poco.FormTemplate;
using D7Soft.Data.Repository.Interfaces;
using D7Soft.Services.Forms.Infrastructure;
using D7Soft.Services.Forms.Interfaces;
using D7Soft.Services.Forms.ParamModels;
using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;

namespace D7Soft.Services.Forms.Tests
{
    [TestFixture]
    public class TemplateServiceTests
    {
        private Mock<IFormTemplateRepository> _formTemplateRepoMock;
        private ITemplateService _templateService;
            
        [SetUp]
        public void Setup()
        {
            var mapper = new MapperConfiguration(c => c.CreateProfile("test"));
            new FormModelMapper().Configure(mapper);

            _formTemplateRepoMock = new Mock<IFormTemplateRepository>();
            _templateService = new TemplateService(_formTemplateRepoMock.Object, mapper.CreateMapper());
        }

        [Test]
        public void Can_Add_Form_Template()
        {
            var fixture = new Fixture();

            _formTemplateRepoMock.Setup(c => c.AddFormTemplate(It.IsAny<AddFormTemplatePoco>()))
                .Returns(fixture.Create<FormTemplateSummaryPoco>());

            _formTemplateRepoMock.Setup(c => c.AddFormTemplateInput(It.IsAny<AddFormTemplateInputPoco>()))
                .Returns(fixture.Create<FormTemplateInputSummaryPoco>());

            var paramModel = fixture.Create<AddFormTemplateParamModel>();

            var result = _templateService.AddFormTemplate(paramModel);

            Assert.NotNull(result);
            Assert.AreNotEqual(0, result.Id);
        }


        [Test]
        public void Can_Get_Form_Templates()
        {
            var fixture = new Fixture();

            _formTemplateRepoMock.Setup(c => c.GetAllFormTemplates())
                .Returns(fixture.Create<IEnumerable<FormTemplateSummaryPoco>>());

            _formTemplateRepoMock.Setup(c => c.GetFormTemplateInputs(It.IsAny<int>()))
                .Returns(fixture.Create<IEnumerable<FormTemplateInputSummaryPoco>>);

            var result = _templateService.GetAllFormTemplates();

            Assert.NotNull(result);
        }

        [Test]
        public void Can_Get_Form_Template_By_Id()
        {
            var fixture = new Fixture();

            _formTemplateRepoMock.Setup(c => c.GetFormTemplateById(It.IsAny<int>()))
                .Returns(fixture.Create<FormTemplateSummaryPoco>());

            _formTemplateRepoMock.Setup(c => c.GetFormTemplateInputs(It.IsAny<int>()))
                .Returns(fixture.Create<IEnumerable<FormTemplateInputSummaryPoco>>());

            var result = _templateService.GetFormTemplateById(1);
            Assert.NotNull(result);

        }


        [Test]
        public void Can_Update_Forms_Template()
        {
            var fixture = new Fixture();

            _formTemplateRepoMock.Setup(c => c.UpdateFormTemplate(It.IsAny<UpdateFormTemplatePoco>()))
                .Returns(fixture.Create<FormTemplateSummaryPoco>());

            _formTemplateRepoMock.Setup(c => c.AddFormTemplateInput(It.IsAny<AddFormTemplateInputPoco>()))
                .Returns(fixture.Create<FormTemplateInputSummaryPoco>());

            var result = _templateService.UpdateFormTemplate(fixture.Create<UpdateFormTemplateParamModel>());
            Assert.NotNull(result);
        }

        [Test]
        public void Can_Delete_Forms_Template()
        {
            _templateService.DeleteTemplate(1);
            _formTemplateRepoMock.Verify(c => c.DeleteFormTemplate(It.IsAny<int>()), Times.Once);
        }
    }
}
