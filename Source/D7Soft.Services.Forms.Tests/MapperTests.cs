﻿using AutoMapper;
using D7Soft.Services.Forms.Infrastructure;
using NUnit.Framework;

namespace D7Soft.Services.Forms.Tests
{
    [TestFixture]
    class MapperTests
    {
        [Test]
        public void All_Mappers_Are_Mapping_Correctly()
        {
            var config = new MapperConfiguration(c => c.CreateProfile("Test"));
            new FormModelMapper().Configure(config);
            config.AssertConfigurationIsValid();
        }
    }
}
