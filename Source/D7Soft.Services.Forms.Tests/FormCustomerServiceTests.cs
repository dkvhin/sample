﻿using AutoMapper;
using D7Soft.Core.Validation;
using D7Soft.Data.Poco.FormCustomer;
using D7Soft.Data.Poco.FormTemplate;
using D7Soft.Data.Repository.Interfaces;
using D7Soft.Services.Forms.Infrastructure;
using D7Soft.Services.Forms.Interfaces;
using D7Soft.Services.Forms.ParamModels;
using D7Soft.Services.Forms.Validators;
using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;
using System.Collections.Generic;
using System.Linq;

namespace D7Soft.Services.Forms.Tests
{
    [TestFixture]
    public class FormCustomerServiceTests
    {
        private Mock<IFormCustomerRepository> _formCustomerRepoMock;
        private IFormCustomerService _customerService;
        private ValidationProvider _validationProvider;

        [SetUp]
        public void Setup()
        {
            var mapper = new MapperConfiguration(c => c.CreateProfile("test"));
            new FormModelMapper().Configure(mapper);

            _validationProvider = new ValidationProvider(c => new AddFormCustomerValidator());

            _formCustomerRepoMock = new Mock<IFormCustomerRepository>();
            _customerService = new FormCustomerService(_formCustomerRepoMock.Object, mapper.CreateMapper(), _validationProvider);
        }


        [Test]
        public void Can_Add_Form_Customer()
        {
            var fixture = new Fixture();

            _formCustomerRepoMock.Setup(c => c.AddCustomer(It.IsAny<AddFormCustomerPoco>()))
                .Returns(fixture.Create<FormCustomerPoco>());

            var paramModel = fixture.Create<AddFormCustomerParamModel>();

            var result = _customerService.AddCustomer(paramModel);

            Assert.NotNull(result);
            Assert.AreNotEqual(0, result.Id);
        }

        [Test]
        public void Can_Get_Form_Customer()
        {
            var fixture = new Fixture();

            _formCustomerRepoMock.Setup(c => c.GetAll())
                .Returns(fixture.Create<IEnumerable<FormCustomerPoco>>());

            var result = _customerService.GetAll();

            Assert.NotNull(result);
            Assert.IsTrue(result.Any());
        }

        [Test]
        public void Can_Filter_Form_Customer()
        {
            var fixture = new Fixture();

            _formCustomerRepoMock.Setup(c => c.Filter(It.IsAny<string>()))
                .Returns(fixture.Create<IEnumerable<FormCustomerPoco>>());

            var result = _customerService.Filter("Sample Sample");

            Assert.NotNull(result);
            Assert.IsTrue(result.Any());
        }

        [Test]
        public void Can_Get_Form_Customer_By_Id()
        {
            var fixture = new Fixture();

            _formCustomerRepoMock.Setup(c => c.GetCustomerById(It.IsAny<int>()))
                .Returns(fixture.Create<FormCustomerPoco>());

            var result = _customerService.GetCustomerById(1);
            Assert.NotNull(result);

        }


        [Test]
        public void Can_Update_Forms_Customer()
        {
            var fixture = new Fixture();

            _formCustomerRepoMock.Setup(c => c.UpdateCustomer(It.IsAny<UpdateFormCustomerPoco>()))
                .Returns(fixture.Create<FormCustomerPoco>());

            var result = _customerService.UpdateCustomer(fixture.Create<UpdateFormCustomerParamModel>());
            Assert.NotNull(result);
        }

        [Test]
        public void Can_Delete_Forms_Customer()
        {
            _customerService.DeleteCustomer(1);
            _formCustomerRepoMock.Verify(c => c.DeleteCustomer(It.IsAny<int>()), Times.Once);
        }
    }
}
