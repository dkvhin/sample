﻿namespace D7Soft.Web.Models
{
    public class BaseModel
    {
        public string Environment { get; set; }
        public string Version { get; set; }
    }
}