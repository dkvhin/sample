﻿using System;
using System.Linq;
using AutoMapper;
using D7Soft.Common.Interfaces;
using D7Soft.Services.Accounts.Infrastructure;
using D7Soft.Services.Forms.Infrastructure;
using SimpleInjector;

namespace D7Soft.Web
{
    public static class MapperConfig
    {
        public static void Configure(Container container)
        {
            ConfigureMapper(container,
                new[]
                {
                    typeof (FormModelMapper),
                    typeof (AccountModelMapper),
                    typeof (BoostrapMapper)
                });
        }

        private static void ConfigureMapper(Container container, Type[] types)
        {
            var config = new MapperConfiguration(c => c.CreateProfile("MainMapperProfile"));

            var mapperType = typeof(IMapperBootstrapper);
            foreach (var type in types)
            {
                var newTypes = type.Assembly.GetTypes()
                    .Where(p => mapperType.IsAssignableFrom(p) && !p.IsInterface).ToList();

                newTypes.ForEach(t =>
                {
                    var n = (IMapperBootstrapper)Activator.CreateInstance(t);
                    n.Configure(config);
                });
            }

            container.RegisterSingleton(config.CreateMapper());
        }
    }
}