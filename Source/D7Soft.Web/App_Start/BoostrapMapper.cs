﻿using System;
using System.Linq;
using AutoMapper;
using D7Soft.Common.Interfaces;

namespace D7Soft.Web
{
    public class BoostrapMapper : IMapperBootstrapper
    {
        public void Configure(IMapperConfiguration configuration)
        {
            var type = typeof(ICustomMapper);
            var types = typeof(RouteConfig).Assembly.GetTypes()
                .Where(p => type.IsAssignableFrom(p) && !p.IsInterface).ToList();

            types.ForEach(t =>
            {
                var n = (ICustomMapper)Activator.CreateInstance(t);
                n.Map(configuration);
            });
        }
    }
}