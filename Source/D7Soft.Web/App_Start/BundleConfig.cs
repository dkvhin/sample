﻿using System.Collections.Generic;
using System.Web;
using System.Web.Optimization;

namespace D7Soft.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            var orderer = new PassthruBundleOrderer();
            var styleBundle = new StyleBundle("~/bundles/css")
                .Include("~/Content/css/bootstrap.css", new CssRewriteUrlTransform())
                .Include("~/Content/css/style.css", new CssRewriteUrlTransform())
                .Include("~/Content/css/animate.css", new CssRewriteUrlTransform())
                .Include("~/Content/css/font-awesome.css", new CssRewriteUrlTransform())
                .Include("~/Content/css/angular-block-ui.css", new CssRewriteUrlTransform())
            .Include("~/Content/css/ui-grid.css", new CssRewriteUrlTransform());
            styleBundle.Orderer = orderer;
            bundles.Add(styleBundle);
        }
    }

    class PassthruBundleOrderer : IBundleOrderer
    {
        public IEnumerable<BundleFile> OrderFiles(BundleContext context, IEnumerable<BundleFile> files)
        {
            return files;
        }
    }
}
