﻿using System;
using System.Linq;
using System.Web.Http;
using D7Soft.Web;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;
using SimpleInjector;
using D7Soft.Web.Infrastructure.DependencyInjection;
using Microsoft.Owin.Security.Infrastructure;
using SimpleInjector.Integration.WebApi;

[assembly: OwinStartup(typeof(Startup))]
namespace D7Soft.Web
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration config = new HttpConfiguration();
            WebApiConfig.Register(config);

            var container = new Container();

            container.Options.DefaultScopedLifestyle = new WebApiRequestLifestyle();

            Configure(container);

            MapperConfig.Configure(container);

            container.RegisterWebApiControllers(GlobalConfiguration.Configuration);

            container.Verify();

            GlobalConfiguration.Configuration.DependencyResolver =
                new SimpleInjectorWebApiDependencyResolver(container);
            app.UseOAuthAuthorizationServer(new OAuthAuthorizationServerOptions
            {
                AllowInsecureHttp = true,
                ApplicationCanDisplayErrors = true,
                TokenEndpointPath = new PathString("/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(20),
                Provider =  container.GetInstance<IOAuthAuthorizationServerProvider>(),
                RefreshTokenProvider = container.GetInstance<IAuthenticationTokenProvider>()
            });

            
            // token consumption
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
        }

        public void Configure(Container container)
        {
            var type = typeof(IInjectionBootstrapper);
            var types = typeof(IInjectionBootstrapper).Assembly.GetTypes()
                .Where(p => type.IsAssignableFrom(p) && !p.IsInterface).ToList();

            types.ForEach(t =>
            {
                var n = (IInjectionBootstrapper)Activator.CreateInstance(t);
                n.Configure(container);
            });
        }
    }
}