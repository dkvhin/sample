﻿using System.Linq;
using D7Soft.Data.Repository.Interfaces;
using SimpleInjector;
namespace D7Soft.Web.Infrastructure.DependencyInjection
{
    public class RepositoryBootstrap : IInjectionBootstrapper
    {
        public void Configure(Container container)
        {
            var repositoryAssembly = typeof(IUserAuthenticationRepository).Assembly;

            var registrations =
                from type in repositoryAssembly.GetExportedTypes()
                where type.Namespace == "D7Soft.Data.Repository"
                where type.GetInterfaces().Any()
                select new { Service = type.GetInterfaces().Single(), Implementation = type };

            foreach (var reg in registrations)
                container.Register(reg.Service, reg.Implementation, Lifestyle.Transient);
        }
    }
}