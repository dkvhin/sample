﻿using D7Soft.Core.Validation;
using D7Soft.Services.Accounts.Infrastructure.Modules;
using SimpleInjector;
using System;
using System.Linq;
using D7Soft.Common.Interfaces;
using D7Soft.Services.Forms.Infrastructure.Modules;

namespace D7Soft.Web.Infrastructure.DependencyInjection
{
    public class ValidationBootstrap : IInjectionBootstrapper
    {
        public void Configure(Container container)
        {

            var types = new[]
            {
                typeof (AccountUserValidatorModule),
                typeof(FormCustomerValidatorModule)
            };

            Configure(container, types);
        }

        private void Configure(Container container, Type[] types)
        {
            container.RegisterSingleton(typeof(IValidationProvider), new ValidationProvider(type => (IValidator)container.GetInstance(typeof(Validator<>).MakeGenericType(type))));
            container.RegisterConditional(typeof(Validator<>), typeof(NullValidator<>), c => c.ImplementationType == typeof(NullValidator<>));

            var mapperType = typeof(IValidatorBootstrapper);
            foreach (var type in types)
            {
                var newTypes = type.Assembly.GetTypes()
                    .Where(p => mapperType.IsAssignableFrom(p) && !p.IsInterface).ToList();

                newTypes.ForEach(t =>
                {
                    var n = (IValidatorBootstrapper)Activator.CreateInstance(t);
                    n.Configure(container);
                });
            }

        }
    }
}