﻿using D7Soft.Data.DAO;
using SimpleInjector;

namespace D7Soft.Web.Infrastructure.DependencyInjection
{
    public class DatabaseBootstrap : IInjectionBootstrapper
    {
        public void Configure(Container container)
        {
            container.RegisterPerWebRequest<IDatabaseContext, DatabaseContext>();
        }
    }
}