﻿using System.Linq;
using D7Soft.Services.Forms.Interfaces;
using SimpleInjector;
using System;
using D7Soft.Services.Accounts.Interfaces;

namespace D7Soft.Web.Infrastructure.DependencyInjection
{
    public class ServiceBootstrap : IInjectionBootstrapper
    {
        public void Configure(Container container)
        {
            var types = new[]
            {
                typeof (ITemplateService), 
                typeof (IAccountUserService)
            };

            configure(container, types);
        }

        private void configure(Container container, Type[] types)
        {
            foreach (var typ in types)
            {
                var repositoryAssembly = typ.Assembly;
                var registrations =
                   from type in repositoryAssembly.GetExportedTypes()
                   where type.Name.EndsWith("Service")
                   where type.GetInterfaces().Any()
                   select new { Service = type.GetInterfaces().Single(), Implementation = type };

                foreach (var reg in registrations)
                    container.Register(reg.Service, reg.Implementation, Lifestyle.Transient);
            }
        
        }
    }
}