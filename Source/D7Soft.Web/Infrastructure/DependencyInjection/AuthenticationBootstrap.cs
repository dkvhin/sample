﻿using D7Soft.Authentication;
using Microsoft.Owin.Security.Infrastructure;
using Microsoft.Owin.Security.OAuth;

namespace D7Soft.Web.Infrastructure.DependencyInjection
{
    public class AuthenticationBootstrap : IInjectionBootstrapper
    {
        public void Configure(SimpleInjector.Container container)
        {
            container.Register<IPasswordHasher, PasswordHasher>();

            container.Register<IOAuthAuthorizationServerProvider, CustomOAuthAuthorizationProvider>();
            container.Register<IAuthenticationTokenProvider, CustomOAuthRefreshTokenProvider>();
        }
    }
}