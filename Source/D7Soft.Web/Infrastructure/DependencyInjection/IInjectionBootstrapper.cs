﻿using SimpleInjector;

namespace D7Soft.Web.Infrastructure.DependencyInjection
{
    public interface IInjectionBootstrapper
    {
        void Configure(Container container);
    }
}