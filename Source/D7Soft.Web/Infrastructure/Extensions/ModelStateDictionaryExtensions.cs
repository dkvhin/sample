﻿using D7Soft.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web.Http.ModelBinding;
using D7Soft.Core.Validation;

namespace D7Soft.Web.Infrastructure.Extensions
{
    public static class ModelStateDictionaryExtensions
    {
        /// <summary>
        /// Removes the specified member from the  <see cref="ModelStateDictionary"/>.
        /// </summary>
        /// <param name="me">Me.</param>
        /// <param name="lambdaExpression">The lambda expression.</param>
        /// <param name="model">The prefix of the modelstate</param>
        public static void Remove<TViewModel>(
            this ModelStateDictionary me,
            Expression<Func<TViewModel, object>> lambdaExpression)
        {
            me.Remove(lambdaExpression.GetPropertyName());
        }



        /// <summary>
        /// Adds the model error.
        /// </summary>
        /// <typeparam name="TViewModel">The type of the view model.</typeparam>
        /// <param name="me">Me.</param>
        /// <param name="lambdaExpression">The lambda expression.</param>
        /// <param name="errorMessage">The error message.</param>
        public static void AddModelError<TViewModel>(
            this ModelStateDictionary me,
            Expression<Func<TViewModel, object>> lambdaExpression,
            string errorMessage)
        {
            me.AddModelError(lambdaExpression.GetPropertyName(), errorMessage);
        }

        /// <summary>
        /// Adds the model error.
        /// </summary>
        /// <typeparam name="TViewModel">The type of the view model.</typeparam>
        /// <param name="me">Me.</param>
        /// <param name="lambdaExpression">The lambda expression.</param>
        /// <param name="exception">The exception.</param>
        public static void AddModelError<TViewModel>(
            this ModelStateDictionary me,
            Expression<Func<TViewModel, object>> lambdaExpression,
            Exception exception)
        {
            me.AddModelError(lambdaExpression.GetPropertyName(), exception);
        }

        public static Dictionary<string, IEnumerable<string>> ToErrorDictionary(
            this System.Web.Http.ModelBinding.ModelStateDictionary modelState, string model = "model",
            bool camelCaseKeyName = true)
        {
            var errors = modelState
              .Where(x => x.Value.Errors.Any())
              .ToDictionary(
                kvp => CamelCasePropNames(kvp.Key, model),
                kvp => kvp.Value.Errors.Select(e => e.ErrorMessage)
              );

            return errors;
        }

        private static string CamelCasePropNames(string propName, string model)
        {
            var array = propName.Split('.');
            var camelCaseList = new string[array.Length];
            for (var i = 0; i < array.Length; i++)
            {
                var prop = array[i];
                camelCaseList[i] = prop.Substring(0, 1).ToLower() + prop.Substring(1, prop.Length - 1);
            }

            var firstItem = camelCaseList.FirstOrDefault();
            if (!string.IsNullOrEmpty(firstItem) && String.Equals(firstItem, model, StringComparison.CurrentCultureIgnoreCase))
                camelCaseList = camelCaseList.Where((val, idx) => idx != 0).ToArray();

            return string.Join(".", camelCaseList);
        }

        /// <summary>
        /// Removes the specified member from the  <see cref="ModelStateDictionary"/>.
        /// </summary>
        /// <param name="me">Me.</param>
        /// <param name="lambdaExpression">The lambda expression.</param>
        /// <param name="prefix"></param>
        public static void Remove<TViewModel>(
            this System.Web.Http.ModelBinding.ModelStateDictionary me,
            Expression<Func<TViewModel, object>> lambdaExpression, string prefix = "model")
        {
            me.Remove(prefix.TrimEnd('.') + "." + lambdaExpression.GetPropertyName());
            me.Remove(lambdaExpression.GetPropertyName());
        }

        public static void AddModelError<TViewModel>(
            this System.Web.Http.ModelBinding.ModelStateDictionary me,
            Expression<Func<TViewModel, object>> lambdaExpression,
            Exception exception, string prefix = "model")
        {
            me.AddModelError(prefix.TrimEnd('.') + "." + lambdaExpression.GetPropertyName(), exception);
        }



        public static void AddModelError<TViewModel>(
            this System.Web.Http.ModelBinding.ModelStateDictionary me,
            Expression<Func<TViewModel, object>> lambdaExpression,
            string errorMessage, string prefix = "model")
        {
            me.AddModelError(prefix.TrimEnd('.') + "." + lambdaExpression.GetPropertyName(), errorMessage);
        }

        public static void AddModelError(
           this System.Web.Http.ModelBinding.ModelStateDictionary me,
           string key,
           string errorMessage, string prefix = "model")
        {
            me.AddModelError(prefix.TrimEnd('.') + "." + key, errorMessage);
        }

        public static void AddErrors(this ModelStateDictionary modelStateDictionary, ValidationException exception, string prefix = "model")
        {
            foreach (var error in exception.Errors)
            {
                var key = prefix.TrimEnd('.') + "." + error.Key;
                if (modelStateDictionary.ContainsKey(key))
                    modelStateDictionary[key].Errors.Add(error.Message);
                else
                    modelStateDictionary.AddModelError(key, error.Message);
            }
               
        }
    }
}