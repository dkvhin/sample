﻿using System.Web.Mvc;

namespace D7Soft.Web.Areas.Payroll
{
    public class PayrollAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Payroll";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
               "Payroll_default",
               "payroll/{controller}/{action}/{id}",
               new { action = "Index", controller = "Home", id = UrlParameter.Optional },
               namespaces: new[] { "D7Soft.Web.Areas.Payroll.Controllers" }
           );
        }
    }
}