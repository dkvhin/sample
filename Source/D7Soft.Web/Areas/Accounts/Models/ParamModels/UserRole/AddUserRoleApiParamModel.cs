﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace D7Soft.Web.Areas.Accounts.Models.ParamModels.UserRole
{
    public class AddUserRoleApiParamModel : IValidatableObject
    {
        
        [Required]
        public int? UserId { get; set; }
        public List<int> RoleIds { get; set; }

        public AddUserRoleApiParamModel()
        {
            RoleIds = new List<int>();
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (RoleIds.Count == 0)
                yield return new ValidationResult("You need to supply at least one role id", new[] {"RoleIds"});
        }
    }
}