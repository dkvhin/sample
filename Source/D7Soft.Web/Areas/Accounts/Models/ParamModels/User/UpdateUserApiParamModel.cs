﻿using System.ComponentModel.DataAnnotations;
namespace D7Soft.Web.Areas.Accounts.Models.ParamModels.User
{
    public class UpdateUserApiParamModel
    {
        [Required]
        public int? Id { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string Username { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

    }
}