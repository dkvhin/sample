﻿using System.ComponentModel.DataAnnotations;
namespace D7Soft.Web.Areas.Accounts.Models.ParamModels.User
{
    public class AddUserApiParamModel
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string Username { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }
    }
}