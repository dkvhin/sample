﻿using System.ComponentModel.DataAnnotations;

namespace D7Soft.Web.Areas.Accounts.Models.ParamModels.User
{
    public class UpdateUserPasswordApiParamModel
    {
        [Required]
        public int? Id { get; set; }

        [Required]
        [MinLength(4)]
        public string Password { get; set; }

        [Required]
        [MinLength(4)]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }
    }
}