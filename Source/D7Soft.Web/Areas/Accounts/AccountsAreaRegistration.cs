﻿using System.Web.Http;
using System.Web.Mvc;

namespace D7Soft.Web.Areas.Accounts
{
    public class AccountsAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Accounts";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.Routes.MapHttpRoute(
               name: "API Area Default",
               routeTemplate: "accounts/api/{controller}/{id}",
               defaults: new { id = RouteParameter.Optional }
           );

            context.MapRoute(
                "Accounts_default",
                "accounts/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "D7Soft.Web.Areas.Accounts.Controllers" }
            );
        }
    }
}