﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using D7Soft.Services.Accounts.Interfaces;
using D7Soft.Services.Accounts.ParamModels;
using D7Soft.Web.Areas.Accounts.Models.ParamModels.UserRole;
using D7Soft.Web.Infrastructure.Extensions;

namespace D7Soft.Web.Areas.Accounts.Controllers.Api.V1
{
    [Authorize(Roles = "super_admin,accounts_admin")]
    [RoutePrefix("accounts/api/roles")]
    public class AccountRoleApiController : ApiController
    {
        private readonly IAccountUserService _accountUserService;
        private readonly IMapper _mapper;
        public AccountRoleApiController(IAccountUserService accountUserService, IMapper mapper)
        {
            _accountUserService = accountUserService;
            _mapper = mapper;
        }

        [HttpGet]
        [Route("getall")]
        public HttpResponseMessage GetAll(int? userId)
        {
            if (userId.HasValue)
            {
                var result = _accountUserService.GetAllRoleGroupWithRoles(userId.Value);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }

            return Request.CreateResponse(HttpStatusCode.NotFound);
        }

        [HttpPost]
        [Route("add")]
        public HttpResponseMessage Add([FromBody] AddUserRoleApiParamModel model)
        {
            if (ModelState.IsValid)
            {
                var paramModel = _mapper.Map<AddUserRolesParamModel>(model);
                var result = _accountUserService.AddUserRoles(paramModel);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState.ToErrorDictionary());
        }
    }
}
