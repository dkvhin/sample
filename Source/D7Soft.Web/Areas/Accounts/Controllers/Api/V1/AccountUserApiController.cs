﻿using System.Net;
using System.Web.Http;
using D7Soft.Services.Accounts.Interfaces;
using System.Net.Http;
using AutoMapper;
using D7Soft.Core.Validation;
using D7Soft.Services.Accounts.ParamModels;
using D7Soft.Web.Areas.Accounts.Models.ParamModels.User;
using D7Soft.Web.Infrastructure.Extensions;

namespace D7Soft.Web.Areas.Accounts.Controllers.Api.V1
{
    [Authorize(Roles = "super_admin,accounts_admin")]
    [RoutePrefix("accounts/api/users")]
    public class AccountUserApiController : ApiController
    {
        private readonly IAccountUserService _accountUserService;
        private readonly IMapper _mapper;
        public AccountUserApiController(IAccountUserService accountUserService, IMapper mapper)
        {
            _accountUserService = accountUserService;
            _mapper = mapper;
        }

        [HttpGet]
        [Route("getall")]
        public HttpResponseMessage GetAll()
        {
            var result = _accountUserService.GetAllUsers();
            return Request.CreateResponse(HttpStatusCode.OK, result); 
        }

        [HttpPost]
        [Route("add")]
        public HttpResponseMessage AddUse([FromBody] AddUserApiParamModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var paramModel = _mapper.Map<AddUserParamModel>(model);
                    var result = _accountUserService.AddUser(paramModel);
                    return Request.CreateResponse(HttpStatusCode.OK, result); 
                }
                catch (ValidationException ex)
                {
                    ModelState.AddErrors(ex);
                }
               
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState.ToErrorDictionary());
        }

        [HttpGet]
        [Route("get")]
        public HttpResponseMessage GetUser(int? id)
        {
            if (id.HasValue)
            {
                var result = _accountUserService.GetUserDetailsById(id.Value);
                return result == null
                    ? Request.CreateResponse(HttpStatusCode.NotFound)
                    : Request.CreateResponse(HttpStatusCode.OK, result);
            }

            return Request.CreateResponse(HttpStatusCode.NotFound);
        }

        [HttpPut]
        [Route("update")]
        public HttpResponseMessage Update([FromBody] UpdateUserApiParamModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var paramModel = _mapper.Map<UpdateUserParamModel>(model);
                    var result = _accountUserService.UpdateUser(paramModel);
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }
                catch (ValidationException ex)
                {
                    ModelState.AddErrors(ex);
                }
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState.ToErrorDictionary());
        }

        [HttpPut]
        [Route("changepassword")]
        public HttpResponseMessage ChangePassword([FromBody] UpdateUserPasswordApiParamModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var paramModel = _mapper.Map<UpdateUserPasswordParamModel>(model);
                    var result = _accountUserService.UpdateUserPassword(paramModel);
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }
                catch (ValidationException ex)
                {
                    ModelState.AddErrors(ex);
                }
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState.ToErrorDictionary());
        }
    }
}
