﻿using AutoMapper;
using D7Soft.Common.Interfaces;
using D7Soft.Services.Accounts.ParamModels;
using D7Soft.Web.Areas.Accounts.Models.ParamModels.UserRole;

namespace D7Soft.Web.Areas.Accounts.Infrastructure.Mappers
{
    public class AccountUserRoleMapper : ICustomMapper
    {
        public void Map(IMapperConfiguration configuration)
        {
            configuration.CreateMap<AddUserRoleApiParamModel, AddUserRolesParamModel>();
        }
    }
}