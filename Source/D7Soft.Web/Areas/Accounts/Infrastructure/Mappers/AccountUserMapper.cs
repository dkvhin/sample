﻿using AutoMapper;
using D7Soft.Common.Interfaces;
using D7Soft.Services.Accounts.ParamModels;
using D7Soft.Web.Areas.Accounts.Models.ParamModels.User;

namespace D7Soft.Web.Areas.Accounts.Infrastructure.Mappers
{
    public class AccountUserMapper : ICustomMapper
    {
        public void Map(IMapperConfiguration configuration)
        {
            configuration.CreateMap<AddUserApiParamModel, AddUserParamModel>();
            configuration.CreateMap<UpdateUserApiParamModel, UpdateUserParamModel>();
            configuration.CreateMap<UpdateUserPasswordApiParamModel, UpdateUserPasswordParamModel>();
        }
    }
}