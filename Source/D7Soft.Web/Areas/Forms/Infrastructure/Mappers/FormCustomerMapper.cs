﻿using AutoMapper;
using D7Soft.Common.Interfaces;
using D7Soft.Services.Forms.ParamModels;
using D7Soft.Web.Areas.Forms.Models.ParamModels.FormCustomer;

namespace D7Soft.Web.Areas.Forms.Infrastructure.Mappers
{
    public class FormCustomerMapper : ICustomMapper
    {
        public void Map(IMapperConfiguration configuration)
        {
            configuration.CreateMap<AddFormCustomerApiParamModel, AddFormCustomerParamModel>();
            configuration.CreateMap<UpdateFormCustomerApiParamModel, UpdateFormCustomerParamModel>();
        }
    }
}