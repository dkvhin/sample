﻿using AutoMapper;
using D7Soft.Common.Interfaces;
using D7Soft.Services.Forms.ParamModels;
using D7Soft.Web.Areas.Forms.Models.ParamModels.FormTemplate;

namespace D7Soft.Web.Areas.Forms.Infrastructure.Mappers
{
    public class FormTemplateMapper : ICustomMapper
    {
        public void Map(IMapperConfiguration configuration)
        {
            configuration.CreateMap<AddFormTemplateApiParamModel, AddFormTemplateParamModel>();
            configuration.CreateMap<AddFormTemplateInputApiParamModel, AddFormTemplateInputParamModel>();

            configuration.CreateMap<UpdateFromTemplateApiParamModel, UpdateFormTemplateParamModel>();
            configuration.CreateMap<AddFormTemplateInputApiParamModel, UpdateFormTemplateInputParamModel>();
        }
    }
}