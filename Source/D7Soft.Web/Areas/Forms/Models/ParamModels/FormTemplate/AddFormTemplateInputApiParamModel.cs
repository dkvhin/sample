﻿using System.ComponentModel.DataAnnotations;

namespace D7Soft.Web.Areas.Forms.Models.ParamModels.FormTemplate
{
    public class AddFormTemplateInputApiParamModel
    {
        [Required]
        public int? Width { get; set; }

        [Required]
        public int? PositionX { get; set; }

        [Required]
        public int? PositionY { get; set; }
        public bool IsNumeric { get; set; }
        public bool HasConvertInput { get; set; }
        public bool IsConvert { get; set; }
        public int FontSize { get; set; }
        public bool IsDate { get; set; }
        public bool IsAutoComplete { get; set; }
    }
}