﻿using System.ComponentModel.DataAnnotations;
namespace D7Soft.Web.Areas.Forms.Models.ParamModels.FormTemplate
{
    public class UpdateFromTemplateApiParamModel : AddFormTemplateApiParamModel
    {
        [Required]
        public int? Id { get; set; }
    }
}