﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace D7Soft.Web.Areas.Forms.Models.ParamModels.FormTemplate
{
    public class AddFormTemplateApiParamModel
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public int? Width { get; set; }

        [Required]
        public int? Height { get; set; }

        [Required]
        public string Image { get; set; }

        public List<AddFormTemplateInputApiParamModel> Inputs { get; set; }
    }
}