﻿using System.ComponentModel.DataAnnotations;
namespace D7Soft.Web.Areas.Forms.Models.ParamModels.FormCustomer
{
    public class UpdateFormCustomerApiParamModel : AddFormCustomerApiParamModel
    {
        [Required]
        public int? Id { get; set; }
    }
}