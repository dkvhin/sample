﻿using System.ComponentModel.DataAnnotations;

namespace D7Soft.Web.Areas.Forms.Models.ParamModels.FormCustomer
{
    public class AddFormCustomerApiParamModel
    {
        [StringLength(150)]
        public string FirstName { get; set; }

        [StringLength(150)]
        public string LastName { get; set; }

        [StringLength(150)]
        public string MiddleName { get; set; }

        [StringLength(250)]
        public string Company { get; set; }

        [StringLength(50)]
        public string ContactNumber { get; set; }

        [StringLength(250)]
        public string Address { get; set; }
    }
}