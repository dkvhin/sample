﻿using System.Web.Http;
using System.Web.Mvc;

namespace D7Soft.Web.Areas.Forms
{
    public class FormsAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Forms";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.Routes.MapHttpRoute(
                name: "API Area Default Forms",
                routeTemplate: "forms/api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            context.MapRoute(
                "Forms_default",
                "forms/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "D7Soft.Web.Areas.Forms.Controllers" }
            );
        }
    }
}