﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using D7Soft.Services.Forms.Interfaces;
using AutoMapper;
using D7Soft.Services.Forms.ParamModels;
using D7Soft.Web.Areas.Forms.Models.ParamModels.FormCustomer;
using D7Soft.Web.Infrastructure.Extensions;

namespace D7Soft.Web.Areas.Forms.Controllers.Api.V1
{
    [Authorize(Roles = "super_admin,form_admin")]
    [RoutePrefix("forms/api/customer")]
    public class FormCustomerApiController : ApiController
    {
        private readonly IFormCustomerService _customerService;
        private readonly IMapper _mapper;
        public FormCustomerApiController(IFormCustomerService customerService, IMapper mapper)
        {
            _customerService = customerService;
            _mapper = mapper;
        }

        [HttpPost]
        [Route("add", Name = "AddFormCustomerV1")]
        public HttpResponseMessage AddFormCustomer([FromBody] AddFormCustomerApiParamModel model)
        {
            if (ModelState.IsValid)
            {
                var paramModel = _mapper.Map<AddFormCustomerParamModel>(model);
                var result = _customerService.AddCustomer(paramModel);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState.ToErrorDictionary());
        }

        [HttpPut]
        [Route("update", Name = "UpdateFormCustomerV1")]
        public HttpResponseMessage UpdateFormCustomer([FromBody] UpdateFormCustomerApiParamModel model)
        {
            if (ModelState.IsValid)
            {
                var paramModel = _mapper.Map<UpdateFormCustomerParamModel>(model);
                var result = _customerService.UpdateCustomer(paramModel);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState.ToErrorDictionary());
        }

        [HttpDelete]
        [Route("delete", Name = "DeleteFormCustomerV1")]
        public HttpResponseMessage DeleteFormCustomer(int id)
        {
            var item = _customerService.GetCustomerById(id);

            if (item != null)
            {
                _customerService.DeleteCustomer(item.Id);
                return Request.CreateResponse(HttpStatusCode.OK);
            }

            return Request.CreateResponse(HttpStatusCode.NotFound, ModelState.ToErrorDictionary());
        }


        [HttpGet]
        [Route("get", Name = "GetFormCustomerV1")]
        public HttpResponseMessage GetFormCustomer(int id)
        {
            var result = _customerService.GetCustomerById(id);

            if (result != null)
                return Request.CreateResponse(HttpStatusCode.OK, result);

            return Request.CreateResponse(HttpStatusCode.NotFound);
        }

        [HttpGet]
        [Route("getall", Name = "GetAllFormCustomerV1")]
        public HttpResponseMessage GetAllFormCustomers()
        {
            var result = _customerService.GetAll();
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [HttpGet]
        [Route("filter", Name = "FilterFormCustomerV1")]
        public HttpResponseMessage Filter(string keyword)
        {
            var result = _customerService.Filter(keyword);
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
    }
}
