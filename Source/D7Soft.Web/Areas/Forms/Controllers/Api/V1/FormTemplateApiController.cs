﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using D7Soft.Services.Forms.Interfaces;
using D7Soft.Web.Areas.Forms.Models.ParamModels.FormTemplate;
using D7Soft.Web.Infrastructure.Extensions;
using AutoMapper;
using D7Soft.Services.Forms.ParamModels;

namespace D7Soft.Web.Areas.Forms.Controllers.Api.V1
{
    [Authorize(Roles = "super_admin,form_admin")]
    [RoutePrefix("forms/api/templates")]
    public class FormTemplateApiController : ApiController
    {
        private readonly ITemplateService _templateService;
        private readonly IMapper _mapper;
        public FormTemplateApiController(ITemplateService templateService, IMapper mapper)
        {
            _templateService = templateService;
            _mapper = mapper;
        }

        [HttpPost]
        [Route("add", Name = "AddTemplateV1")]
        public HttpResponseMessage AddTemplate([FromBody] AddFormTemplateApiParamModel model)
        {
            if (ModelState.IsValid)
            {
                var paramModel = _mapper.Map<AddFormTemplateParamModel>(model);
                var result = _templateService.AddFormTemplate(paramModel);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState.ToErrorDictionary());
        }

        [HttpPut]
        [Route("update", Name = "UpdateTemplateV1")]
        public HttpResponseMessage UpdateTemplate([FromBody] UpdateFromTemplateApiParamModel model)
        {
            if (ModelState.IsValid)
            {
                var paramModel = _mapper.Map<UpdateFormTemplateParamModel>(model);
                var result = _templateService.UpdateFormTemplate(paramModel);
                return Request.CreateResponse(HttpStatusCode.OK, result); 
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState.ToErrorDictionary());
        }

        [HttpDelete]
        [Route("delete", Name = "DeleteTemplateV1")]
        public HttpResponseMessage DeleteTemplate(int id)
        {
            var item = _templateService.GetFormTemplateById(id);

            if (item != null)
            {
                _templateService.DeleteTemplate(item.Id);
                return Request.CreateResponse(HttpStatusCode.OK);
            }

            return Request.CreateResponse(HttpStatusCode.NotFound, ModelState.ToErrorDictionary());
        }

        [HttpGet]
        [Route("get", Name = "GetTemplateV1")]
        public HttpResponseMessage GetTemplate(int id)
        {
            var result = _templateService.GetFormTemplateById(id);

            if (result != null)
                return Request.CreateResponse(HttpStatusCode.OK, result);

            return Request.CreateResponse(HttpStatusCode.NotFound);
        }

        [HttpGet]
        [Route("getall", Name = "GetAllTemplateV1")]
        public HttpResponseMessage GetAllTemplates()
        {
            var result = _templateService.GetAllFormTemplates();
            return Request.CreateResponse(HttpStatusCode.OK, result); 
        }
    }
}
