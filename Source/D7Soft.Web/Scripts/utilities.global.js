﻿var require = {
    baseUrl: "/scripts"
};

var d7 = {
    cookieName: 'd7Authorization',
    environment: 'local',
    version: '1.0.0',
    lower: function (text) {
        return text.toLowerCase();
    },
    isLive: function () {
        return d7.environment === 'prd';
    },
    checkMinified: function (path) {
        if (d7.environment === 'prd' && path.indexOf('.min') === -1) {
            return path + '.min';
        }

        return path;
    },
    checkMinifiedHtml: function (path) {
        if (d7.environment === 'prd') {
            return path + '.min.html?v=' + d7.version;
        }

        return path + '.html?' + new Date().getTime();
    },
    path: {
        vendor: function (file) {
            return d7.checkMinified('vendors/' + file);
        },
        plugin: function (file) {
            return d7.checkMinified('vendors/plugins/' + file);
        },
        service: function (file, area) {
            var buildPath = 'app/';

            if (area) {
                buildPath += 'areas/' + area + '/';
            }

            return d7.checkMinified(buildPath + 'services/' + file + '.service');
        },
        filter: function (file) {
            return d7.checkMinified('app/filters/' + file + '.filter');
        },
        config: function (file) {
            return d7.checkMinified('app/config/' + file + '.config');
        },
        directiveTemplate: function (file, area) {
            var buildPath = '/scripts/app/';

            if (area) {
                buildPath += 'areas/' + area + '/';
            }
            return d7.checkMinifiedHtml(buildPath + 'directives/' + file + '.directive');
        },
        directive: function (file, area) {

            var buildPath = 'app/';

            if (area) {
                buildPath += 'areas/' + area + '/';
            }
            return d7.checkMinified(buildPath + 'directives/' + file + '.directive');
        },
        modules: function (file) {
            return d7.checkMinified('app/modules/' + file + '.module');
        },
        model: function (file) {
            return d7.checkMinified('app/models/' + file + '.model');
        },
        constant: function (file) {
            return d7.checkMinified('app/constants/' + file + '.constant');
        },
        routes: function (file, area) {
            var buildPath = 'app/';

            if (area) {
                buildPath += 'areas/' + area + '/';
            }

            return d7.checkMinified(buildPath + file + '.routes');
        },
        controller: function (file, section, area) {
            var buildPath = 'app/';

            if (area) {
                buildPath += 'areas/' + area + '/';
            }

            buildPath += 'controllers/';

            if (section) {
                buildPath += section + '/';
            }
            return d7.checkMinified(buildPath + file + '.controller');

        },
        view: function (file, section, area) {
            var buildPath = '/scripts/app/';

            if (area) {
                buildPath += 'areas/' + area + '/';
            }

            buildPath += 'views/';

            if (section) {
                buildPath += section + '/';
            }
            return d7.checkMinifiedHtml(buildPath + file);
        }
    }
};