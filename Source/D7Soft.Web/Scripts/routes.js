﻿"use strict";

define([
    'defaultRoutes',
    'formsRoutes',
    'accountsRoutes'],
    function (defaultRoutes, formsRoutes, accountsRoutes) {

        var routes = [defaultRoutes, formsRoutes, accountsRoutes];

        return routes;
    });