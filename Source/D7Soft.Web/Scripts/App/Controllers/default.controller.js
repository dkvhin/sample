﻿"use strict";

define(['app-root'], function (app) {

    var controller = d7.lower('defaultController');
    app.register.controller(controller, ['$scope', '$rootScope', '$log', 'alertBox', 'authenticationService', '$state',
        function ($scope, $rootScope, $log, alertBox, authenticationService, $state) {
            $log.debug('defaultController - Loaded');

            $scope.logout = function (e) {
                e.preventDefault();
                authenticationService.logout();
                $state.go('site.login');
            };
        }]);
});


