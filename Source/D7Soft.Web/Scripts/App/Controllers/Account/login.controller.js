﻿"use strict";

define(['app-root', 'authenticationService'], function (app) {
    var controller = d7.lower('accountLoginController');

    app.register.controller(controller, ['$scope', '$rootScope', '$state', '$log', '$stateParams', 'blockUI', 'authenticationService', 'alertBox',
        function ($scope, $rootScope, $state, $log, $stateParams, blockUI, authenticationService, alertBox) {
            $log.debug('accountLoginController - Loaded');



            $scope.form = {
                
            };

            $scope.loginSubmit = function() {
                $log.log('accountLoginController - Login triggered');


                blockUI.start({ message: 'Signing in...' });

                var data = {
                    username: $scope.form.username,
                    password: $scope.form.password
                };

                authenticationService.login(data).then(function (response) {
                    blockUI.stop();
                    if ($rootScope.toState && $rootScope.toState.name !== 'login') {
                        $state.go($rootScope.toState, $rootScope.toStateParams);
                    } else {
                        $state.go('site.forms.templates.list');
                    }
                    
                }, function(err) {
                    blockUI.stop();
                    alertBox.show({ message: err.error_description, 'type': 'error' });
                });
            };

        }]);
});


