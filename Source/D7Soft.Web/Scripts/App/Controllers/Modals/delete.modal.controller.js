﻿"use strict";

define(['app-root', 'authenticationService'], function (app) {
    var controller = d7.lower('modalDeleteController');

    app.register.controller(controller, ['$scope', '$uibModalInstance', '$rootScope', '$log', 'params',
        function ($scope, $uibModalInstance, $rootScope, $log, params) {
            $log.debug('modalDeleteController - Loaded');

            $scope.title = params.title;
            $scope.message = params.message;


            $scope.delete = function() {
                $uibModalInstance.close();
            };

            $scope.cancel = function() {
                $uibModalInstance.dismiss();
            };

        }]);
});


