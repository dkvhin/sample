﻿"use strict";

define(['app-root'], function (app) {

    var controller = d7.lower('accessDeniedController');
    app.register.controller(controller, ['$scope', '$rootScope', '$log', 'alertBox', 'authenticationService', '$state',
        function ($scope, $rootScope, $log, alertBox, authenticationService, $state) {
            $log.debug('accessDeniedController - Loaded');

            $scope.logout = function (event) {
                event.preventDefault();
                authenticationService.logout();
                $state.go('login');
            };
        }]);
});


