﻿"use strict";

define(['angularAMD'], function (angularAMD) {
    return {
        configure: function ($stateProvider) {

            $stateProvider
                .state("login", angularAMD.route({
                    url: '^/account/login',
                    templateUrl: d7.path.view('login', 'account'),
                    controllerUrl: d7.path.controller('login', 'account'),
                    controller: d7.lower('accountLoginController')
                }))
                .state('site', {
                    'abstract': true,
                    resolve: {
                        authorize: [
                            'authorization',
                            function(authorization) {
                                return authorization.authorize();
                            }
                        ]
                    },
                    template: '<div ui-view />'
                })
                .state("site.default", angularAMD.route({
                    url: '^/',
                    templateUrl: d7.path.view('default'),
                    controllerUrl: d7.path.controller('default'),
                    controller: d7.lower('defaultController')
                }))
                .state("site.accessdenied", angularAMD.route({
                    url: '^/access-denied',
                    templateUrl: d7.path.view('accessdenied'),
                    controllerUrl: d7.path.controller('accessdenied'),
                    controller: d7.lower('accessDeniedController')
                }));

        }
    }
});

