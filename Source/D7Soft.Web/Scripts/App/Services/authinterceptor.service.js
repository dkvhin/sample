﻿"use strict";

define(['angularAMD'], function (angularAMD) {
    angularAMD.factory('authInterceptorService', [
        '$q', '$log', '$rootScope', '$injector', 'localStorageService',
        function ($q, $log, $rootScope, $injector, localStorageService) {
            return {
                'request': function(config) {
                    
                    config.headers = config.headers || {};
                    config.headers['X-Requested-With'] = 'XMLHttpRequest';
                    var authData = localStorageService.cookie.get(d7.cookieName);

                    // We will ignore html files to avoid extra request payload
                    if (authData && config.url.indexOf('.html') === -1) {
                        $log.log('AuthInterceptor - Append Bearer Token');
                        config.headers.Authorization = 'Bearer ' + authData.token;
                    }

                    return config;
                },
                'response': function(response) {
                    // do something on success
                    return response;
                }
            }
        }
    ]);
});