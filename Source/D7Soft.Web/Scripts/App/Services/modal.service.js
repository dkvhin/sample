﻿/*
 * Sample usage
    modalService.delete({
        title: 'Warning!', // this is optional, default value is 'Warning!'
        message: 'Are you sure you want to delete?', //this is optional, default value is 'Are you sure you want to delete?'
        size : 'md', // Optiional, default value is 'md'
        onSuccess : function(response) {
            $log.log('success');
        },
        onCancel : function(response) {
            $log.log('canclled');
        }
    });
 */

"use strict";
define(['angularAMD'], function (angularAMD) {

    angularAMD.service('modalService', ['$log', '$uibModal', 'blockUI', function ($log, $uibModal, blockUI) {
        var self = this;

        self.delete = function (params) {
            /// <summary>
            /// Open a delete modal window
            /// contains several properties :
            /// {
            ///     title : optional, default value is 'Warning!'
            ///     message : optional, default value is 'Are you sure you want to delete?'
            ///     size : optional, default value is 'md'
            ///     onSuccess : required, this is the callback method when the user clicked the delete button.
            ///     onCancel : required, callback when the user clicked the cancel button.
            /// }
            ///</summary>
            /// <param name="params" type="JsonObject"></param>

            blockUI.start();
            var modalInstance = $uibModal.open(angularAMD.route({
                animation: true,
                backdrop: 'static',
                templateUrl: d7.path.view('delete.modal', 'modals'),
                controllerUrl: d7.path.controller('delete.modal', 'modals'),
                controller: d7.lower('modalDeleteController'),
                size: params.size ? params.size : 'md',
                resolve: {
                    params: function () {
                        return {
                            title: params.title ? params.title : 'Warning!',
                            message: params.message ? params.message : 'Are you sure you want to delete?'
                        };
                    }
                }
            }));

            modalInstance.opened.then(function () {
                blockUI.stop();
            });

            modalInstance.result.then(params.onSuccess, params.onCancel);
        };

    }]);
});


