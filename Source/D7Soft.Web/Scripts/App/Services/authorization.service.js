﻿"use strict";
define(['angularAMD', 'authenticationService'], function (angularAMD) {

    angularAMD.factory('authorization', ['$rootScope', '$state', '$log', 'authenticationService', '$q', 'blockUI',
        function ($rootScope, $state, $log, authenticationService, $q, blockUI) {

            return {
                authorize: function () {
                    return authenticationService.identity();
                }
            }

        }]);
});


