﻿"use strict";
define(['angularAMD'], function (angularAMD) {

    angularAMD.service('authenticationService', ['$http', '$q', '$log', 'localStorageService', function ($http, $q, $log, localStorageService) {
        var self = this;
        self._indentity = undefined;

        self.configData = {
            clientId: '18d55035-c3b4-43c8-b723-e9bf787e5db1',
            clientSecret: 'd3duoIRTJxAyekhgbtstjQT58mAquGF5mC40gVwZw9FEb8h5Tvm90MAGsvCA1yabryZQP/jSYLqsz3Oo8pQXwjyIzBCpsG/IxDdX0W9nnh0LPCsmPCIeQUVJCY6SwcdYhLp3/VIoyXspCm7ABR3E7/YTgUX+aY1erXsff2du+xVJ0Y4NPdKcSYF8yu3AE4zqrcySB4xPgkm5TXKbO2xoiq09AXVMRQID9y+qvgFcy3ZU5cEoc2u2HRSoHEars0/soLIhlnFF2AE='
        };

        self.authenticationData = {};

        self.fillAuthData = function () {
            $log.log('authenticationService - Fill Auth Data');
            self.authenticationData = localStorageService.cookie.get(d7.cookieName);
        }();
        

        var transform = function (data) {
            var str = [];
            for (var p in data)
                if (data.hasOwnProperty(p)) str.push(encodeURIComponent(p) + "=" + encodeURIComponent(data[p]));
            return str.join("&");
        }

        self.isIdentityResolved = function() {
            return angular.isDefined(self._identity);
        };

        self.isInRole = function(role) {
            if (!self.authenticationData || !self.authenticationData.roles) return false;

            return self.authenticationData.roles.indexOf(role) !== -1;
        };

        self.isInAnyRole = function(roles) {
            if (!self.authenticationData || !self.authenticationData.roles) return false;

            for (var i = 0; i < roles.length; i++) {
                if (this.isInRole(roles[i])) return true;
            }

            return false;
        };

        self.isAuthenticated = function () {
            return !!self.authenticationData;
        };

        self.authentication = function() {
            return self.authenticationData;
        };

        self.refreshAccessToken = function (refreshToken, username, rememberMe) {

            var data = {
                grant_type: 'refresh_token',
                refresh_token: refreshToken,
                client_id: self.configData.clientId,
                client_secret: self.configData.clientSecret
            };

            var deferred = $q.defer();

            $http.post('/token', data, {
                transformRequest: transform,
                headers:
                { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).success(function (response) {

                var expiration = new Date();
                expiration.setSeconds(expiration.getSeconds() + response.expires_in);

                var authDataVar = {
                    id: response.userId,
                    token: response.access_token,
                    refreshToken: response.refresh_token,
                    expiration: expiration,
                    username: username,
                    rememberMe: rememberMe,
                    roles : response.roles.split(',')
                };

                localStorageService.cookie.set(d7.cookieName, authDataVar, expiration);

                self.authenticationData = authDataVar;

                deferred.resolve(response);
            }).error(function (err, status) {
                deferred.resolve(err);
                deferred.reject(err);
            });

            return deferred.promise;
        };

        self.logout = function() {
            localStorageService.cookie.remove(d7.cookieName);
            self.authenticationData = null;
        };

        self.login = function (data) {
            $log.log('authenticationService - Login');
            var loginData = {
                grant_type: 'password',
                username: data.username,
                password: data.password,
                client_id: self.configData.clientId,
                client_secret: self.configData.clientSecret
            };

            var deferred = $q.defer();
            $http.post('/token', loginData, {
                transformRequest: transform,
                headers:
                { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).success(function (response) {

                if (response.error) {
                    $log.warn('authenticationService - Login Failed');
                    deferred.resolve(response);
                    return;
                }

                $log.log('authenticationService - Login Success');

                var expiration = new Date();
                expiration.setSeconds(expiration.getSeconds() + response.expires_in);

                var expiry = 0;
                if (data.rememberMe) {
                    expiry = 5;
                }
                $log.log(expiry);

                var authDataVar = {
                    id : response.userId,
                    token: response.access_token,
                    refreshToken: response.refresh_token,
                    expiration: expiration,
                    username: loginData.username,
                    rememberMe: loginData.rememberMe,
                    roles: response.roles.split(',')
                };

                localStorageService.cookie.set(d7.cookieName, authDataVar, expiry);

                self.authenticationData = authDataVar;

                deferred.resolve(response);
            }).error(function (result) {
                $log.warn('authenticationService - Login Failed');
                deferred.reject(result);
            });

            return deferred.promise;
        };

        self.identity = function(force) {
            var deferred = $q.defer();

            if (force === true) self._identity = undefined;
            $q.when(angular.noop).then(function() {
                self._identity = null;
                deferred.resolve(self._identity);
            });
            
            return deferred.promise;
        };

        self.logout = function() {
            localStorageService.cookie.remove(d7.cookieName);
            self.authenticationData = null;
        };
    }]);
});


