﻿define(['app-root'], function (app) {

    var alertBox = angular.module('alertBox', []);

    alertBox.run(["$document", function ($document) {
        $document.find('body').attr('alert-box', '');
    }]);

    alertBox.factory('alertBox', ['$document', function ($document) {
        var self = this;
        self.status = null;
        self.message = null;
        self.type = null;

        self.show = function (config) {
            if (config) {
                self.status = 'show';
                self.message = config.message;
                self.type = config.type;
            }
        };

        self.close = function () {
            self.status = null;
            self.message = null;
            self.type = null;
        };

        return self;
    }]);


    alertBox
    .directive('alertBoxContent', function() {
        return {
            scope: true,
            restrict: 'A',
            template: "<div ng-hide=\"!closed && !show\" ng-class=\"{'fadeOutUp':!show && closed}\" class=\"loading-general top {{backgroundColor}} animated fadeInDown clearfix\"><div class=\"message-room clearfix\"><div class=\"icon-room\"><i class=\"fa {{icon}}\"></i></div><span class=\"message\">{{message}}</span></div><a href=\"#\" ng-click=\"close($event)\" class=\"close-message\"><i class=\"fa fa-times\"></i></a></div>",
            controller: ['$scope', 'alertBox', function ($scope, alertBox) {
                
                $scope.alertBox = alertBox;
                $scope.closed = false;

                $scope.backgroundColor = 'info';
                $scope.icon = 'fa-check';

                var getInfo = function (type) {
                    switch (type) {
                        case 'success':
                            return { bg: 'success', icon: 'fa-check' };
                        case 'warning':
                            return { bg: 'warning', icon: 'fa-warning' };
                        case 'error':
                            return { bg: 'error', icon: 'fa-exclamation' };
                        default:
                            return { bg: 'info', icon: 'fa-info-circle' };
                    }
                };

                $scope.$watch('alertBox.status', function () {
                    $scope.show = !!$scope.alertBox.status;
                    $scope.message = $scope.alertBox.message;
                    $scope.type = $scope.alertBox.type;
                    var info = getInfo($scope.type);
                    $scope.backgroundColor = info.bg;
                    $scope.icon = info.icon;
                });

                $scope.close = function ($e) {
                    $e.preventDefault();
                    $scope.closed = true;
                    $scope.alertBox.status = null;

                };
            }]
        };
    })
    .directive('alertBox', [
        'alertBoxCompileFn',
        function (alertBoxCompileFn) {
            return {
                scope: true,
                restrict: 'A',
                compile: alertBoxCompileFn
            };
        }
    ]).factory('alertBoxCompileFn', [function() {
            return function($element, $attrs) {
                $element.append('<div alert-box-content></div>');
            };
        }
    ]);
});