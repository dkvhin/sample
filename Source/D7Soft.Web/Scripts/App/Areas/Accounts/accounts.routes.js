﻿"use strict";

define(['angularAMD'], function (angularAMD) {
    return {
        configure: function ($stateProvider) {

            var area = 'accounts';

            var defaultRoles = ['super_admin', 'accounts_admin'];

            $stateProvider
                .state("site.accounts", angularAMD.route({
                    url: '^/accounts',
                    abstract:true,
                    templateUrl: d7.path.view('default', null, area),
                    controllerUrl: d7.path.controller('default', null, area),
                    controller: d7.lower('accountsDefaultController'),
                    data: {
                        roles: defaultRoles
                    }
                }))
                /* User Module */
                .state("site.accounts.user", angularAMD.route({
                    url: '/users',
                    abstract: true,
                    templateUrl: d7.path.view('user', 'user', area),
                    controllerUrl: d7.path.controller('user', 'user', area),
                    controller: d7.lower('accountsUserController'),
                    data: {
                        roles: defaultRoles.concat(['accounts_user_moderator'])
                    }
                }))
                .state("site.accounts.user.list", angularAMD.route({
                    url: '/list',
                    title : 'Accounts : User : List',
                    templateUrl: d7.path.view('list', 'user', area),
                    controllerUrl: d7.path.controller('list', 'user', area),
                    controller: d7.lower('accountsUserListController'),
                    data: {
                        roles: defaultRoles
                    }
                }))
                .state("site.accounts.user.add", angularAMD.route({
                    url: '/add',
                    title: 'Accounts : User : Add',
                    templateUrl: d7.path.view('add', 'user', area),
                    controllerUrl: d7.path.controller('add', 'user', area),
                    controller: d7.lower('accountsUserAddController'),
                    data: {
                        roles: defaultRoles
                    }
                }))
                .state("site.accounts.user.role", angularAMD.route({
                    url: '/roles?id',
                    params: { id: null },
                    title: 'Accounts : User : Roles',
                    templateUrl: d7.path.view('role', 'user', area),
                    controllerUrl: d7.path.controller('role', 'user', area),
                    controller: d7.lower('accountsUserRoleController'),
                    data: {
                        roles: defaultRoles
                    }
                }))
                .state("site.accounts.user.edit", angularAMD.route({
                    url: '/edit?id',
                    params: { id: null },
                    title: 'Accounts : User : Edit',
                    templateUrl: d7.path.view('edit', 'user', area),
                    controllerUrl: d7.path.controller('edit', 'user', area),
                    controller: d7.lower('accountsUserEditController'),
                    data: {
                        roles: defaultRoles
                    }
                }))
                .state("site.accounts.user.password", angularAMD.route({
                    url: '/password?id',
                    params: { id: null },
                    title: 'Accounts : User : Change Password',
                    templateUrl: d7.path.view('password', 'user', area),
                    controllerUrl: d7.path.controller('password', 'user', area),
                    controller: d7.lower('accountsUserPasswordController'),
                    data: {
                        roles: defaultRoles
                    }
                }));
                ;
        }
    }
});

