﻿"use strict";

define(['app-root', 'accountUserService'], function (app) {
    var controller = d7.lower('accountsUserAddController');

    app.register.controller(controller, ['$scope', '$rootScope', '$state', '$log', '$stateParams','accountUserService','blockUI','alertBox',
        function ($scope, $rootScope, $state, $log, $stateParams, accountUserService, blockUI, alertBox) {
            $log.debug('accountsUserAddController - Loaded');


            $scope.form = {
            
            };

            $scope.formSubmit = function() {
                alertBox.close();

                if (!$scope.userForm.$valid) {
                    alertBox.show({ 'message': 'Kindly check the fields for errors or missing info.', 'type': 'warning' });
                    return;
                }

                blockUI.start();
                accountUserService.addUser($scope.form).then(function (response) {
                    blockUI.stop();
                    $log.log('accountsUserAddController - User added');
                    $state.go('site.accounts.user.role', { id: response.id });
                    alertBox.show({ 'message': 'User succesfully added.', 'type': 'success' });
                }, function (err) {
                    blockUI.stop();
                    $log.warn('accountsUserAddController - Failed to add user');
                    alertBox.show({ 'message': 'Failed to add user. Please try again.', 'type': 'warning' });
                });

            };

            $scope.cancel = function() {
                $state.go('site.accounts.user.list');
            };

        }]);
});


