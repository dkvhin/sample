﻿"use strict";

define(['app-root', 'ngload!ngGrid', 'accountUserService'], function (app) {
    var controller = d7.lower('accountsUserListController');

    app.register.controller(controller, ['$scope', '$rootScope', '$state', '$log', '$stateParams', 'blockUI', 'alertBox','accountUserService',
        function ($scope, $rootScope, $state, $log, $stateParams, blockUI, alertBox, accountUserService) {
            $log.debug('accountsUserListController - Loaded');

            $scope.gridOptions = {
                data: [],
                rowSelection: true,
                columnDefs: [
                    { field: 'username', displayName: 'Username' },
                    { field: 'firstName', displayName: 'First Name' },
                    { field: 'lastName', displayName: 'Last Name' },
                    { field: 'dateCreated', displayName: 'Date Created' },
                    { field: 'dateModified', displayName: 'Date Modified' },
                    {
                        field: 'action',
                        displayName: 'Action',
                        width:200,
                        cellTemplate: "<div class='ui-grid-cell-contents'>" +
                                        "<a href='#' class='btn btn-sm btn-success small-btn' ui-sref=\"site.accounts.user.edit({id:row.entity.id})\"><i class='fa fa-pencil'></i> Edit</a>" +
                                        " <a href='#' class='btn btn-sm btn-default small-btn' title='Change Roles' ui-sref=\"site.accounts.user.role({id:row.entity.id})\"><i class='fa fa-users'></i> Roles</a>" +
                                        " <a href='#' class='btn btn-sm btn-info small-btn' title='Change Password' ui-sref=\"site.accounts.user.password({id:row.entity.id})\"><i class='fa fa-lock'></i> Pass</a>" +
                                        "</div>"
                    }
                ]
            };

            $scope.refreshGrid = function () {
                blockUI.start();
                accountUserService.getAllUsers().then(function (response) {
                    blockUI.stop();
                    $log.log('accountsUserListController - Users Loaded');
                    $scope.gridOptions.data = response;
                }, function (err) {
                    $log.warn('accountsUserListController - Failed to load users');
                });

            }();

        }]);
});


