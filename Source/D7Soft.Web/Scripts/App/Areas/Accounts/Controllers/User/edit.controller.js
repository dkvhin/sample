﻿"use strict";

define(['app-root', 'accountUserService'], function (app) {
    var controller = d7.lower('accountsUserEditController');

    app.register.controller(controller, ['$scope', '$rootScope', '$state', '$log', '$stateParams', 'accountUserService', 'blockUI', 'alertBox',
        function ($scope, $rootScope, $state, $log, $stateParams, accountUserService, blockUI, alertBox) {
            $log.debug('accountsUserEditController - Loaded');


            $scope.form = {

            };
            $scope.user = {};
            $scope.getData = function() {

                blockUI.start();
                accountUserService.getById($stateParams.id).then(function (response) {
                    blockUI.stop();
                    $log.log('accountsUserEditController - User Loaded');
                    $scope.form = {
                        id : response.id,
                        firstName: response.firstName,
                        lastName: response.lastName,
                        username: response.username,
                        email: response.email
                    };
                    $scope.user = response;
                }, function (err) {
                    $log.warn('accountsUserEditController - Failed to load user');
                });


            }();

            $scope.cancel = function () {
                $state.go('site.accounts.user.list');
            };

            $scope.formSubmit = function () {
                alertBox.close();

                if (!$scope.userForm.$valid) {
                    alertBox.show({ 'message': 'Kindly check the fields for errors or missing info.', 'type': 'warning' });
                    return;
                }

                blockUI.start();
                accountUserService.updateUser($scope.form).then(function (response) {
                    blockUI.stop();
                    $log.log('accountsUserEditController - User Updated');
                    $state.go('site.accounts.user.list');
                    alertBox.show({ 'message': 'User succesfully Updated.', 'type': 'success' });
                }, function (err) {
                    blockUI.stop();
                    $log.warn('accountsUserEditController - Failed to update user');
                    alertBox.show({ 'message': 'Failed to update user. Please try again.', 'type': 'warning' });
                });

            };


        }]);
});


