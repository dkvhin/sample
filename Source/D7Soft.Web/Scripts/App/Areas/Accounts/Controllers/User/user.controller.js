﻿"use strict";

define(['app-root'], function (app) {
    var controller = d7.lower('accountsUserController');

    app.register.controller(controller, ['$scope', '$rootScope', '$state', '$log', '$stateParams',
        function ($scope, $rootScope, $state, $log, $stateParams) {
            $log.debug('accountsUserController - Loaded');

        }]);
});


