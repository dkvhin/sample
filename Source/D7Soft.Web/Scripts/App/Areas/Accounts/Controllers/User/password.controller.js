﻿"use strict";

define(['app-root', 'accountUserService'], function (app) {
    var controller = d7.lower('accountsUserPasswordController');

    app.register.controller(controller, ['$scope', '$rootScope', '$state', '$log', '$stateParams', 'accountUserService', 'blockUI', 'alertBox',
        function ($scope, $rootScope, $state, $log, $stateParams, accountUserService, blockUI, alertBox) {
            $log.debug('accountsUserPasswordController - Loaded');


            $scope.form = {

            };

            $scope.user = {};

            $scope.getData = function () {

                blockUI.start();
                accountUserService.getById($stateParams.id).then(function (response) {
                    blockUI.stop();
                    $log.log('accountsUserPasswordController - User Loaded');
                    $scope.form = {
                        id: response.id
                    };

                    $scope.user = response;
                }, function (err) {
                    $log.warn('accountsUserPasswordController - Failed to load user');
                    $state.go('site.accounts.user.list');
                });


            }();

            $scope.cancel = function () {
                $state.go('site.accounts.user.list');
            };

            $scope.formSubmit = function () {
                alertBox.close();

                if (!$scope.userForm.$valid) {
                    alertBox.show({ 'message': 'Kindly check the fields for errors or missing info.', 'type': 'warning' });
                    return;
                }

                blockUI.start();
                accountUserService.updatePassword($scope.form).then(function (response) {
                    blockUI.stop();
                    $log.log('accountsUserPasswordController - User Password Updated');
                    $state.go('site.accounts.user.list');
                    alertBox.show({ 'message': 'Succesfully Updated user password.', 'type': 'success' });
                }, function (err) {
                    blockUI.stop();
                    $log.warn('accountsUserPasswordController - Failed to update user password');
                    alertBox.show({ 'message': 'Failed to update user password. Please try again.', 'type': 'warning' });
                });

            };


        }]);
});


