﻿"use strict";

define(['app-root', 'accountRoleService', 'accountUserService'], function (app) {
    var controller = d7.lower('accountsUserRoleController');

    app.register.controller(controller, ['$scope', 'alertBox', '$state', '$log', '$stateParams','accountRoleService','blockUI','$filter','accountUserService',
        function ($scope, alertBox, $state, $log, $stateParams, accountRoleService, blockUI, $filter, accountUserService) {
            $log.debug('accountsUserRoleController - Loaded');

            $scope.roleGroups = [];
            $scope.user = {};
            $scope.refreshList = function () {
                blockUI.start();
                accountRoleService.getAllRoleGroups($stateParams.id).then(function (response) {
                    blockUI.stop();
                    $log.log('accountsUserRoleController - User roles Loaded');
                    $scope.roleGroups = response;
                }, function (err) {
                    $log.warn('accountsUserRoleController - Failed to load user roles');
                });

                blockUI.start();
                accountUserService.getById($stateParams.id).then(function (response) {
                    blockUI.stop();
                    $log.log('accountsUserEditController - User Loaded');
                    $scope.user = response;
                }, function (err) {
                    $log.warn('accountsUserEditController - Failed to load user');
                });


            }();


            var getRolesToAdd = function() {
                var rolesToAdd = [];

                for (var i = 0; i < $scope.roleGroups.length; i++) {
                    var result = $filter('filter')($scope.roleGroups[i].roles, { selected: true });
                    if (result.length > 0) {
                        rolesToAdd = rolesToAdd.concat(result);
                    }
                }

                return rolesToAdd;
            };

            $scope.formSubmit = function () {
                alertBox.close();

                var result = getRolesToAdd();
                if (result.length === 0) {
                    alertBox.show({ message: 'Please select at least one role.', type: 'warning' });
                    return;
                }

                var data = {
                    userId: $stateParams.id,
                    roleIds : []
                };

                angular.forEach(result, function(value) {
                    this.push(value.id);
                }, data.roleIds);

                blockUI.start();
                accountRoleService.addUserRoles(data).then(function (response) {
                    blockUI.stop();
                    $log.log('accountsUserRoleController - User Role Added');
                    $state.go('site.accounts.user.list');
                    alertBox.show({ 'message': 'Successfully updated user role', 'type': 'success' });
                }, function (err) {
                    $log.warn('accountsUserRoleController - Failed to add user role');
                    alertBox.show({ 'message': 'Failed to update user role. Please try again.', 'type': 'warning' });
                });
            };

            $scope.cancel = function () {
                $state.go('site.accounts.user.list');
            };

        }]);
});


