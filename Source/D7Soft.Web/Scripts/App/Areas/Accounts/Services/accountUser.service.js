﻿"use strict";
define(['app-root'], function (app) {

    app.register.service('accountUserService', ['$http', '$q', '$log', function ($http, $q, $log) {

        this.getAllUsers = function () {
            var deferred = $q.defer();
            $http.get('/accounts/api/users/getall').success(function (response) {
                deferred.resolve(response);
            }).error(function (err, status) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.addUser = function (data) {
            var deferred = $q.defer();
            $http.post('/accounts/api/users/add', data).success(function (response) {
                deferred.resolve(response);
            }).error(function (err, status) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.getById = function(id) {
            var deferred = $q.defer();
            $http.get('/accounts/api/users/get', { params: { id: id } }).success(function(response) {
                deferred.resolve(response);
            }).error(function(err, status) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.updateUser = function (data) {
            var deferred = $q.defer();
            $http.put('/accounts/api/users/update', data).success(function (response) {
                deferred.resolve(response);
            }).error(function (err, status) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.updatePassword = function (data) {
            var deferred = $q.defer();
            $http.put('/accounts/api/users/changepassword', data).success(function (response) {
                deferred.resolve(response);
            }).error(function (err, status) {
                deferred.reject(err);
            });
            return deferred.promise;
        };
    }]);
});


