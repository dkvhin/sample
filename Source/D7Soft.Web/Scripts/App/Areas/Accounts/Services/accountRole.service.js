﻿"use strict";
define(['app-root'], function (app) {

    app.register.service('accountRoleService', ['$http', '$q', '$log', function ($http, $q, $log) {

        this.getAllRoleGroups = function (userId) {
            var deferred = $q.defer();
            $http.get('/accounts/api/roles/getall', { params: { userId : userId } }).success(function (response) {
                deferred.resolve(response);
            }).error(function (err, status) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.addUserRoles = function(data) {
            var deferred = $q.defer();
            $http.post('/accounts/api/roles/add', data).success(function (response) {
                deferred.resolve(response);
            }).error(function (err, status) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

    }]);
});


