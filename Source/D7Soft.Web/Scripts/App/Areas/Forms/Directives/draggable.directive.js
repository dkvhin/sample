﻿"use strict";

define(['app-root'], function (app) {

    app.register.directive('draggable', ['$document', '$log', function ($document, $log) {
        return {
            restrict: 'A',
            replace: true,
            scope: {
                ngModel: '=',
                onDelete: '=',
                onRemove: '=',
                index: '=',
                onConvert: '='
            },

            templateUrl : d7.path.directiveTemplate('draggable','forms'),
            link: function (scope, elm, attrs) {

                var startX, startY, initialMouseX, initialMouseY, maxX, maxY;
                elm.css({
                    position: 'absolute',
                    top: !!scope.ngModel.positionY ? scope.ngModel.positionY + 'px' : 0,
                    left: !!scope.ngModel.positionX ? scope.ngModel.positionX + 'px' : 0
                });

                angular.element(elm.find('input')[0]).css({
                    'text-align': scope.ngModel.isNumeric ? 'right' : 'left',
                    'border-color': scope.ngModel.isNumeric ? '#0066ff' : scope.ngModel.isConvert ? '#009933' : '#ff3300',
                    'width': !!scope.ngModel.width ? scope.ngModel.width + 'px' : '35px',
                    'font-size': !!scope.ngModel.fontSize ? scope.ngModel.fontSize + 'px' : '20px'
                });

                if (!scope.ngModel.fontSize) {
                    scope.ngModel.fontSize = 20;
                }
                

                var parent = elm.parent();

                elm.find('input').bind('mousedown', function ($event) {
                    maxX = parent[0].clientWidth;
                    maxY = parent[0].clientHeight;                    
                    startX = elm.prop('offsetLeft');
                    startY = elm.prop('offsetTop');
                    initialMouseX = $event.clientX;
                    initialMouseY = $event.clientY;
                    $document.bind('mousemove', mousemove);
                    $document.bind('mouseup', mouseup);
                    return false;
                });

                var resizeX, resizeY;

                var tbW = elm.find('input')[0].clientWidth,
                    tbH = 0;

                angular.element(elm[0].querySelector('.resize')).bind('mousedown', function (event) {
                    event.preventDefault();

                    var domElement = elm.find('input')[0];
                    var rect = domElement.getBoundingClientRect();
                    resizeX = event.screenX;
                    resizeY = event.screenY;
                    console.log(resizeX + " " + resizeY);

                    $document.bind('mousemove', resize);
                    $document.bind('mouseup', mouseup2);
                    event.cancelBubble = true;
                });

                var ab = false;
                function resize($event) {
                    if (ab == false) {
                        $log.log("resizing");
                        ab = true;
                    }

                    tbH += $event.screenY - resizeY;
                    resizeY = $event.screenY;
                    tbW += $event.screenX - resizeX;
                    resizeX = $event.screenX;
                    scope.$apply(function () {
                        angular.element(elm.find('input')[0]).css({
                            width: tbW + 'px'
                        });
                        scope.ngModel.width = tbW;
                    });
                };

                function mouseup2($event) {
                    $document.unbind('mousemove', resize);
                    $document.unbind('mouseup', mouseup2);
                    ab = false;
                }

                function mousemove($event) {
                    
                    var dx = $event.clientX - initialMouseX;
                    var dy = $event.clientY - initialMouseY;

                    var top = startY + dy;
                    var left = startX + dx;

                    if (top > -1 && top <= maxY - elm[0].clientHeight) {
                        elm.css({
                            top: startY + dy + 'px'
                        });

                        scope.ngModel.positionY = startY + dy;
                    }
                    if (left > -1 && left <= maxX - elm[0].clientWidth) {
                        elm.css({
                            left: startX + dx + 'px'
                        });

                        scope.ngModel.positionX = startX + dx;
                    }

                    return false;
                }

                function mouseup() {
                    $document.unbind('mousemove', mousemove);
                    $document.unbind('mouseup');
                }

                function getTitle() {
                  
                    if (scope.ngModel.isNumeric) {
                        return 'Numeric';
                    }

                    if (scope.ngModel.isDate) {
                        return 'Date';
                    }

                    if (scope.ngModel.isConvert) {
                        return 'Text Convert';
                    }


                    return 'Text';
                };

                scope.ngModel.title = getTitle();

                scope.increaseFont = function () {
                    var input = angular.element(elm.find('input')[0]);
                    var fontSize = input.css('font-size').replace('px', '');
                    var newFont = parseInt(fontSize) + 1;
                    scope.ngModel.fontSize = newFont;
                    input.css({
                        'font-size': newFont + 'px'
                    });
                };

                scope.decreaseFont = function () {
                    var input = angular.element(elm.find('input')[0]);
                    var fontSize = input.css('font-size').replace('px', '');

                    if (fontSize < 5) {
                        return;
                    }

                    var newFont = parseInt(fontSize) - 1;
                   
                    scope.ngModel.fontSize = newFont;

                    input.css({
                        'font-size': newFont + 'px'
                    });
                };
            }
        };
    }]);
});


