﻿"use strict";

define(['angularAMD'], function (angularAMD) {
    return {
        configure: function ($stateProvider) {

            var area = 'forms';

            var defaultRoles = ['super_admin', 'forms_admin'];

            $stateProvider
                .state("site.forms", angularAMD.route({
                    url: '^/forms',
                    abstract: true,
                    templateUrl: d7.path.view('default', null, area),
                    controllerUrl: d7.path.controller('default', null, area),
                    controller: d7.lower('formsDefaultController'),
                    data: {
                        roles: defaultRoles
                    }
                }))
                /* Template Module */
                .state("site.forms.templates", angularAMD.route({
                    url: '/templates',
                    abstract:true,
                    templateUrl: d7.path.view('template', 'template', area),
                    controllerUrl: d7.path.controller('template', 'template', area),
                    controller: d7.lower('formsTemplateController'),
                    data: {
                        roles: defaultRoles
                    }
                }))
                .state("site.forms.templates.list", angularAMD.route({
                    url: '/list',
                    title : 'Forms : Templates : List',
                    templateUrl: d7.path.view('list', 'template', area),
                    controllerUrl: d7.path.controller('list', 'template', area),
                    controller: d7.lower('formsTemplateListController'),
                    data: {
                        roles: defaultRoles
                    }
                }))
                .state("site.forms.templates.add", angularAMD.route({
                    url: '/add',
                    title: 'Forms : Templates : Add',
                    templateUrl: d7.path.view('add', 'template', area),
                    controllerUrl: d7.path.controller('add', 'template', area),
                    controller: d7.lower('formsTemplateAddController'),
                    data: {
                        roles: defaultRoles.concat(['forms_editor'])
                    }
                }))
                .state("site.forms.templates.edit", angularAMD.route({
                    url: '/edit?id',
                    title: 'Forms : Templates : Edit',
                    templateUrl: d7.path.view('edit', 'template', area),
                    controllerUrl: d7.path.controller('edit', 'template', area),
                    controller: d7.lower('formsTemplateEditController'),
                    data: {
                        roles: defaultRoles.concat(['forms_editor'])
                    }
                }))
                .state("site.forms.templates.print", angularAMD.route({
                    url: '/print?id',
                    title: 'Forms : Templates : Print',
                    templateUrl: d7.path.view('print', 'template', area),
                    controllerUrl: d7.path.controller('print', 'template', area),
                    controller: d7.lower('formsTemplatePrintController'),
                    data: {
                        roles: defaultRoles.concat(['forms_editor','forms_printer'])
                    }
                }))
                .state("site.forms.customers", angularAMD.route({
                    url: '/customer',
                    abstract: true,
                    title: 'Forms : Customer',
                    templateUrl: d7.path.view('customer', 'customer', area),
                    controllerUrl: d7.path.controller('customer', 'customer', area),
                    controller: d7.lower('formsCustomerController'),
                    data: {
                        roles: defaultRoles.concat(['forms_editor', 'forms_printer'])
                    }
                }))
                .state("site.forms.customers.list", angularAMD.route({
                    url: '/list',
                    title: 'Forms : Customer : List',
                    templateUrl: d7.path.view('list', 'customer', area),
                    controllerUrl: d7.path.controller('list', 'customer', area),
                    controller: d7.lower('formsCustomerListController'),
                    data: {
                        roles: defaultRoles.concat(['forms_editor', 'forms_printer'])
                    }
                }))
            ;
        }
    }
});

