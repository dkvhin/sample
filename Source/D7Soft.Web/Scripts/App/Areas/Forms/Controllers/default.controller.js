﻿"use strict";

define(['app-root', 'authenticationService'], function (app) {

    var controller = d7.lower('formsDefaultController');
    app.register.controller(controller, ['$scope', '$rootScope', '$log', 'alertBox', '$state', 'authenticationService',
        function ($scope, $rootScope, $log, alertBox, $state, authenticationService) {
            $log.debug('formsDefaultController - Loaded');


            $scope.logout = function(event) {
                event.preventDefault();
                authenticationService.logout();
                $state.go('login');
            };

        }]);
});


