﻿"use strict";

define(['app-root', 'ngload!ngGrid', 'formsCustomerService', 'formsModalService'], function (app) {
    var controller = d7.lower('formsCustomerListController');

    app.register.controller(controller, ['$scope', '$rootScope', '$state', '$log', '$stateParams', 'blockUI', 'alertBox', 'Upload', 'formsCustomerService', 'formsModalService', 'modalService',
        function ($scope, $rootScope, $state, $log, $stateParams, blockUI, alertBox, Upload, formsCustomerService, formsModalService, modalService) {
            $log.debug('formsCustomerListController - Loaded');

            $scope.gridOptions = {
                data: [],
                rowSelection: true,
                columnDefs: [
                    { field: 'firstName', displayName: 'First Name' },
                    { field: 'lastName', displayName: 'Last Name' },
                    { field: 'middleName', displayName: 'Middle Name' },
                    { field: 'company', displayName: 'Company' },
                    { field: 'contactNumber', displayName: 'Contact Number' },
                    { field: 'address', displayName: 'Address' },
                    {
                        field: 'action',
                        displayName: 'Action',
                        cellTemplate: "<div class='ui-grid-cell-contents'>" +
                                        "<button class='btn btn-sm btn-success small-btn' ng-click=\"grid.appScope.editCustomer(row.entity)\"><i class='fa fa-pencil'></i> Edit</button>" +
                                        " <button class='btn btn-sm btn-default small-btn' ng-click=\"grid.appScope.deleteCustomer(row.entity)\"><i class='fa fa-trash'></i> Delete</button>" +
                                        "</div>"
                    }
                ]
            };

            $scope.refreshGrid = function () {
                blockUI.start();
                formsCustomerService.getCustomers().then(function (response) {
                    blockUI.stop();
                    $log.log('formsCustomerListController - Customers Loaded');
                    $scope.gridOptions.data = response;
                }, function (err) {
                    $log.warn('formsCustomerListController - Failed to load customers');
                });

            }();

            $scope.addCustomer = function () {
                formsModalService.addCustomer({
                    onSuccess: function (response) {
                        $log.log('success');
                        $scope.gridOptions.data.push(response.customer);
                    },
                    onCancel: function (response) {
                        $log.log('cancelled');
                    }
                });
            };

            $scope.editCustomer = function (customer) {
                formsModalService.editCustomer({
                    customerId: customer.id,
                    onSuccess: function (response) {
                        $log.log('success');
                        angular.forEach(response.customer, function (value, key) {
                            customer[key] = value;
                        });
                    },
                    onCancel: function (response) {
                        $log.log('cancelled');
                    }
                });
            };

            $scope.deleteCustomer = function (customer) {
                modalService.delete({
                    onSuccess: function (response) {
                        $log.log('success');
                        formsCustomerService.deleteCustomer(customer.id).then(function (response) {
                            $scope.gridOptions.data.splice($scope.gridOptions.data.indexOf(customer), 1);
                        }, function (err) {
                            $log.log('formsCustomerListController - Failed to delete customer. Please try again.');
                        });
                    },
                    onCancel: function (response) {
                        $log.log('cancelled');
                    }
                });
            };

        }]);
});


