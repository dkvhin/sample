﻿"use strict";

define(['app-root', 'ngload!ngFileUpload', 'formsCustomerService'], function (app) {
    var controller = d7.lower('formsCustomerEditModalController');

    app.register.controller(controller, ['$scope', '$rootScope', '$state', '$log', '$stateParams', 'blockUI', 'alertBox', 'Upload', 'formsCustomerService', '$uibModalInstance', 'params',
        function ($scope, $rootScope, $state, $log, $stateParams, blockUI, alertBox, Upload, formsCustomerService, $uibModalInstance, params) {
            $log.debug('formsCustomerEditModalController - Loaded');

            $scope.form = {};

            $scope.getCustomer = function () {
                formsCustomerService.getCustomerById(params.customerId).then(function (response) {
                    $scope.form = response;
                    $log.log('formsCustomerEditModalController - Customer loaded successfully.');
                }, function (err) {
                    $log.log('formsCustomerEditModalController - Unable to load customer.');
                });
            }();

            $scope.onSubmit = function () {
                alertBox.close();

                if (!$scope.formCustomerEdit.$valid) {
                    alertBox.show({ 'msesage': 'Kindly check the fields for errors or missing info.', 'type': 'warning' });
                    return;
                }

                blockUI.start();
                formsCustomerService.editCustomer($scope.form).then(function (response) {
                    blockUI.stop();
                    $log.log('formsCustomerEditModalController - Customer updated');
                    alertBox.show({ 'message': 'Customer succesfully updated.', 'type': 'success' });
                    $uibModalInstance.close({
                        customer: response
                    });
                }, function (err) {
                    blockUI.stop();
                    $log.warn('formsCustomerEditModalController - Failed to update customer');
                    alertBox.show({ 'message': 'Failed to update customer. Please try again.', 'type': 'warning' });
                });
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss();
            };

        }]);
});


