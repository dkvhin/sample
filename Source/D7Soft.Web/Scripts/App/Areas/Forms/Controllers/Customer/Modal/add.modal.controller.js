﻿"use strict";

define(['app-root', 'ngload!ngFileUpload', 'formsCustomerService'], function (app) {
    var controller = d7.lower('formsCustomerAddModalController');

    app.register.controller(controller, ['$scope', '$rootScope', '$state', '$log', '$stateParams', 'blockUI', 'alertBox', 'Upload', 'formsCustomerService', '$uibModalInstance', 'params',
        function ($scope, $rootScope, $state, $log, $stateParams, blockUI, alertBox, Upload, formsCustomerService, $uibModalInstance, params) {
            $log.debug('formsCustomerAddModalController - Loaded');

            $scope.form = {
                firstName: '',
                lastName: ''
            };

            (function () {

                if (!params.name) {
                    return;
                }

                var names = params.name.split(' ');

                if (names.length > 1) {
                    $scope.form.lastName = names[names.length - 1];
                    names = names.splice(0, names.length - 1);

                    $scope.form.firstName = names.join(' ');
                } else {
                    $scope.form.firstName = params.name;
                }


            })();

            $scope.onSubmit = function () {

                alertBox.close();

                if (!$scope.formCustomerAdd.$valid) {
                    alertBox.show({ 'message': 'Kindly check the fields for errors or missing info.', 'type': 'warning' });
                    return;
                }

                blockUI.start();
                formsCustomerService.addCustomer($scope.form).then(function (response) {
                    blockUI.stop();
                    $log.log('formsCustomerAddModalController - Customer added');
                    alertBox.show({ 'message': 'Customer succesfully added.', 'type': 'success' });
                    $uibModalInstance.close({
                        customer: response
                    });
                }, function (err) {
                    blockUI.stop();
                    $log.warn('formsCustomerAddModalController - Failed to add customer');
                    alertBox.show({ 'message': 'Failed to add customer. Please try again.', 'type': 'warning' });
                });
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss();
            };

        }]);
});


