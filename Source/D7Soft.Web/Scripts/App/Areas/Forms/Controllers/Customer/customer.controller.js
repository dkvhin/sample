﻿"use strict";

define(['app-root', 'ngload!ngFileUpload', 'formsCustomerService'], function (app) {
    var controller = d7.lower('formsCustomerController');

    app.register.controller(controller, ['$scope', '$rootScope', '$state', '$log', '$stateParams', 'blockUI', 'alertBox', 'Upload', 'formsCustomerService',
        function ($scope, $rootScope, $state, $log, $stateParams, blockUI, alertBox, Upload, formsCustomerService) {
            $log.debug('formsCustomerController - Loaded');

        }]);
});