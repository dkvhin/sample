﻿"use strict";

define(['app-root', 'ngload!ngFileUpload', 'formsTemplateService'], function (app) {
    var controller = d7.lower('formsTemplateEditController');

    app.register.controller(controller, ['$scope', '$rootScope', '$state', '$log', '$stateParams', 'blockUI', 'alertBox', 'Upload', 'formsTemplateService',
        function ($scope, $rootScope, $state, $log, $stateParams, blockUI, alertBox, Upload, formsTemplateService) {
            $log.debug('formsTemplateEditController - Loaded');

            $scope.template = {};

            $scope.form = {
                inputs : []
            };

            $scope.addText = function () {
                $scope.form.inputs.push({
                    width: 200
                });
            };

            $scope.addDate = function () {
                $scope.form.inputs.push({
                    width: 200,
                    isDate: true
                });
            };

            $scope.addNumeric = function () {
                $scope.form.inputs.push({
                    isNumeric: true,
                    width: 200
                });
            };

            $scope.cancel = function() {
                $state.go('site.forms.templates.list');
            };

            (function () {
                blockUI.start();
                formsTemplateService.getById($stateParams.id).then(function (response) {
                    blockUI.stop();
                    $log.log('formsTemplateEditController - loaded template');
                    $scope.template = response;
                    $scope.form = angular.copy(response);
                }, function (err) {
                    blockUI.stop();
                    $log.warn('formsTemplateEditController - failed to load template');
                    alertBox.show({ message: 'Failed to load template.', 'type': 'error' });
                });
            })();

            $scope.onRemove = function (idx) {
                $log.log('removed');

                var input = $scope.form.inputs[idx];

                if (input) {
                    angular.forEach($scope.form.inputs, function (inpt) {
                        if (inpt.hasConvertInput) {
                            inpt.hasConvertInput = false;
                        }
                    });

                    $scope.form.inputs.splice(idx, 1);
                }
            };

            $scope.onConvert = function (input) {
                $log.log('convert');
                if (input.isNumeric && !input.hasConvertInput) {
                    input.hasConvertInput = true;
                    $scope.form.inputs.push({
                        isConvert: true,
                        width: 200
                    });
                }
            };

            $scope.onSubmit = function () {

                if (!$scope.formTemplate.$valid) {
                    alertBox.show({ message: 'Validation Error. Please make sure you supplied the correct info.', 'type': 'warning' });
                    return;
                }

                Upload.base64DataUrl($scope.file).then(function (url) {
                    var data = {
                        id : $scope.template.id,
                        name: $scope.form.name,
                        width: $scope.form.width,
                        height: $scope.form.height,
                        image: (!url) ? $scope.template.image : url,
                        inputs: $scope.form.inputs
                    };

                    blockUI.start();
                    formsTemplateService.updateTemplate(data).then(function (response) {
                        blockUI.stop();
                        
                        $state.go('site.forms.templates.list');
                        alertBox.show({ message: 'Template updated.', 'type': 'success' });
                    }, function (err) {
                        blockUI.stop();
                        alertBox.show({ message: 'Failed to Update template.', 'type': 'error' });
                    });
                });
            };

        }]);
});


