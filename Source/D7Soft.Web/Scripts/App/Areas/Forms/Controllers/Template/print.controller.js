﻿"use strict";

define(['app-root', 'ngload!ngFileUpload', 'formsTemplateService', 'formsCustomerService', 'formsModalService'], function (app) {
    var controller = d7.lower('formsTemplatePrintController');

    app.register.controller(controller, ['$scope', '$rootScope', '$state', '$log', '$stateParams', 'blockUI', 'alertBox', 'formsTemplateService','$timeout','$window','$filter', 'formsCustomerService', 'formsModalService',
        function ($scope, $rootScope, $state, $log, $stateParams, blockUI, alertBox, formsTemplateService, $timeout, $window, $filter, formsCustomerService, formsModalService) {
            $log.debug('formsTemplatePrintController - Loaded');

            (function() {
                blockUI.start();
                formsTemplateService.getById($stateParams.id).then(function (response) {
                    blockUI.stop();
                    $log.log('formsTemplateEditController - loaded template');
                    $scope.template = response;
                }, function (err) {
                    blockUI.stop();
                    $log.warn('formsTemplateEditController - failed to load template');
                    alertBox.show({ message: 'Failed to load template.', 'type': 'error' });
                });
            })();

            var tempFilterText = '',filterTextTimeout;
            $scope.numberToText = function (input) {
                if (filterTextTimeout) $timeout.cancel(filterTextTimeout);
               
                tempFilterText = input.text;
                filterTextTimeout = $timeout(function () {

                    if ($scope.isForced) {
                        $scope.isForced = false;
                        return;
                    }

                    if (input.text && input.text.length > 0) {
                        tempFilterText = input.text.replace(',', '');
                        input.text = $filter('number')(tempFilterText, 2);
                    }
                   
                    angular.forEach($scope.template.inputs, function(val) {
                        if (val.isConvert) {
                            var result = formsTemplateService.toWords(tempFilterText, '','');

                            if (result) {
                                val.text = result + ' ONLY';
                            }
                            
                        }
                    });
                }, 2000);
            };

            $scope.isForced = false;
            $scope.forceUpdate = function () {
                $scope.isForced = true;
                angular.forEach($scope.template.inputs, function (val) {
                    if (val.isConvert) {
                        var result = formsTemplateService.toWords(tempFilterText, '', '');

                        if (result) {
                            val.text = result + ' ONLY';
                        }

                    }

                    if (val.isNumeric) {
                        if (val.text && val.text.length > 0) {
                            tempFilterText = val.text.replace(',', '');
                            val.text = $filter('number')(tempFilterText, 2);
                        }
                    }
                });
            };

            $scope.print = function () {
                $scope.forceUpdate();
                $timeout(function() {
                    for (var i = 0; i < $scope.template.inputs.length; i++) {
                        if ($scope.template.inputs[i].isAutoComplete && $scope.template.inputs[i].text && !$scope.template.inputs[i].text.id && !$scope.template.inputs[i].isManuallyAdded) {
                            $scope.addCustomerModal($scope.template.inputs[i], function() {
                                $timeout(function() {
                                    $window.print();
                                }, 200);

                            });
                            return;
                        }
                    }
                    $window.print();
                },100);
             
            };

            $scope.getNames = function (val) {
                return formsCustomerService.filter(val).then(function (response) {
                    $log.log(response);
                    return response.map(function(item) {
                        return {
                            name: item.firstName + " " + item.lastName,
                            id: item.id
                        }
                    });
                }, function (err) {
                    $log.log('formsTemplateEditController - Unable to load names.');
                });
            };

            $scope.addCustomerModal = function (input, callBack) {
                formsModalService.addCustomer({
                    name: input.text,
                    onSuccess: function (response) {
                        $log.log('success');

                        input.isManuallyAdded = true;

                        if (callBack) {
                            callBack();
                        }
                    },
                    onCancel: function (response) {
                        $log.log('cancelled');
                    }
                });
            };

            $scope.clearInput = function(input) {
                input.isManuallyAdded = false;
            };
        }]);
});


