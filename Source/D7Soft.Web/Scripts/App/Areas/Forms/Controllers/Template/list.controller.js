﻿"use strict";

define(['app-root', 'ngload!ngGrid', 'formsTemplateService', 'modalService'], function (app) {
    var controller = d7.lower('formsTemplateListController');

    app.register.controller(controller, ['$scope', '$rootScope', '$state', '$log', '$stateParams', 'blockUI', 'authenticationService', 'alertBox', 'formsTemplateService', 'modalService',
        function ($scope, $rootScope, $state, $log, $stateParams, blockUI, authenticationService, alertBox, formsTemplateService, modalService) {
            $log.debug('formsTemplateListController - Loaded');

            $scope.gridOptions = {
                data: [],
                rowSelection : true,
                columnDefs: [
                    { field: 'name', displayName: 'Name' },
                    { field: 'isActive', displayName: 'Active' },
                    {
                        field: 'action',
                        displayName: 'Action',
                        cellTemplate: "<div class='ui-grid-cell-contents'>" +
                                        "<a href='#' class='btn btn-sm btn-success small-btn' ui-sref=\"site.forms.templates.print({id:row.entity.id})\"><i class='fa fa-print'></i> Print</a>" +
                                        " <a href='#' class='btn btn-sm btn-default small-btn' ui-sref=\"site.forms.templates.edit({id:row.entity.id})\"><i class='fa fa-pencil'></i> Edit</a>" +
                                        " <button class='btn btn-sm btn-danger small-btn' ng-click=\"grid.appScope.delete(row.entity)\"><i class='fa fa-trash'></i> Del</button>" +
                                        "</div>"
                    }
                ]
            };

            $scope.refreshGrid = function() {
                blockUI.start();
                formsTemplateService.getAll().then(function (response) {
                    blockUI.stop();
                    $log.log('formsTemplateListController - Templates Loaded');
                    $scope.gridOptions.data = response;
                }, function(err) {
                    $log.warn('formsTemplateListController - Failed to load templates');
                });

            }();

            $scope.delete = function (template) {
                modalService.delete({
                    onSuccess: function (response) {
                        $log.log('success');
                        formsTemplateService.deleteTemplate(template.id).then(function (response) {
                            $scope.gridOptions.data.splice($scope.gridOptions.data.indexOf(template), 1);
                        }, function (err) {
                            $log.log('formsCustomerListController - Failed to delete customer. Please try again.');
                        });
                    },
                    onCancel: function (response) {
                        $log.log('canclled');
                    }
                });
            };

        }]);
});


