﻿"use strict";

define(['app-root', 'ngload!ngFileUpload', 'formsTemplateService'], function (app) {
    var controller = d7.lower('formsTemplateAddController');

    app.register.controller(controller, ['$scope', '$rootScope', '$state', '$log', '$stateParams', 'blockUI', 'alertBox', 'Upload', 'formsTemplateService',
        function ($scope, $rootScope, $state, $log, $stateParams, blockUI, alertBox, Upload, formsTemplateService) {
            $log.debug('formsTemplateAddController - Loaded');

            $scope.form = {
                inputs: [],
                width: 768,
                height: 384
            };

            $scope.addText = function () {
                $scope.form.inputs.push({
                    width:200
                });
            };

            $scope.addDate = function() {
                $scope.form.inputs.push({
                    width: 200,
                    isDate: true
                });
            };

            $scope.addNumeric = function () {
                $scope.form.inputs.push({
                    isNumeric: true,
                    width: 200
                });
            };

            $scope.onRemove = function (idx) {
                var input = $scope.form.inputs[idx];

                if (input) {
                    angular.forEach($scope.form.inputs, function (inpt) {
                        if (inpt.hasConvertInput) {
                            inpt.hasConvertInput = false;
                        }
                    });

                    $scope.form.inputs.splice(idx, 1);
                }
            };

            $scope.onConvert = function (input) {
                if (input.isNumeric && !input.hasConvertInput) {
                    input.hasConvertInput = true;
                    $scope.form.inputs.push({
                        isConvert: true,
                        width:200
                    });
                }
            };

            $scope.cancel = function() {
                $state.go('site.forms.templates.list');
            };

            $scope.onSubmit = function () {

                if (!$scope.formTemplate.$valid) {
                    alertBox.show({ message: 'Validation Error. Please make sure you supplied the correct info.', 'type': 'warning' });
                    return;
                }

                Upload.base64DataUrl($scope.file).then(function(url) {
                    var data = {
                        name: $scope.form.name,
                        width: $scope.form.width,
                        height: $scope.form.height,
                        image: url,
                        inputs: $scope.form.inputs
                    };

                    blockUI.start();
                    formsTemplateService.addTemplate(data).then(function (response) {
                        blockUI.stop();
                        alertBox.show({ message: 'Succesfully Added new Template.', 'type': 'success' });
                        $state.go('site.forms.templates.list');
                    }, function (err) {
                        blockUI.stop();
                        alertBox.show({ message: 'Failed to add template.', 'type': 'error' });
                    });
                });
            };
        }]);
});


