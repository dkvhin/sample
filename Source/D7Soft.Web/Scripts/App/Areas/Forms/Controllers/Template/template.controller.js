﻿"use strict";

define(['app-root', 'draggableDirective'], function (app) {
    var controller = d7.lower('formsTemplateController');

    app.register.controller(controller, ['$scope', '$rootScope', '$state', '$log', '$stateParams',
        function ($scope, $rootScope, $state, $log, $stateParams) {
            $log.debug('formsTemplateController - Loaded');

        }]);
});


