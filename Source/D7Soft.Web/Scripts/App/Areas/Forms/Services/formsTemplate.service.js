﻿"use strict";
define(['app-root'], function (app) {

    app.register.service('formsTemplateService', ['$http', '$q', '$log', function ($http, $q, $log) {

        this.addTemplate = function (data) {
            var deferred = $q.defer();
            $http.post('/forms/api/templates/add', data).success(function (response) {
                deferred.resolve(response);
            }).error(function (err, status) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.updateTemplate = function (data) {
            var deferred = $q.defer();
            $http.put('/forms/api/templates/update', data).success(function (response) {
                deferred.resolve(response);
            }).error(function (err, status) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.getAll = function () {
            var deferred = $q.defer();
            $http.get('/forms/api/templates/getall').success(function (response) {
                deferred.resolve(response);
            }).error(function (err, status) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.getById = function (id) {
            var deferred = $q.defer();
            $http.get('/forms/api/templates/get', { params: { id: id } }).success(function (response) {
                deferred.resolve(response);
            }).error(function (err, status) {
                deferred.reject(err);
            });
            return deferred.promise;
        };


        // Convert numbers to words
        // copyright 25th July 2006, by Stephen Chapman http://javascript.about.com
        // permission to use this Javascript on your web page is granted
        // provided that all of the code (including this copyright notice) is
        // used exactly as shown (you can change the numbering system if you wish)

        // American Numbering System
        var th = ['', 'thousand', 'million', 'billion', 'trillion'];
        // uncomment this line for English Number System
        // var th = ['','thousand','million', 'milliard','billion'];

        var dg = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];
        var tn = ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'];
        var tw = ['twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];

        this.toWords = function (number, singular, plural) {
            if (!number) {
                return '';
            }

            var splitNumber = number.split('.');

            var mainNumber = this.toWordsMethod(splitNumber[0]);

            var cents = splitNumber[1];

            if (cents) {

                if (cents.length < 2) {
                    cents = cents + '0';
                }

                if (parseInt(cents) > 0) {
                    mainNumber += 'and ' + cents + '/100';
                }                
            }

            return mainNumber;
        };

        this.toWordsMethod = function (number) {
            if (!number) return '';

            number = number.toString();
            number = number.replace(/[\, ]/g, '');
            if (number != parseFloat(number)) return 'not a number';
            var x = number.indexOf('.');
            if (x == -1) x = number.length;
            if (x > 15) return 'too big';
            var n = number.split('');
            var str = '';
            var sk = 0;
            for (var i = 0; i < x; i++) {
                if ((x - i) % 3 == 2) {
                    if (n[i] == '1') {
                        str += tn[Number(n[i + 1])] + ' ';
                        i++;
                        sk = 1;
                    } else if (n[i] != 0) {
                        str += tw[n[i] - 2] + ' ';
                        sk = 1;
                    }
                } else if (n[i] != 0) {
                    str += dg[n[i]] + ' ';
                    if ((x - i) % 3 == 0) str += 'hundred ';
                    sk = 1;
                }
                if ((x - i) % 3 == 1) {
                    if (sk) str += th[(x - i - 1) / 3] + ' ';
                    sk = 0;
                }
            }
            return str.replace(/\s+/g, ' ');
        };

        this.deleteTemplate = function (id) {
            var deferred = $q.defer();
            $http.delete('/forms/api/templates/delete', { params: { id: id } }).success(function (response) {
                deferred.resolve(response);
            }).error(function (err, status) {
                deferred.reject(err);
            });
            return deferred.promise;
        };
    }]);
});


