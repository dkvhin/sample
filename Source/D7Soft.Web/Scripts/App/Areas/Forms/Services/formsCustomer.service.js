﻿"use strict";
define(['app-root'], function (app) {

    app.register.service('formsCustomerService', ['$http', '$q', '$log', function ($http, $q, $log) {

        this.getCustomers = function () {
            var deferred = $q.defer();
            $http.get('/forms/api/customer/getall').success(function (response) {
                deferred.resolve(response);
            }).error(function (err, status) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.getCustomerById = function (id) {
            var deferred = $q.defer();
            $http.get('/forms/api/customer/get', { params: { id: id } }).success(function (response) {
                deferred.resolve(response);
            }).error(function (err, status) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.addCustomer = function (data) {
            var deferred = $q.defer();
            $http.post('/forms/api/customer/add', data).success(function (response) {
                deferred.resolve(response);
            }).error(function (err, status) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.editCustomer = function (data) {
            var deferred = $q.defer();
            $http.put('/forms/api/customer/update', data).success(function (response) {
                deferred.resolve(response);
            }).error(function (err, status) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.deleteCustomer = function (id) {
            var deferred = $q.defer();
            $http.delete('/forms/api/customer/delete', { params: { id: id } }).success(function (response) {
                deferred.resolve(response);
            }).error(function (err, status) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.filter = function (keyword) {
            var deferred = $q.defer();
            $http.get('/forms/api/customer/filter', { params: { keyword: keyword } }).success(function (response) {
                deferred.resolve(response);
            }).error(function (err, status) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

    }]);
});


