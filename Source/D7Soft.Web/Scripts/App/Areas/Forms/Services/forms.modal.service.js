﻿"use strict";
define(['angularAMD'], function (angularAMD) {

    angularAMD.service('formsModalService', ['$log', '$uibModal', 'blockUI', function ($log, $uibModal, blockUI) {
        var self = this;

        self.addCustomer = function (params) {
            blockUI.start();
            var modalInstance = $uibModal.open(angularAMD.route({
                animation: true,
                backdrop: 'static',
                templateUrl: d7.path.view('add.modal', 'customer/modal', 'forms'),
                controllerUrl: d7.path.controller('add.modal', 'customer/modal', 'forms'),
                controller: d7.lower('formsCustomerAddModalController'),
                size: 'md',
                resolve: {
                    params: function () {
                        return {
                            name: params.name
                        };
                    }
                }
            }));

            modalInstance.opened.then(function () {
                blockUI.stop();
            });

            modalInstance.result.then(params.onSuccess, params.onCancel);
        };

        self.editCustomer = function (params) {
            blockUI.start();
            var modalInstance = $uibModal.open(angularAMD.route({
                animation: true,
                backdrop: 'static',
                templateUrl: d7.path.view('edit.modal', 'customer/modal', 'forms'),
                controllerUrl: d7.path.controller('edit.modal', 'customer/modal', 'forms'),
                controller: d7.lower('formsCustomerEditModalController'),
                size: 'md',
                resolve: {
                    params: function () {
                        return {
                            customerId: params.customerId
                        };
                    }
                }
            }));

            modalInstance.opened.then(function () {
                blockUI.stop();
            });

            modalInstance.result.then(params.onSuccess, params.onCancel);
        };

        self.deleteCustomer = function (params) {
            blockUI.start();
            var modalInstance = $uibModal.open(angularAMD.route({
                animation: true,
                backdrop: 'static',
                templateUrl: d7.path.view('delete.modal', 'customer/modal', 'forms'),
                controllerUrl: d7.path.controller('delete.modal', 'customer/modal', 'forms'),
                controller: d7.lower('formsCustomerDeleteModalController'),
                size: 'md',
                resolve: {
                    params: function () {
                        return {
                            customerId: params.customerId
                        };
                    }
                }
            }));

            modalInstance.opened.then(function () {
                blockUI.stop();
            });

            modalInstance.result.then(params.onSuccess, params.onCancel);
        };

    }]);
});