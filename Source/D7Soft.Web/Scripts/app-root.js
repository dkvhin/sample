﻿"use strict";

define(['angularAMD', 'authInterceptorService', 'routes',
    'ui-route', 'ui-bootstrap', 'angular-sanitize', 'blockUI', 'ngMessages', 'http-auth-interceptor', 'localStorageService', 'alertBox', 'authorization', 'authenticationService', 'modalService'],
    function (angularAMD, authInterceptorService, routes) {
        var app = angular.module("mainModule", ['ngMessages', 'ui.router', 'blockUI', 'ngSanitize', 'ui.bootstrap', 'http-auth-interceptor', 'LocalStorageModule', 'alertBox']);




        app.config(['$httpProvider', function ($httpProvider) {
            //$httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
            $httpProvider.defaults.withCredentials = true;
            $httpProvider.interceptors.push('authInterceptorService');
        }]);

        app.config(['localStorageServiceProvider', function (localStorageServiceProvider) {
            var $log = angular.injector(['ng']).get('$log');

            $log.info('app-root : set session storage');
            localStorageServiceProvider
              .setStorageType('sessionStorage');
        }]);

        app.config(['blockUIConfig', function (blockUIConfig) {

            // Change the default overlay message
            blockUIConfig.message = "loading...";
            // Change the default delay to 100ms before the blocking is visible
            blockUIConfig.delay = 1;
            // Disable automatically blocking of the user interface
            blockUIConfig.autoBlock = false;
            //test build
        }]);

        app.config(['$logProvider', function ($logProvider) {
            $logProvider.debugEnabled(true);
        }]);


        /*
         * ROUTES SETTINGS
         */

        app.config([
            '$stateProvider', '$urlRouterProvider', '$urlMatcherFactoryProvider',
            function ($stateProvider, $urlRouterProvider, $urlMatcherFactory) {

                $urlMatcherFactory.caseInsensitive(true);

                angular.forEach(routes, function (route) {
                    route.configure($stateProvider);
                });

                $urlRouterProvider.otherwise('/');
            }
        ]);

        app.run(['$rootScope', 'blockUI', '$timeout', 'alertBox', '$state', '$stateParams', 'localStorageService', '$injector', '$log', 'authorization', 'authenticationService',
            function ($rootScope, blockUI, $timeout, alertBox, $state, $stateParams, localStorageService, $injector, $log, authorization, authenticationService) {
                $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
                    blockUI.stop();
                });



                $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
                    $log.info('$stateChangeStart - ' + toState.name);
                    $rootScope.toState = toState;
                    $rootScope.toStateParams = toParams;
                    blockUI.start();
                    alertBox.close();
                    $rootScope.productnav = false;
                    if (toState.title) {
                        $rootScope.pageTitle = toState.title;
                    }


                    var isAuthenticated = authenticationService.isAuthenticated();
                    if (toState.data && toState.data.roles && toState.data.roles.length > 0 && !authenticationService.isInAnyRole(toState.data.roles)) {
                        $log.log('Identity Check');

                        if (isAuthenticated) {
                            blockUI.stop();
                            $state.go('site.accessdenied', {}, { location: false });
                            event.preventDefault();
                        }
                        else {
                            event.preventDefault();
                            $log.log('Identity Checked');
                            $rootScope.returnToState = toState;
                            $rootScope.returnToStateParams = toParams;
                            $log.info('Redirecting to Login');
                            blockUI.stop();
                            $state.go('login');
                        }
                    }
                });

                $rootScope.failedRefresh = 0;
                $rootScope.inFlightAuthRequest = false;

                $rootScope.$on('event:auth-loginRequired', function () {
                    $log.info('event:auth-loginRequired');
                    $rootScope.failedRefresh++;
                    var authInterceptorService = $injector.get('authInterceptorServiceB');
                    var authData = localStorageService.cookie.get(d7.cookieName);

                    if (!authData || $rootScope.failedRefresh > 2) {
                        authInterceptorService.loginCancelled(null, { config: {}, reponse: 'not authenticated' });
                        authenticationService.logout();

                        $rootScope.toState = $state.current;
                        $rootScope.toStateParams = $stateParams;

                        blockUI.stop();
                        $state.go('login');
                        $rootScope.failedRefresh = 0;
                        return;
                    }

                    if (!$rootScope.inFlightAuthRequest) {
                        $rootScope.inFlightAuthRequest =
                            authenticationService.refreshAccessToken(authData.refreshToken, authData.username).then(function (response) {
                                $rootScope.inFlightAuthRequest = null;
                                if (!response.error) {
                                    $log.info('event:Authenticated');
                                    authInterceptorService.loginConfirmed();
                                    $rootScope.failedRefresh = 0;
                                } else {
                                    authInterceptorService.loginCancelled(null, { config: {}, reponse: 'not authenticated' });
                                    authenticationService.logout();
                                    $log.log(response);
                                    blockUI.stop();
                                    $state.go('login');
                                }
                            }, function (err) {
                                authInterceptorService.loginCancelled(null, { config: {}, reponse: 'not authenticated' });
                                authenticationService.logout();
                                $log.log(err);
                                blockUI.stop();
                                $state.go('login');
                            });
                    }
                });
            }]);

        var indexController = function ($scope, $rootScope, $http, $location, blockUI) {
            $scope.initializeController = function () {
            }
        };

        indexController.$inject = ['$scope', '$rootScope', '$http', '$location', 'blockUI'];
        app.controller("indexController", indexController);

        // Bootstrap Angular when DOM is ready
        angularAMD.bootstrap(app);

        return app;
    });