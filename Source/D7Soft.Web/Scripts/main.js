﻿/// <reference path="Scripts/ui-bootstrap-tpls-0.11.0.js" />
/// <reference path="Scripts/ui-bootstrap-tpls-0.11.0.js" />
/// <reference path="Scripts/ui-bootstrap-tpls-0.11.0.js" />
require.config({
    urlArgs: 'v=' + (d7.isLive() ? d7.version : new Date().getTime()),
    waitSeconds: 15,
    baseUrl: "/scripts/",
    optimizeAllPluginResources: true,

    // alias libraries paths
    paths: {
        'app-root': 'app-root',
        'routes' : 'routes',
        'angular': d7.path.vendor('angular.min'),
        'ui-route': d7.path.plugin('angular-ui-router'),
        'angularAMD': d7.path.plugin('angularAMD'),
        'ui-bootstrap': d7.path.plugin('ui-bootstrap-tpls-0.14.3'),
        'ngMessages': d7.path.plugin('angular-messages'),
        'blockUI': d7.path.plugin('angular-block-ui'),
        'alertBox': d7.path.modules('alertbox'),
        'http-auth-interceptor': d7.path.plugin('angular-http-auth-interceptor'),
        'ngload': d7.path.plugin('ngload'),
        'angular-sanitize': d7.path.plugin('angular-sanitize'),
        'authInterceptorService': d7.path.service('authInterceptor'),
        'localStorageService': d7.path.plugin('angular-local-storage'),
        'authenticationService': d7.path.service('authentication'),
        'ngFileUpload': d7.path.plugin('ng-file-upload'),
        'ngFileUploadShim': d7.path.plugin('ng-file-upload-shim'),
        'ngGrid': d7.path.plugin('ui-grid.min'),

       
        // Service Helpers
        'modalService' : d7.path.service('modal'),

        //Authorization
        'authorization': d7.path.service('authorization'),

        // routes
        'defaultRoutes': d7.path.routes('default'),
        'formsRoutes': d7.path.routes('forms', 'forms'),
        'accountsRoutes': d7.path.routes('accounts', 'accounts'),


        //Area Forms
        'formsTemplateService': d7.path.service('formsTemplate', 'forms'),
        'formsCustomerService': d7.path.service('formsCustomer', 'forms'),
        'formsModalService': d7.path.service('forms.modal', 'forms'),
        'draggableDirective': d7.path.directive('draggable', 'forms'),

        //Area Accounts
        'accountRoleService': d7.path.service('accountRole', 'accounts'),
        'accountUserService': d7.path.service('accountUser', 'accounts')
    },

    // Add angular modules that does not support AMD out of the box, put it in a shim
    shim: {
        'angularAMD': ['angular'],
        'ngload': ['angularAMD'],
        'ui-route': ['angular'],
        'blockUI': ['angular'],
        'ngMessages' : ['angular'],
        'angular-sanitize': ['angular'],
        'ui-bootstrap': ['angular'],
        'http-auth-interceptor': ['angular'],
        'localStorageService': ['angular'],
        'alertbox': ['angular'],

        'ngFileUpload': ['angular', 'ngFileUploadShim'],
        'ngFileUploadShim': ['angular'],
        'ngGrid': ['angular'],
        'authorization': ['authenticationService']
    },

    // kick start application
    deps: ['app-root']
});
