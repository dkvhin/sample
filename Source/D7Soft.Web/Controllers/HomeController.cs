﻿using System.Configuration;
using System.Diagnostics;
using System.Reflection;
using System.Web.Mvc;
using D7Soft.Web.Models;

namespace D7Soft.Web.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            string version = fvi.FileVersion;

            var model = new BaseModel
            {
                Environment = ConfigurationManager.AppSettings["Environment"],
                Version = version
            };
            return View(model);
        }
    }
}