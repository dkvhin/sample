﻿namespace D7Soft.Authentication
{
    public static class ClaimTypes
    {
        public const string Role = "Role";
        public const string Id = "Id";
        public const string Username = "Username";
    }
}
