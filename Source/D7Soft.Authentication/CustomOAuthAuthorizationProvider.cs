﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using D7Soft.Data.Repository.Interfaces;
using Microsoft.Owin.Security.OAuth;
using D7Soft.Data.Poco.Authentication;
using Microsoft.Owin.Security;

namespace D7Soft.Authentication
{
    public class CustomOAuthAuthorizationProvider : OAuthAuthorizationServerProvider
    {
        private readonly IUserAuthenticationRepository _userAuthenticationRepository;
        private readonly IUserRepository _userRepository;
        private readonly IUserRoleRepository _userRoleRepository;
        private readonly IPasswordHasher _passwordHasher;

        public CustomOAuthAuthorizationProvider(IUserAuthenticationRepository userAuthenticationRepository,
            IUserRepository userRepository, IUserRoleRepository userRoleRepository, IPasswordHasher passwordHasher)
        {
            _userAuthenticationRepository = userAuthenticationRepository;
            _userRepository = userRepository;
            _userRoleRepository = userRoleRepository;
            _passwordHasher = passwordHasher;
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            string clientId;
            string clientSecret;

            if (!context.TryGetBasicCredentials(out clientId, out clientSecret))
            {
                if (!context.TryGetFormCredentials(out clientId, out clientSecret))
                {
                    context.SetError("invalid_client", "Client credentials could not be retrieved through the Authorization header.");
                    context.Rejected();
                    return Task.FromResult(0);
                }
            }
            try
            {
                var client = _userAuthenticationRepository.GetByClientId(clientId);

                if (client != null && client.ClientSecret == clientSecret)
                {
                    context.OwinContext.Set("ouath:client", client);
                    context.Validated(clientId);
                }
                else
                {
                    context.SetError("invalid_client", "Client credentials are invalid");
                    context.Rejected();
                }
            }
            catch (Exception)
            {
                context.SetError("server_error");
                context.Rejected();
            }
            return Task.FromResult(0);
        }

        public override Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var client = context.OwinContext.Get<ClientAppModel>("ouath:client");

            var user = _userRepository.GetUserByUsername(context.UserName);

            if (user == null)
            {
                context.SetError("login_error", "Invalid username.");
                return Task.FromResult(0);
            }

            var hashed = _passwordHasher.Sha256Hash(context.Password + user.PasswordSalt);

            if (user.HashedPassword != hashed)
            {
                context.SetError("login_error", "Invalid Password.");
                return Task.FromResult(0);
            }

            var roles = _userRoleRepository.GetRolesByUserId(user.Id);

            var rolesFormatted = roles.Select(c => c.Key).Aggregate((k, v) => k + "," + v);

            var claimIndentity = new ClaimsIdentity("Bearer", "Id", ClaimTypes.Role);
            claimIndentity.AddClaim(new Claim(ClaimTypes.Id, user.Id.ToString()));
            claimIndentity.AddClaim(new Claim(ClaimTypes.Role, rolesFormatted));

            context.OwinContext.Set("as:client_id", client.ClientId.ToString(CultureInfo.InvariantCulture));

            var props = new AuthenticationProperties(new Dictionary<string, string>
            {
                {"roles", rolesFormatted}
            });


            context.Validated(new AuthenticationTicket(claimIndentity, props));
            return Task.FromResult(0);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

        public override Task GrantRefreshToken(OAuthGrantRefreshTokenContext context)
        {
            var originalClient = context.OwinContext.Get<string>("as:client_id");
            var currentClient = context.OwinContext.Get<ClientAppModel>("ouath:client");

            if (originalClient != currentClient.ClientId)
            {
                context.Rejected();
                return Task.FromResult(0);
            }

            var newId = new ClaimsIdentity(context.Ticket.Identity);
            var newTicket = new AuthenticationTicket(newId, context.Ticket.Properties);
            context.Validated(newTicket);

            return Task.FromResult(0);
        }
    }
}
