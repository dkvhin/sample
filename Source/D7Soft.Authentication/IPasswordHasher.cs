﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace D7Soft.Authentication
{
    public sealed class PasswordHasher : IPasswordHasher
    {
        public string Sha256Hash(string password)
        {
            var sb = new StringBuilder();

            using (SHA256 hash = SHA256.Create())
            {
                Encoding enc = Encoding.UTF8;
                Byte[] result = hash.ComputeHash(enc.GetBytes(password));

                foreach (Byte b in result)
                    sb.Append(b.ToString("x2"));
            }
            return sb.ToString();
        }

        public string CreateRandomPassword(int passwordLength)
        {
            const string allowedChars = "abcdefghjkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789";
            var chars = new char[passwordLength];
            var rd = new Random();

            for (var i = 0; i < passwordLength; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }

            return new string(chars);
        }

        public string GenerateRandomSalt(int size = 16)
        {
            var rng = new RNGCryptoServiceProvider();
            var bytes = new Byte[size];
            rng.GetBytes(bytes);
            return Convert.ToBase64String(bytes);
        }
    }

    public interface IPasswordHasher
    {
        string Sha256Hash(string password);
        string CreateRandomPassword(int passwordLength);
        string GenerateRandomSalt(int size = 16);
    }
}
