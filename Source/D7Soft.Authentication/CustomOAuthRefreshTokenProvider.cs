﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Helpers;
using D7Soft.Data.Poco.UserRefreshToken;
using D7Soft.Data.Repository.Interfaces;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Infrastructure;
namespace D7Soft.Authentication
{
    public class CustomOAuthRefreshTokenProvider : IAuthenticationTokenProvider
    {
        private readonly IUserAuthenticationRepository _userAuthenticationRepository;
        private readonly IUserRoleRepository _userRoleRepository;
        public CustomOAuthRefreshTokenProvider(IUserAuthenticationRepository userAuthenticationRepository, IUserRoleRepository userRoleRepository)
        {
            _userAuthenticationRepository = userAuthenticationRepository;
            _userRoleRepository = userRoleRepository;
        }

        public void Create(AuthenticationTokenCreateContext context)
        {
            throw new System.NotImplementedException();
        }

        public Task CreateAsync(AuthenticationTokenCreateContext context)
        {
            var guid = Guid.NewGuid().ToString();
            var refreshToken = Crypto.Hash(guid);
            var userId = context.Ticket.Identity.FindFirst(ClaimTypes.Id).Value;

            _userAuthenticationRepository.AddRefreshToken(new AddUserRefreshTokenPoco
            {
                Token = refreshToken,
                UserId = int.Parse(userId),
                ClientId = context.OwinContext.Get<string>("as:client_id"),
                Expiration = context.Ticket.Properties.ExpiresUtc.GetValueOrDefault().DateTime
            });

            context.SetToken(refreshToken);
            return Task.FromResult(0);
        }

        public void Receive(AuthenticationTokenReceiveContext context)
        {
            throw new System.NotImplementedException();
        }

        public Task ReceiveAsync(AuthenticationTokenReceiveContext context)
        {
            var refresh = _userAuthenticationRepository.GetRefreshTokenByToken(context.Token);

            if (refresh != null)
            {
                var roles = _userRoleRepository.GetRolesByUserId(refresh.UserId);

                var rolesFormatted = roles.Select(c => c.Key).Aggregate((k, v) => k + "," + v);

                var claimIndentity = new ClaimsIdentity("Bearer", "Id", ClaimTypes.Role);
                claimIndentity.AddClaim(new Claim(ClaimTypes.Id, refresh.UserId.ToString()));
                claimIndentity.AddClaim(new Claim(ClaimTypes.Role, rolesFormatted));

                context.OwinContext.Set("as:client_id", refresh.ClientId.ToString(CultureInfo.InvariantCulture));

                var props = new AuthenticationProperties(new Dictionary<string, string>
                {
                    {"roles", rolesFormatted}
                });

                var ticket = new AuthenticationTicket(claimIndentity, props);
                ticket.Properties.IssuedUtc = DateTime.UtcNow;
                ticket.Properties.ExpiresUtc = DateTime.UtcNow.AddMinutes(1); // This needs to be after Issued, not the real expiry time
                context.SetTicket(ticket);

                _userAuthenticationRepository.DeleteByToken(refresh.Token);
            }

            return Task.FromResult(0);
        }
    }
}
