﻿namespace D7Soft.Data.Poco.Authentication
{
    public class ClientAppModel
    {
        public int Id { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
    }
}
