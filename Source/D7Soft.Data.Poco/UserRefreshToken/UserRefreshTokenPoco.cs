﻿using System;

namespace D7Soft.Data.Poco.UserRefreshToken
{
    public class UserRefreshTokenPoco
    {
        public int Id { get; set; }
        public string Token { get; set; }
        public int UserId { get; set; }
        public string ClientId { get; set; }
        public DateTime Expires { get; set; }
    }
}
