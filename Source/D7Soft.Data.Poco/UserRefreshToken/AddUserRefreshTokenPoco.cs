﻿using System;

namespace D7Soft.Data.Poco.UserRefreshToken
{
    public class AddUserRefreshTokenPoco
    {
        public string Token { get; set; }
        public int UserId { get; set; }
        public DateTime Expiration { get; set; }
        public string ClientId { get; set; }
    }
}
