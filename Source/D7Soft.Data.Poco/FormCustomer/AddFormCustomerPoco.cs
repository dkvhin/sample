﻿using System;

namespace D7Soft.Data.Poco.FormCustomer
{
    public class AddFormCustomerPoco
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Company { get; set; }
        public string ContactNumber { get; set; }
        public string Address { get; set; }

    }
}
