﻿namespace D7Soft.Data.Poco.FormTemplate
{
    public class FormTemplateSummaryPoco
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string Image { get; set; }
        public bool IsActive { get; set; }
    }
}
