﻿namespace D7Soft.Data.Poco.Role
{
    public class RoleSummaryPoco
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Key { get; set; }
        public string Description { get; set; }
    }
}
