﻿using System.Collections.Generic;
using D7Soft.Data.Poco.Role;

namespace D7Soft.Data.Poco.RoleGroup
{
    public class RoleGroupSummaryPoco
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public IEnumerable<RoleInfoSummaryPoco> Roles { get; set; }
    }
}
