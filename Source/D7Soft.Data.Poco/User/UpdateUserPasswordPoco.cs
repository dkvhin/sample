﻿namespace D7Soft.Data.Poco.User
{
    public class UpdateUserPasswordPoco
    {
        public int Id { get; set; }
        public string PasswordSalt { get; set; }
        public string HashedPassword { get; set; }
    }
}
