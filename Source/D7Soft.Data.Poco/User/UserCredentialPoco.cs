﻿namespace D7Soft.Data.Poco.User
{
    public class UserCredentialPoco : UserDetailsPoco
    {
        public string PasswordSalt { get; set; }
        public string HashedPassword { get; set; }
    }
}
