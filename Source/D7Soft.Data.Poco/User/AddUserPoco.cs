﻿using System.ComponentModel.DataAnnotations;

namespace D7Soft.Data.Poco.User
{
    public class AddUserPoco
    {
        [StringLength(150)]
        public string Email { get; set; }

        [StringLength(50)]
        public string PasswordSalt { get; set; }

        [StringLength(250)]
        public string HashedPassword { get; set; }

        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string LastName { get; set; }

        [StringLength(150)]
        public string Username { get; set; }
    }
}
