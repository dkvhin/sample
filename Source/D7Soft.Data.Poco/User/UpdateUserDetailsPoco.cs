﻿namespace D7Soft.Data.Poco.User
{
    public class UpdateUserDetailsPoco
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
    }
}
