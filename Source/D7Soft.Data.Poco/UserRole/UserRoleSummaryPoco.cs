﻿using System.Collections.Generic;
using D7Soft.Data.Poco.Role;

namespace D7Soft.Data.Poco.UserRole
{
    public class UserRoleSummaryPoco
    {
        public int UserId { get; set; }
        public List<RoleSummaryPoco> Roles { get; set; }
    }
}
