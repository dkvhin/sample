﻿using System.Collections.Generic;

namespace D7Soft.Data.Poco.UserRole
{
    public class AddUserRolePoco
    {
        public AddUserRolePoco()
        {
            RoleIds = new List<int>();
        }

        public int UserId { get; set; }
        public List<int> RoleIds { get; set; }
    }
}
