﻿using AutoMapper;
using D7Soft.Services.Accounts.Infrastructure;
using NUnit.Framework;

namespace D7Soft.Services.Accounts.Tests
{
    [TestFixture]
    class MapperTests
    {
        [Test]
        public void All_Mappers_Are_Mapping_Correctly()
        {
            var config = new MapperConfiguration(c => c.CreateProfile("Test"));
            new AccountModelMapper().Configure(config);
            config.AssertConfigurationIsValid();
        }
    }
}
