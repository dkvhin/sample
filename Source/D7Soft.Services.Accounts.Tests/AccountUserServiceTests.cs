﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using D7Soft.Authentication;
using D7Soft.Core.Validation;
using D7Soft.Data.Poco.RoleGroup;
using D7Soft.Data.Poco.User;
using D7Soft.Data.Poco.UserRole;
using D7Soft.Data.Repository.Interfaces;
using D7Soft.Services.Accounts.Infrastructure;
using D7Soft.Services.Accounts.Interfaces;
using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;
using D7Soft.Services.Accounts.ParamModels;
using D7Soft.Services.Accounts.Validators;
using Assert = NUnit.Framework.Assert;

namespace D7Soft.Services.Accounts.Tests
{
    [TestFixture]
    public class AccountUserServiceTests
    {
        private Mock<IUserRoleRepository> _userRoleRepoMock;
        private IAccountUserService _accountUserService;
        private Mock<IUserRepository> _userRepositoryMock;
        private ValidationProvider _validationProvider;

        [SetUp]
        public void Setup()
        {
            var mapper = new MapperConfiguration(c => c.CreateProfile("test"));
            new AccountModelMapper().Configure(mapper);
            
            _userRepositoryMock = new Mock<IUserRepository>();
            _userRoleRepoMock = new Mock<IUserRoleRepository>();

            var passwordHasher = new PasswordHasher();
            _validationProvider = new ValidationProvider(c => new AddUserValidator(_userRepositoryMock.Object));
            _accountUserService = new AccountUserService(_userRoleRepoMock.Object, mapper.CreateMapper(),
                _userRepositoryMock.Object, _validationProvider, passwordHasher);
        }

        [Test]
        public void Can_Get_Role_Groups_With_Roles()
        {
            var fixture = new Fixture();

            _userRoleRepoMock.Setup(c => c.GetAllGroupsWithRoles(It.IsAny<int>()))
                .Returns(fixture.Create<List<RoleGroupSummaryPoco>>());

            var result = _accountUserService.GetAllRoleGroupWithRoles(99);
            Assert.NotNull(result);
            Assert.Greater(result.Count(),0);
        }

        [Test]
        public void Can_Update_User_Roles()
        {
            var mapper = new MapperConfiguration(c => c.CreateProfile("test"));
            new AccountModelMapper().Configure(mapper);

            var passwordHasher = new PasswordHasher();
            _validationProvider = new ValidationProvider(c => new AddUserRoleValidator(_userRepositoryMock.Object));
            _accountUserService = new AccountUserService(_userRoleRepoMock.Object, mapper.CreateMapper(),
                _userRepositoryMock.Object, _validationProvider, passwordHasher);

            var fixture = new Fixture();

            _userRoleRepoMock.Setup(c => c.AddRoles(It.IsAny<AddUserRolePoco>()))
                .Returns(fixture.Create<UserRoleSummaryPoco>());
            
            _userRoleRepoMock.Setup(c => c.RemoveUserRoles(It.IsAny<int>()));

            var result = _accountUserService.AddUserRoles(fixture.Create<AddUserRolesParamModel>());
            Assert.NotNull(result);
            Assert.Greater(result.Count(),0);
            _userRoleRepoMock.Verify(c => c.RemoveUserRoles(It.IsAny<int>()), Times.Once);
        }

        [Test]
        public void Can_Get_All_List_Of_Users()
        {
            var fixture = new Fixture();

            _userRepositoryMock.Setup(c => c.GetAll())
                .Returns(fixture.Create<List<UserDetailsPoco>>());

            var result = _accountUserService.GetAllUsers();
            Assert.NotNull(result);
            Assert.Greater(result.Count(), 0);
        }

        [Test]
        public void Cannot_Add_User_With_Existing_Username()
        {
            Assert.Throws<ValidationException>(() =>
            {
                var fixture = new Fixture();

                _userRepositoryMock.Setup(c => c.IsDuplicateUsername(It.IsAny<string>()))
                    .Returns(true);

                var fixObj = fixture.Create<AddUserParamModel>();
                fixObj.Email = "sample@email.com";

                var result = _accountUserService.AddUser(fixture.Create<AddUserParamModel>());
                Assert.NotNull(result);
            }, "Duplicate Username was allowed");
        }

        [Test]
        public void Cannot_Add_User_With_Existing_Email()
        {
            Assert.Throws<ValidationException>(() =>
            {
                var fixture = new Fixture();

                _userRepositoryMock.Setup(c => c.IsDuplicateEmail(It.IsAny<string>()))
                    .Returns(true);

                var fixObj = fixture.Create<AddUserParamModel>();
                fixObj.Email = "sample@email.com";

                var result = _accountUserService.AddUser(fixObj);
                Assert.NotNull(result);
            }, "Duplicate email was allowed");
        }

        [Test]
        public void Cannot_Add_User_With_Invalid_Email()
        {
            Assert.Throws<ValidationException>(() =>
            {
                var fixture = new Fixture();

                _userRepositoryMock.Setup(c => c.IsDuplicateUsername(It.IsAny<string>()))
                    .Returns(false);

                _userRepositoryMock.Setup(c => c.IsDuplicateUsername(It.IsAny<string>()))
                    .Returns(false);

                var fixObj = fixture.Create<AddUserParamModel>();
                fixObj.Email = "This is an invalid email";

                var result = _accountUserService.AddUser(fixObj);
                Assert.NotNull(result);
            }, "Duplicate email was allowed");
        }

        [Test]
        public void Can_Add_User_Without_Existing_Email_And_Username()
        {
            var fixture = new Fixture();

            _userRepositoryMock.Setup(c => c.IsDuplicateUsername(It.IsAny<string>()))
                .Returns(false);

            _userRepositoryMock.Setup(c => c.IsDuplicateUsername(It.IsAny<string>()))
                .Returns(false);

            _userRepositoryMock.Setup(c => c.AddUser(It.IsAny<AddUserPoco>()))
                .Returns(fixture.Create<UserCredentialPoco>());

            var fixObj = fixture.Create<AddUserParamModel>();
            fixObj.Email = "sample@email.com";

            var result = _accountUserService.AddUser(fixObj);
            Assert.NotNull(result);
        }

        [Test]
        public void Can_Get_User_Details_By_Id()
        {
            var fixture = new Fixture();

            _userRepositoryMock.Setup(c => c.GetUserDetailsById(It.IsAny<int>()))
                .Returns(fixture.Create<UserDetailsPoco>());

            var result = _accountUserService.GetUserDetailsById(1);
            Assert.NotNull(result);
        }

        [Test]
        public void Can_Update_User_Details()
        {
            var mapper = new MapperConfiguration(c => c.CreateProfile("test"));
            new AccountModelMapper().Configure(mapper);

            var passwordHasher = new PasswordHasher();
            _validationProvider = new ValidationProvider(c => new UpdateUserValidator(_userRepositoryMock.Object));
            _accountUserService = new AccountUserService(_userRoleRepoMock.Object, mapper.CreateMapper(),
                _userRepositoryMock.Object, _validationProvider, passwordHasher);

            var fixture = new Fixture();

            _userRepositoryMock.Setup(c => c.GetUserDetailsById(It.IsAny<int>()))
                .Returns(fixture.Create<UserDetailsPoco>());

            _userRepositoryMock.Setup(c => c.UpdateUserDetails(It.IsAny<UpdateUserDetailsPoco>()))
                .Returns(fixture.Create<UserDetailsPoco>());

            var updated = _accountUserService.UpdateUser(fixture.Create<UpdateUserParamModel>());

            Assert.NotNull(updated);
        }

        [Test]
        public void Can_Update_Usr_password()
        {
            var mapper = new MapperConfiguration(c => c.CreateProfile("test"));
            new AccountModelMapper().Configure(mapper);

            var passwordHasher = new PasswordHasher();
            _validationProvider = new ValidationProvider(c => new UpdateUserPasswordValidator(_userRepositoryMock.Object));
            _accountUserService = new AccountUserService(_userRoleRepoMock.Object, mapper.CreateMapper(),
                _userRepositoryMock.Object, _validationProvider, passwordHasher);

            var fixture = new Fixture();

            _userRepositoryMock.Setup(c => c.GetUserDetailsById(It.IsAny<int>()))
                .Returns(fixture.Create<UserDetailsPoco>());

            _userRepositoryMock.Setup(c => c.UpdateUserPassword(It.IsAny<UpdateUserPasswordPoco>()))
                .Returns(fixture.Create<UserCredentialPoco>());

            var updated = _accountUserService.UpdateUserPassword(fixture.Create<UpdateUserPasswordParamModel>());

            Assert.NotNull(updated);
        }
    }
}
