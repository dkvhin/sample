﻿using AutoMapper;
using SimpleInjector;

namespace D7Soft.Common.Interfaces
{
    public interface IValidatorBootstrapper
    {
        void Configure(Container container);
    }
}
