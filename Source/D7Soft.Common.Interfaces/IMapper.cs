﻿using AutoMapper;

namespace D7Soft.Common.Interfaces
{
    public interface ICustomMapper
    {
        void Map(IMapperConfiguration configuration);
    }
}
