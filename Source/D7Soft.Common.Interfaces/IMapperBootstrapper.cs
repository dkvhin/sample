﻿using AutoMapper;

namespace D7Soft.Common.Interfaces
{
    public interface IMapperBootstrapper
    {
        void Configure(IMapperConfiguration configuration);
    }
}
