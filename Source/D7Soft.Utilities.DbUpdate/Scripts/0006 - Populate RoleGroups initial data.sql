SET IDENTITY_INSERT [dbo].[RoleGroups] ON 
GO
INSERT [dbo].[RoleGroups] ([Id], [Name], [Description], [IsActive], [DateCreated]) VALUES (1, N'Super', N'Super Role Group', 1, GETDATE())
GO
INSERT [dbo].[RoleGroups] ([Id], [Name], [Description], [IsActive], [DateCreated]) VALUES (2, N'Payroll', N'Payroll Access Role', 1,  GETDATE())
GO
SET IDENTITY_INSERT [dbo].[RoleGroups] OFF
GO
