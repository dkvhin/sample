ALTER TABLE dbo.FormTemplateInputs ADD
	FontSize int NOT NULL CONSTRAINT DF_FormTemplateInputs_FontSize DEFAULT 20
GO