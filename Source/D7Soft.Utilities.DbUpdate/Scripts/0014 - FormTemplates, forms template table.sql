CREATE TABLE dbo.FormTemplates
	(
	Id int NOT NULL IDENTITY (1, 1),
	Name varchar(50) NOT NULL,
	Width int NOT NULL,
	Height int NOT NULL,
	Image varchar(MAX) NOT NULL,
	IsActive bit NOT NULL,
	DateCreated datetime NOT NULL,
	DateModified datetime NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.FormTemplates ADD CONSTRAINT
	DF_FormTemplates_IsActive DEFAULT 1 FOR IsActive
GO
ALTER TABLE dbo.FormTemplates ADD CONSTRAINT
	PK_FormTemplates PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO