ALTER TABLE dbo.FormTemplateInputs ADD
	IsAutoComplete bit NOT NULL CONSTRAINT DF_FormTemplateInputs_IsText DEFAULT 0,
	IsDate bit NOT NULL CONSTRAINT DF_FormTemplateInputs_IsDate DEFAULT 0
GO