CREATE TABLE dbo.FormTemplateInputs
	(
	Id int NOT NULL IDENTITY (1, 1),
	FormTemplateId int NOT NULL,
	Width int NOT NULL,
	PositionX int NOT NULL,
	PositionY int NOT NULL,
	IsNumeric bit NOT NULL,
	HasConvertInput bit NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.FormTemplateInputs ADD CONSTRAINT
	DF_FormTemplateInputs_IsNumeric DEFAULT 0 FOR IsNumeric
GO
ALTER TABLE dbo.FormTemplateInputs ADD CONSTRAINT
	DF_FormTemplateInputs_HasConvertInput DEFAULT 0 FOR HasConvertInput
GO
ALTER TABLE dbo.FormTemplateInputs ADD CONSTRAINT
	PK_FormTemplateInputs PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.FormTemplateInputs ADD CONSTRAINT
	FK_FormTemplateInputs_FormTemplates FOREIGN KEY
	(
	FormTemplateId
	) REFERENCES dbo.FormTemplates
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO