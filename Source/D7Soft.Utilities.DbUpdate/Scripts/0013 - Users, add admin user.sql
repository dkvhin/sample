declare @id int

INSERT [dbo].[Users] ([FirstName], [LastName], [Email], [Username], [PasswordSalt], [HashedPassword], [DateCreated], [DateModified]) VALUES (N'User', N'user', N'admin@email.com', N'admin', N'kPiS4Me1150pzb8CoAg0lQ==', N'9da0d6b738affc3fa8b86d29abf40eb2519ae765862e56a326cafe1ef363bdb0', CAST(N'2016-01-26 00:00:00.000' AS DateTime), CAST(N'2016-01-26 00:00:00.000' AS DateTime))

set @id = SCOPE_IDENTITY()

INSERT INTO [dbo].[UserRoles] ([UserId],[RoleId]) VALUES (@id, 1)