CREATE TABLE dbo.FormCustomers
	(
	Id int NOT NULL IDENTITY (1, 1),
	FirstName varchar(150) NULL,
	LastName varchar(150) NULL,
	MiddleName varchar(150) NULL,
	Company varchar(250) NULL,
	ContactNumber varchar(50) NULL,
	Address varchar(250) NULL,
	DateCreated datetime NOT NULL,
	DateModified datetime NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.FormCustomers ADD CONSTRAINT
	PK_FormCustomers PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO