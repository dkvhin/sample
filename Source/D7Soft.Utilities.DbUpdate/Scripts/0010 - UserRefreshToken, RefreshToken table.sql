CREATE TABLE dbo.UserRefreshTokens
	(
	Id int NOT NULL,
	Token varchar(MAX) NOT NULL,
	UserId int NOT NULL,
	Expires datetime NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.UserRefreshTokens ADD CONSTRAINT
	PK_UserRefreshTokens PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.UserRefreshTokens ADD CONSTRAINT
	FK_UserRefreshTokens_Users FOREIGN KEY
	(
	UserId
	) REFERENCES dbo.Users
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO