SET IDENTITY_INSERT [dbo].[Roles] ON 
GO
INSERT [dbo].[Roles] ([Id], [RoleGroupId], [Name], [Key], [Description], [IsActive], [DateCreated], [DateModified]) VALUES (1, 1, N'Super Admin', N'super_admin', N'Can access all site applications', 1, GETDATE(), GETDATE())
GO
INSERT [dbo].[Roles] ([Id], [RoleGroupId], [Name], [Key], [Description], [IsActive], [DateCreated], [DateModified]) VALUES (2, 2, N'Payroll Admin', N'payroll_admin', N'Can access all features of payroll', 1, GETDATE(), GETDATE())
GO
SET IDENTITY_INSERT [dbo].[Roles] OFF
GO
