ALTER TABLE dbo.UserRefreshTokens
	DROP CONSTRAINT FK_UserRefreshTokens_Users
GO

CREATE TABLE dbo.Tmp_UserRefreshTokens
	(
	Id int NOT NULL IDENTITY (1, 1),
	Token varchar(MAX) NOT NULL,
	UserId int NOT NULL,
	Expires datetime NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO

SET IDENTITY_INSERT dbo.Tmp_UserRefreshTokens ON
GO
IF EXISTS(SELECT * FROM dbo.UserRefreshTokens)
	 EXEC('INSERT INTO dbo.Tmp_UserRefreshTokens (Id, Token, UserId, Expires)
		SELECT Id, Token, UserId, Expires FROM dbo.UserRefreshTokens WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_UserRefreshTokens OFF
GO
DROP TABLE dbo.UserRefreshTokens
GO
EXECUTE sp_rename N'dbo.Tmp_UserRefreshTokens', N'UserRefreshTokens', 'OBJECT' 
GO
ALTER TABLE dbo.UserRefreshTokens ADD CONSTRAINT
	PK_UserRefreshTokens PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.UserRefreshTokens ADD CONSTRAINT
	FK_UserRefreshTokens_Users FOREIGN KEY
	(
	UserId
	) REFERENCES dbo.Users
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO