INSERT INTO [dbo].[RoleGroups]
           ([Name]
           ,[Description]
           ,[IsActive]
           ,[DateCreated])
     VALUES
           ('Forms'
           ,'Forms Access Role'
           ,1
           ,GETDATE())
GO

declare @roleGroupid int
SET @roleGroupid = SCOPE_IDENTITY();

INSERT INTO [dbo].[Roles]
           ([RoleGroupId]
           ,[Name]
           ,[Key]
           ,[Description]
           ,[IsActive]
           ,[DateCreated]
           ,[DateModified])
     VALUES
           (@roleGroupid
           ,'Forms Admin'
           ,'forms_admin'
           ,'Can access all forms features'
           ,1
           ,GETDATE()
           ,GETDATE())
GO
