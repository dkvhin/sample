INSERT INTO [dbo].[RoleGroups]
           ([Name]
           ,[Description]
           ,[IsActive]
           ,[DateCreated])
     VALUES
           ('Accounts'
           ,'Accounts Access Role'
           ,1
           ,GETDATE())
GO

declare @roleGroupid int
SET @roleGroupid = SCOPE_IDENTITY();

INSERT INTO [dbo].[Roles]
           ([RoleGroupId]
           ,[Name]
           ,[Key]
           ,[Description]
           ,[IsActive]
           ,[DateCreated]
           ,[DateModified])
     VALUES
           (@roleGroupid
           ,'Accounts Admin'
           ,'accounts_admin'
           ,'Can access all accounts features'
           ,1
           ,GETDATE()
           ,GETDATE())
GO
