CREATE TABLE dbo.UserRoles
	(
	UserId int NOT NULL,
	RoleId int NOT NULL
	)  ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX IX_UserRoles ON dbo.UserRoles
	(
	UserId,
	RoleId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.UserRoles ADD CONSTRAINT
	FK_UserRoles_Users FOREIGN KEY
	(
	UserId
	) REFERENCES dbo.Users
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.UserRoles ADD CONSTRAINT
	FK_UserRoles_Roles FOREIGN KEY
	(
	RoleId
	) REFERENCES dbo.Roles
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO