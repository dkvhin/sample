﻿using System;
using System.Configuration;
using System.Linq;
using System.Reflection;
using DbUp;

namespace D7Soft.Utilities.DbUpdate
{
    class Program
    {
        static int Main(string[] args)
        {
            var connectionString =
                ConfigurationManager.ConnectionStrings["DatabaseContext"].ConnectionString
                ?? "Server=.;Database=D7Soft;Integrated Security=SSPI;Application Name=JobDroid;MultipleActiveResultSets=True";

            var upgrader =
                DeployChanges.To
                    .SqlDatabase(connectionString)
                    .WithScriptsEmbeddedInAssembly(Assembly.GetExecutingAssembly())
                    .LogToConsole()
                    .Build();

            var result = upgrader.PerformUpgrade();

            if (!result.Successful)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(result.Error);
                Console.ResetColor();
#if DEBUG
                Console.ReadLine();
#endif
                return -1;
            }

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Success!");
            Console.ResetColor();
            return 0;
        }
    }
}
