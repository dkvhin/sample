﻿using System;
using System.Linq;
using D7Soft.Data.DAO;
using D7Soft.Data.DAO.Entity;
using D7Soft.Data.Poco.User;
using D7Soft.Data.Repository.Interfaces;
using System.Collections.Generic;
namespace D7Soft.Data.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly IDatabaseContext _context;
        public UserRepository(IDatabaseContext context)
        {
            _context = context;
        }


        public UserCredentialPoco AddUser(AddUserPoco addUserPoco)
        {
            var dao = new User
            {
                Email = addUserPoco.Email,
                PasswordSalt = addUserPoco.PasswordSalt,
                HashedPassword = addUserPoco.HashedPassword,
                DateModified = DateTime.Now,
                DateCreated = DateTime.Now,
                FirstName = addUserPoco.FirstName,
                LastName = addUserPoco.LastName,
                Username = addUserPoco.Username
            };

            _context.Users.Add(dao);
            _context.SaveChanges();

            return _context.Users.Where(c => c.Id == dao.Id).Project().To<UserCredentialPoco>().SingleOrDefault();
        }


        public UserCredentialPoco GetUserByEmail(string emailAddress)
        {
            return _context.Users.Where(c => c.Email == emailAddress).Project().To<UserCredentialPoco>().SingleOrDefault();
        }


        public UserCredentialPoco GetUserByUsername(string username)
        {
            return _context.Users.Where(c => c.Username == username).Project().To<UserCredentialPoco>().SingleOrDefault();
        }


        public UserCredentialPoco GetUserCredentialDetailsById(int id)
        {
            return _context.Users.Where(c => c.Id == id).Project().To<UserCredentialPoco>().SingleOrDefault();
        }


        public UserDetailsPoco UpdateUserDetails(UpdateUserDetailsPoco poco)
        {
            var dao = _context.Users.SingleOrDefault(c => c.Id == poco.Id);

            if (dao == null)
                return null;

            dao.FirstName = poco.FirstName;
            dao.LastName = poco.LastName;
            dao.Email = poco.Email;
            dao.Username = poco.Username;
            dao.DateModified = DateTime.Now;
            _context.SaveChanges();

            return _context.Users.Where(c => c.Id == poco.Id).Project().To<UserCredentialPoco>().SingleOrDefault();
        }


        public IEnumerable<UserDetailsPoco> GetAll()
        {
            return _context.Users.Project().To<UserDetailsPoco>();
        }


        public bool IsDuplicateUsername(string username)
        {
            return _context.Users.Any(c => c.Username == username);
        }


        public bool IsDuplicateEmail(string email)
        {
            return _context.Users.Any(c => c.Email == email);
        }


        public UserDetailsPoco GetUserDetailsById(int id)
        {
            return _context.Users.Where(c => c.Id == id).Project().To<UserDetailsPoco>().SingleOrDefault();
        }


        public bool IsDuplicateUsername(string username, int exceptUserId)
        {
            return _context.Users.Any(c => c.Username == username && c.Id != exceptUserId);
        }

        public bool IsDuplicateEmail(string email, int exceptUserId)
        {
            return _context.Users.Any(c => c.Email == email && c.Id != exceptUserId);
        }

        public UserCredentialPoco UpdateUserPassword(UpdateUserPasswordPoco poco)
        {
            var dao = _context.Users.SingleOrDefault(c => c.Id == poco.Id);

            if (dao == null)
                return null;

            dao.PasswordSalt = poco.PasswordSalt;
            dao.HashedPassword = poco.HashedPassword;
            dao.DateModified = DateTime.Now;
            _context.SaveChanges();

            return GetUserCredentialDetailsById(poco.Id);
        }
    }
}
