﻿using System;
using System.Linq;
using D7Soft.Data.DAO;
using D7Soft.Data.DAO.Entity;
using D7Soft.Data.Poco.FormCustomer;
using D7Soft.Data.Repository.Interfaces;
using System.Collections.Generic;
namespace D7Soft.Data.Repository
{
    public class FormCustomerRepository : IFormCustomerRepository
    {
        private readonly IDatabaseContext _context;

        public FormCustomerRepository(IDatabaseContext context)
        {
            _context = context;
        }

        public FormCustomerPoco AddCustomer(AddFormCustomerPoco poco)
        {
            var dao = new FormCustomer
            {
                FirstName = poco.FirstName,
                LastName = poco.LastName,
                MiddleName = poco.MiddleName,
                Company = poco.Company,
                Address = poco.Address,
                ContactNumber = poco.ContactNumber,
                DateCreated = DateTime.Now,
                DateModified = DateTime.Now
            };

            _context.FormCustomers.Add(dao);
            _context.SaveChanges();

            return GetCustomerById(dao.Id);
        }

        public FormCustomerPoco GetCustomerById(int formCustomerId)
        {
            return
                _context.FormCustomers.Where(c => c.Id == formCustomerId)
                    .Take(1)
                    .Project()
                    .To<FormCustomerPoco>().SingleOrDefault();
        }


        public FormCustomerPoco UpdateCustomer(UpdateFormCustomerPoco poco)
        {
            var dao = _context.FormCustomers.SingleOrDefault(c => c.Id == poco.Id);

            if (dao == null)
                return null;

            dao.FirstName = poco.FirstName;
            dao.LastName = poco.LastName;
            dao.ContactNumber = poco.ContactNumber;
            dao.MiddleName = poco.MiddleName;
            dao.Company = poco.Company;
            dao.Address = poco.Address;
            dao.DateModified = DateTime.Now;

            _context.SaveChanges();

            return GetCustomerById(dao.Id);
        }


        public IEnumerable<FormCustomerPoco> GetAll()
        {
            return _context.FormCustomers.Project().To<FormCustomerPoco>();
        }



        public void DeleteCustomer(int formCustomerId)
        {
            var dao = _context.FormCustomers.SingleOrDefault(c => c.Id == formCustomerId);

            if (dao == null)
                return;

            _context.FormCustomers.Remove(dao);
            _context.SaveChanges();
        }


        public IEnumerable<FormCustomerPoco> Filter(string keyword)
        {
            var keywords = keyword.Split(' ');
            return
                _context.FormCustomers.Where(
                    c => keywords.Any(e => c.FirstName.Contains(e) || c.LastName.Contains(e) || c.Company.Contains(e)))
                    .Project()
                    .To<FormCustomerPoco>();
        }
    }
}
