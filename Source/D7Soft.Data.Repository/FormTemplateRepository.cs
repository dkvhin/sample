﻿using System;
using System.Collections.Generic;
using System.Linq;
using D7Soft.Data.DAO;
using D7Soft.Data.DAO.Entity;
using D7Soft.Data.Poco.FormTemplate;
using D7Soft.Data.Repository.Interfaces;
namespace D7Soft.Data.Repository
{
    public class FormTemplateRepository : IFormTemplateRepository
    {
        private readonly IDatabaseContext _context;

        public FormTemplateRepository(IDatabaseContext context)
        {
            _context = context;
        }

        public FormTemplateSummaryPoco AddFormTemplate(AddFormTemplatePoco poco)
        {
            var dao = new FormTemplate
            {
                DateModified = DateTime.Now,
                DateCreated = DateTime.Now,
                Height = poco.Height,
                Image = poco.Image,
                IsActive = true,
                Name = poco.Name,
                Width = poco.Width
            };

            _context.FormTemplates.Add(dao);
            _context.SaveChanges();

            return GetFormTemplateById(dao.Id);
        }

        public FormTemplateSummaryPoco GetFormTemplateById(int formTemplateId)
        {
            return
                _context.FormTemplates.Where(c => c.Id == formTemplateId && c.IsActive)
                    .Take(1)
                    .Project()
                    .To<FormTemplateSummaryPoco>().SingleOrDefault();
        }


        public FormTemplateSummaryPoco UpdateFormTemplate(UpdateFormTemplatePoco poco)
        {
            var dao = _context.FormTemplates.SingleOrDefault(c => c.Id == poco.Id && c.IsActive);

            if (dao == null)
                return null;

            dao.Image = poco.Image;
            dao.Name = poco.Name;
            dao.DateModified = DateTime.Now;
            dao.Height = poco.Height;
            dao.Width = poco.Width;

            _context.SaveChanges();

            return GetFormTemplateById(dao.Id);
        }


        public FormTemplateInputSummaryPoco AddFormTemplateInput(AddFormTemplateInputPoco poco)
        {
            var dao = new FormTemplateInput
            {
                FormTemplateId = poco.FormTemplateId,
                HasConvertInput = poco.HasConvertInput,
                IsNumeric = poco.IsNumeric,
                PositionX = poco.PositionX,
                PositionY = poco.PositionY,
                Width = poco.Width,
                IsConvert = poco.IsConvert,
                FontSize = poco.FontSize,
                IsAutoComplete =  poco.IsAutoComplete,
                IsDate =  poco.IsDate
            };

            _context.FormTemplateInputs.Add(dao);
            _context.SaveChanges();

            return GetFormTemplateInputById(dao.Id);
        }

        public FormTemplateInputSummaryPoco GetFormTemplateInputById(int formTemplateInputId)
        {
            return
                _context.FormTemplateInputs.Where(c => c.Id == formTemplateInputId)
                    .Take(1)
                    .Project()
                    .To<FormTemplateInputSummaryPoco>().SingleOrDefault();
        }


        public IEnumerable<FormTemplateInputSummaryPoco> GetFormTemplateInputs(int formTemplateId)
        {
            return _context.FormTemplateInputs.Where(c => c.FormTemplateId == formTemplateId)
                .Project().To<FormTemplateInputSummaryPoco>();
        }


        public IEnumerable<FormTemplateSummaryPoco> GetAllFormTemplates()
        {
            return _context.FormTemplates.Where(c => c.IsActive).Project().To<FormTemplateSummaryPoco>();
        }


        public void DeleteFormTemplateInputs(int formTemplateId)
        {
            var items = _context.FormTemplateInputs.Where(c => c.FormTemplateId == formTemplateId);
            _context.FormTemplateInputs.RemoveRange(items);
            _context.SaveChanges();
        }


        public void DeleteFormTemplate(int formTemplateId)
        {
            var dao = _context.FormTemplates.SingleOrDefault(c => c.Id == formTemplateId && c.IsActive);

            if (dao == null)
                return;

            dao.IsActive = false;

            _context.SaveChanges();
        }
    }
}
