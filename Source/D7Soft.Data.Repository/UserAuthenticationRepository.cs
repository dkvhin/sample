﻿using System.Linq;
using D7Soft.Data.DAO;
using D7Soft.Data.DAO.Entity;
using D7Soft.Data.Repository.Interfaces;
using D7Soft.Data.Poco.Authentication;
using D7Soft.Data.Poco.UserRefreshToken;

namespace D7Soft.Data.Repository
{
    public class UserAuthenticationRepository : IUserAuthenticationRepository
    {
        private readonly IDatabaseContext _context;

        public UserAuthenticationRepository(IDatabaseContext context)
        {
            _context = context;
        }

        public ClientAppModel GetByClientId(string clientId)
        {
            return
                _context.ClientApps.Where(c => c.ClientId == clientId && c.IsActive)
                    .Project()
                    .To<ClientAppModel>().SingleOrDefault();
        }


        public UserRefreshTokenPoco AddRefreshToken(AddUserRefreshTokenPoco poco)
        {
            var dao = new UserRefreshToken
            {
                Token = poco.Token,
                UserId = poco.UserId,
                Expires = poco.Expiration,
                ClientId = poco.ClientId
            };

            _context.UserRefreshTokens.Add(dao);
            _context.SaveChanges();

            return GetRefreshTokenByToken(poco.Token);
        }

        public UserRefreshTokenPoco GetRefreshTokenByToken(string token)
        {
            return
                _context.UserRefreshTokens.Where(c => c.Token == token)
                    .Take(1)
                    .Project()
                    .To<UserRefreshTokenPoco>()
                    .SingleOrDefault();
        }


        public void DeleteByToken(string token)
        {
            var dao = _context.UserRefreshTokens.SingleOrDefault(c => c.Token == token);

            if (dao == null)
                return;

            _context.UserRefreshTokens.Remove(dao);
            _context.SaveChanges();
        }
    }
}
