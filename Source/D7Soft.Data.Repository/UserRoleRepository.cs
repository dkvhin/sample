﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using D7Soft.Data.DAO;
using D7Soft.Data.DAO.Entity;
using D7Soft.Data.Poco.Role;
using D7Soft.Data.Poco.UserRole;
using D7Soft.Data.Repository.Interfaces;
using D7Soft.Data.Poco.RoleGroup;
namespace D7Soft.Data.Repository
{
    public class UserRoleRepository : IUserRoleRepository
    {
        private readonly IDatabaseContext _context;

        public UserRoleRepository(IDatabaseContext context)
        {
            _context = context;
        }

        public UserRoleSummaryPoco AddRoles(AddUserRolePoco addUserRolePoco)
        {
            var user = _context.Users.SingleOrDefault(c => c.Id == addUserRolePoco.UserId);

            if (user == null)
                return null;

            var roles = _context.Roles.Where(c => addUserRolePoco.RoleIds.Contains(c.Id));
            user.Roles = new List<Role>(roles);
           
            _context.SaveChanges();

            return new UserRoleSummaryPoco
            {
                UserId = addUserRolePoco.UserId,
                Roles = roles.Project().To<RoleSummaryPoco>().ToList()
            };
        }


        public IEnumerable<RoleSummaryPoco> GetRolesByUserId(int userId)
        {
            return _context.Roles.Where(c => c.Users.Any(e => e.Id == userId)).Project().To<RoleSummaryPoco>();
        }


        public IEnumerable<RoleGroupSummaryPoco> GetAllGroupsWithRoles(int userId)
        {
            return _context.RoleGroups.Where(c => c.IsActive)
                .Select(roleGroup => new RoleGroupSummaryPoco
                {
                    Id = roleGroup.Id,
                    Description = roleGroup.Description,
                    Name = roleGroup.Name,
                    Roles =
                        _context.Roles.Where(role => role.RoleGroupId == roleGroup.Id)
                            .Select(role => new RoleInfoSummaryPoco
                            {
                                Id = role.Id,
                                Description = role.Description,
                                Key = role.Key,
                                Name = role.Name,
                                Selected = _context.Users.Any(e => e.Id == userId && e.Roles.Any(f => f.Id == role.Id))
                            })
                });
        }


        public void RemoveUserRoles(int userId)
        {
            var user = _context.Users.Include(c => c.Roles).SingleOrDefault(c => c.Id == userId);

            if (user == null)
                throw new KeyNotFoundException(string.Format("User Id {0} not found", userId));

            user.Roles.Clear();
            _context.SaveChanges();
        }
    }
}
