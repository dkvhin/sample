﻿using System;
using System.Collections.Generic;
using D7Soft.Data.Poco.User;

namespace D7Soft.Data.Repository.Interfaces
{
    public interface IUserRepository
    {
        UserCredentialPoco AddUser(AddUserPoco poco);
        UserCredentialPoco GetUserByEmail(string emailAddress);
        UserCredentialPoco GetUserByUsername(string username);
        UserCredentialPoco GetUserCredentialDetailsById(int id);
        UserDetailsPoco UpdateUserDetails(UpdateUserDetailsPoco poco);
        IEnumerable<UserDetailsPoco> GetAll();
        bool IsDuplicateUsername(string username);
        bool IsDuplicateUsername(string username, int exceptUserId);
        bool IsDuplicateEmail(string email);
        UserDetailsPoco GetUserDetailsById(int userId);
        bool IsDuplicateEmail(string email, int exceptUserId);
        UserCredentialPoco UpdateUserPassword(UpdateUserPasswordPoco poco);
    }
}
