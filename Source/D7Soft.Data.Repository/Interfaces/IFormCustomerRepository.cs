﻿using System.Collections.Generic;
using D7Soft.Data.Poco.FormCustomer;

namespace D7Soft.Data.Repository.Interfaces
{
    public interface IFormCustomerRepository
    {
        FormCustomerPoco AddCustomer(AddFormCustomerPoco poco);
        FormCustomerPoco UpdateCustomer(UpdateFormCustomerPoco poco);
        IEnumerable<FormCustomerPoco> GetAll();
        FormCustomerPoco GetCustomerById(int formCustomerId);
        void DeleteCustomer(int formCustomerId);
        IEnumerable<FormCustomerPoco> Filter(string keyword);
    }
}
