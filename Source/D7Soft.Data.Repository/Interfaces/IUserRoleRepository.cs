﻿using System.Collections.Generic;
using D7Soft.Data.Poco.Role;
using D7Soft.Data.Poco.RoleGroup;
using D7Soft.Data.Poco.UserRole;

namespace D7Soft.Data.Repository.Interfaces
{
    public interface IUserRoleRepository
    {
        UserRoleSummaryPoco AddRoles(AddUserRolePoco addUserRolePoco);

        IEnumerable<RoleSummaryPoco> GetRolesByUserId(int userId);

        IEnumerable<RoleGroupSummaryPoco> GetAllGroupsWithRoles(int userId);

        void RemoveUserRoles(int userId);
    }
}
