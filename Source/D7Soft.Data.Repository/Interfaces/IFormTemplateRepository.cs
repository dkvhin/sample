﻿using System.Collections.Generic;
using D7Soft.Data.Poco.FormTemplate;

namespace D7Soft.Data.Repository.Interfaces
{
    public interface IFormTemplateRepository
    {
        FormTemplateSummaryPoco AddFormTemplate(AddFormTemplatePoco poco);

        FormTemplateSummaryPoco UpdateFormTemplate(UpdateFormTemplatePoco poco);

        FormTemplateInputSummaryPoco AddFormTemplateInput(AddFormTemplateInputPoco poco);

        IEnumerable<FormTemplateInputSummaryPoco> GetFormTemplateInputs(int formTemplateId);
        FormTemplateInputSummaryPoco GetFormTemplateInputById(int formTemplateInputId);

        IEnumerable<FormTemplateSummaryPoco> GetAllFormTemplates();

        FormTemplateSummaryPoco GetFormTemplateById(int formTemplateId);

        void DeleteFormTemplateInputs(int formTemplateId);

        void DeleteFormTemplate(int formTemplateId);
    }
}
