﻿using D7Soft.Data.Poco.Authentication;
using D7Soft.Data.Poco.UserRefreshToken;

namespace D7Soft.Data.Repository.Interfaces
{
    public interface IUserAuthenticationRepository
    {
        ClientAppModel GetByClientId(string clientId);
        UserRefreshTokenPoco AddRefreshToken(AddUserRefreshTokenPoco poco);
        UserRefreshTokenPoco GetRefreshTokenByToken(string token);
        void DeleteByToken(string token);
    }
}
