﻿using System;
using System.Collections;

namespace D7Soft.Core.Validation
{
    public interface IValidationProvider
    {
        void Validate(Object entity);
        void ValidateAll(IEnumerable entities);
    }
}
