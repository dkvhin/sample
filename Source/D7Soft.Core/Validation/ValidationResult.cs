﻿using System;
using System.Linq.Expressions;
using D7Soft.Core.Helpers;

namespace D7Soft.Core.Validation
{
    public class ValidationResult
    {
        public ValidationResult(String key, String message)
        {
            Key = key;
            Message = message;
        }

        public String Key { get; private set; }
        public String Message { get; private set; }
    }

    public class ValidationResult<TEntity> : ValidationResult where TEntity : class
    {
        public ValidationResult(Expression<Func<TEntity, object>> lambdaExpression, String message)
            :base (lambdaExpression.GetPropertyName(),message)
        {
        }
    }
}
