﻿using System;
using System.Collections.Generic;

namespace D7Soft.Core.Validation
{
    public interface IValidator
    {
        IEnumerable<ValidationResult> Validate(Object entity);
    }
}
