﻿using System;
using System.Collections.Generic;

namespace D7Soft.Core.Validation
{
    public abstract class Validator<T> : IValidator
    {
        IEnumerable<ValidationResult> IValidator.Validate(Object entity)
        {
            if (entity == null) 
                throw new ArgumentNullException("entity");

            return this.Validate((T)entity);
        }

        protected abstract IEnumerable<ValidationResult> Validate(T entity);
    }
}
