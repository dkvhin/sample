﻿using System.Collections.Generic;
using System.Linq;

namespace D7Soft.Core.Validation
{
    public sealed class NullValidator<T> : Validator<T>
    {
        protected override IEnumerable<ValidationResult> Validate(T entity)
        {
            return Enumerable.Empty<ValidationResult>();
        }
    }
}
